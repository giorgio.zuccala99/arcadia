
""" Multi-plain configuration --- Analysis of .detsync files:

CONSIDERATIONS:
    
-) This code works with data saved without trigger
-) .detsync data are written by the sw when an event is read simultaneously by all three plains --> advantages:
    
  .) noisy pixels' contribution is neglected
  .) no empty frames --> no spurious events that overload memory when decoding 
 
    
HYPOTHESIS:
    
-) No noisy pixels are fired
-) In the case of misalignment external plains are considered fixed, parallel and perfectly aligned
-) Misalignment causes: traslation (x,y) and azimutal rotation (yaw) --- no ptich & roll   
    
HOW DECODE WORKS:
    
-) All .detsync files are unpacked and decoded
-) frame_all_lst is a big list of 512x512 frames made by groups of 3 frames 
   piled up together, in the order det0-det1-det2    
    
    
INITIAL PROCEDURE --- SAFETY CHECKS:
    
-) Verification of proper behaviour:
    
  .) ts_ext always increasing
  .) alternance of det0, det1, det2 never broken in decoded file

-) Verify that noisy pixels don't turn on
-) Sample peak of ts_ext_diff distribution <-- useful for next comparison analysis with manua datal synchronization
  
    
DATA HANDLING AND SELECTION:
    
-) Removal of corrupted events (verify impact on statistics), i.e. with:
    
  .) Multiple clusters (made up even by 1 pixel)
  .) kinky clusters  ******* (rough recognition through area  fill-factor)
  .) Dsitributions of angle of incidence (devised from det0-det2) for kinky evs in det1  

After cleaning bad evs:
    
-) For each plane:
    
  .) Clusters shape (x,y) and size (area) vs angle of incidence  
  .) Clusters Areas distribution <-- useful for select optimal pix number for golden events (the ones used to assess geometric resolution)
    
-) GOLD events selection (4-5 pixels per plane) <-- (verify impact on statistics)
 
  .) Verify through hist2D that distributions of fired pixels match  
  .) Distribution of incident angles  
    

ANALYSIS OF SENSOR RESOLUTION: 
  
-) 1st measure of residuals divergence <-- Gaussian Fit    
 
-) Evaluation of Res xy vs yx

  .) Cook's algorithm for outliers  ****** (in linear approximation, i.e. small angles)
  .) Linear fit for yaw-rotation angle assessment
     
-) Re-alignment --> Roto-traslation  ****** (arbitrary fulcrum for the rotation)

-) 2nd measurement of residual divergence --> check with pixel dimension's resolution    
    
-- ITERATING PREVIOUS STEPS (see code ZG_gold_evs_ITERATION) --
   
Vary:

    .) gold n. of pixels <-- show angles distribution

Show resolution changes    
    
"""

# *** Unpacking of all DS must return a list long as a multiple of the n. of plains (3) *** #

# -) CALCOLO DEI RESIDUI:   expc - true
# -) CORREZIONE COORDINATE: expc --> true

# -) Sovrapponi hist ang inc prima e dopo i tagli per vedere i cambiamenti
# -) Anziché un taglio in gold evs prova a fare un taglio in angoli di incidenza
# -) Capire perché dopo la gold evs selection non scompare l'erbetta finale nell'istogramma


import os, time


#%% INTRO

""" Personal Folders Handling """
PC_str = r'C:\Giorgio PC/'

main_folder = PC_str + r'UNI\INSUBRIA\Attività Extra\ARCADIA - Recap/' # <-- Codes and Data folder

codes_folder = main_folder + 'Codes/'

txt_sync_folder = main_folder + r'Data\Detsync Data\txt format/'

figs_folder    = main_folder + r'Results\sync_analysis\Figs/'

arrs_folder    = main_folder + r'Results\sync_analysis\Vals/'

# --- Check if folders exist --- #
if (os.path.isdir(codes_folder) == False): raise(Exception(f"Path \n{codes_folder}\n Not found"))

if (os.path.isdir(txt_sync_folder) == False): raise(Exception(f"Path \n{txt_sync_folder}\n Not found"))
if (os.path.isdir(figs_folder)     == False): raise(Exception(f"Path \n{figs_folder}\n Not found")) 
if (os.path.isdir(arrs_folder)     == False): raise(Exception(f"Path \n{arrs_folder}\n Not found")) 

os.chdir(main_folder)

""" Graphic Parameters """
from matplotlib import pyplot as plt

plt.close('all')


last_fig_idx = -1 # <-- keeps count of opened figures

my_rect_fig_size = (15, 8) # <-- figures box dimensions
my_sqrd_fig_size = (8, 8)

my_std_font = {'family': 'Verdana',  'color':'black','weight':'normal','size': 20} # <-- std font for titles and labels 
my_font_sup = {'family': 'garamond', 'color':'darkblue','weight':'normal'} # <-- suptitles font
my_leg_font = {'family': 'Georgia', "size": 15} # <-- legend font

my_sup_title_fs = 25 # <-- suptitle size
my_leg_title_fs = 15 # <-- legend title size
my_txt_fs       = 15 # <-- printed text size

my_xy_tick_size = 18 # <-- labels tick size


# --- Ajusting cmap for 2D plots --- #
import copy, matplotlib as mpl

my_cmap = copy.copy(mpl.cm.inferno) # <-- copy the default cmap

my_cmap.set_bad(color = 'k') 


print("\nspecified folders are OK")
   

#%% MODULES & LIBRARIES

import numpy   as np
import pandas  as pd
import seaborn as sns

os.chdir(codes_folder)

from ARCADIA_funcs import decode_detsync_from_txt, ZG_PACMAN, density_clst_coords, \
                          cook_outliers_rm, rebuild_original_idxs

os.chdir(main_folder)

from scipy.optimize import curve_fit as cf

from numpy import sqrt, pi, arctan


#%% SETUP VARIABLES

# UPSTREAM   DET: 0
# DOWNSTREAM DET: 2

run_figs = True
# run_figs = False

# - Geometric Distances from setup (mm)  - #
z_01 = 19.7        # det0 - det1
z_02 = 19.7 + 19.1 # det0 - det2

open_files_lst = ['12', '13', '14', '15', ]

# - Clusters characterization - #
kink_thr = 0.4 # <-- Area fill-factor for kinky cluster discrimination (e.g. 0.4 ==> clusters' areas filled under 40% will be classified as kinky clusters)

columns_lst = ['SR bit',  'Hitmap Byte',  'Core pr', 'Col',   'Sez',         'Ser', 
               'Falling', 'ts chip',      'ts FPGA', 'ts SW', 'ts extended', 'trigger', 'sensor']


#% DATA LOADING

frames_lst = [] # <-- initialize list of frames

pix_fired_arr = np.array([], dtype = 'int64') # <-- initialize fired pixel array

DS_df = pd.DataFrame(data = np.empty(shape = [0, len(columns_lst)]), 
                     columns = columns_lst) # <-- initialize DataFrame


# - Merging Data together - #
for temp_str in open_files_lst:
    
    temp_frames_lst, temp_pix_fired_arr, temp_DS_df, temp_DS_arr = decode_detsync_from_txt(path_to_load = txt_sync_folder, 
                                                                                           filename = temp_str + '_detsync',
                                                                                           max_events = 80000, 
                                                                                           print_msgs = False) 

    # - Concatenate decoded data - #
    frames_lst += temp_frames_lst
    
    pix_fired_arr = np.concatenate((pix_fired_arr, temp_pix_fired_arr), axis = 0)

    DS_df = pd.concat(objs = (DS_df, temp_DS_df))


DS_arr = DS_df.iloc[:,:].values # <-- I can get him either like this or concatenating temp_DS_arr as for temp_DS_df

print(f"Decoded events for {open_files_lst}: {(len(frames_lst)/3):.0f} events ")
    

#%% CHECK ALTERNANCE 0-1-2

# - Extract just the Detectors' index - #
det_arr = np.array(DS_df['sensor'])

print("\nChecking correct alternance det0-det1-det2")

for i in range(1, det_arr.size - 1, 2): # <-- Loop on even positions of detectors' index

    temp_prv = det_arr[i-1]  # <-- previous idx
    temp_det = det_arr[i]    # <-- current  idx
    temp_nxt = det_arr[i+1]  # <-- next     idx

    # - Next conditions check proper Detectors' order - #
    if temp_det == 0:
        
       if (temp_prv == 0 or temp_prv == 2) and (temp_nxt == 0 or temp_nxt == 1): pass
       else:           
          print(f"PROBLEM at row {i}") 
          print(temp_prv, temp_det, temp_nxt)
   
    elif temp_det == 1:
        
       if (temp_prv == 0 or temp_prv == 1) and (temp_nxt == 1 or temp_nxt == 2): pass
       else:           
          print(f"PROBLEM at row {i}") 
          print(temp_prv, temp_det, temp_nxt)

    elif temp_det == 2:
        
       if (temp_prv == 1 or temp_prv == 2) and (temp_nxt == 2 or temp_nxt == 0): pass
       else:           
          print(f"PROBLEM at row {i}") 
          print(temp_prv, temp_det, temp_nxt)
          break              
              
print("Alternance is OK")    


#%% CHECK TS --- EVs FREQUENCY

# - Extract just the Detectors' index - #
ts_arr = np.array(DS_df['ts extended'])

# - List of arrays for single det ts_ext - #
ts_012_lst = [np.int64(ts_arr[det_arr == 0]),
              np.int64(ts_arr[det_arr == 1]),
              np.int64(ts_arr[det_arr == 2]),]


# - List of time-difference arrays for single det ts_ext - #
ts_012_diff_lst = [np.diff(ts_012_lst[0]),
                   np.diff(ts_012_lst[1]),
                   np.diff(ts_012_lst[2]),]

 ################# PLOT HISTOGRAMS AND ESTIMATE FREQUENCY
 

#%% CHECK EVs QUALITY

good_idx_arr = np.array([], dtype = 'int64') # <-- will contain idx of 3-grouped events that pass the quality check

bad_evs_dct = {
                "empty frame": [],
                "multiple isolated single pixels": [],
                "multiple isolated single pixels + clusters": [],
                "kinky clusters": [],
                "multiple clusters": [],
              }

""" Legend of selected classification keys:
    
   0 --> Empty Matrix frame  (<== must no occur with .detsync data! But let's just be sure)
   1 --> multiple isolated single pixels
   2 --> multiple isolated single pixels + clusters
   3 --> Possible kinky
   4 --> multiple clusters
"""

# - Initialize scan - #
j = 0

print("\nClassifying events by clusters ...")

while j < len(frames_lst): # <-- loop on all frames
    
    temp_frame = frames_lst[j] # <-- get current frame
    
    # - Run my PACMAN algorithm - #
    clst_lst, coords_arr, coords_alone = ZG_PACMAN(frame = temp_frame, pix_radius = 1, look_mode = 'diag')
    
    if len(clst_lst) == 0: # <-- no clusters found
    
       if coords_alone.shape[0] == 0: # <-- It must NOT happen (no empty frames should appear in detsync files)

          bad_evs_dct["empty frame"].append(j)  

       elif coords_alone.shape[0] > 0: # <-- case of isolated pixels
    
            if coords_alone.shape[0] > 1: # <-- case of more than 1 isolated pixels

               bad_evs_dct["multiple isolated single pixels"].append(j) 
               
               j += 3 - np.mod(j,3) # <-- pass to the next group of frames
               
            elif coords_alone.shape[0] == 1: 
               
               if np.mod(j,3) == 2: # <-- if I got to the last frame of the group I save the idx of the 1st (must be a multiple of 3)
                 
                  good_idx_arr = np.concatenate((good_idx_arr, np.array([j-2])), axis = 0) 

               j += 1
    
    elif len(clst_lst) == 1: # <-- case of 1 cluster
          
       if coords_alone.shape[0] > 0: # <-- if there are also single pixels fired

          bad_evs_dct["multiple isolated single pixels + clusters"].append(j) 
          
          j += 3 - np.mod(j,3)   
      
       elif coords_alone.shape[0] == 0: # <-- if there are no other pixels fired in addition to the cluster
           
          temp_dens, temp_area = density_clst_coords(coords_arr) # <-- calculate the density of fired pixels in the cluster
           
          if temp_dens < kink_thr: 

             bad_evs_dct["kinky clusters"].append({"idx": j, "density": temp_dens})  
             
             j += 3 - np.mod(j,3)   
          
          elif temp_dens >= kink_thr: # <-- if cluster-event is not seen as kinky
             
             if np.mod(j,3) == 2:
                 
                good_idx_arr = np.concatenate((good_idx_arr, np.array([j-2])), axis = 0)  

             j += 1
 
    elif len(clst_lst) > 1: # <-- case of multi-clusters

       bad_evs_dct["multiple clusters"].append(j) 
       
       j += 3 - np.mod(j,3)
       

print("Done")


#%% COMs --- Points of Impact 

# - Initialize array to contain coordinates of impacts' COMs -#
xy_coords_COM_arr = np.zeros(shape = [3, good_idx_arr.size, 2]) # <-- 3D for 0-1-2; 1st col: x, 2nd col: y

# --- Initialize array to contain clusters' dimensions ---#
xy_clst_dim_arr = np.zeros(shape = [3, good_idx_arr.size, 2]) # <-- 3D for 0-1-2; 1st col: x-size, 2nd col: y-size

print("\nSaving points of impact and clusters' dimensions ...")

# - Loop on evs that have 1 pixel or 1 cluster fired - # (GOOD events) <-- no need to run PACMAN again
for i, idx in enumerate(good_idx_arr):   
    
    for j in range(idx, idx+2 + 1, 1):

        temp_frame = frames_lst[j]

        temp_coords_arr = np.transpose(np.vstack(np.nonzero(temp_frame)))

        temp_means = np.mean(temp_coords_arr, axis = 0) + 0.5 # <-- 0.5 necessary to align with pixel center                          

        xy_coords_COM_arr[np.mod(j,3), i, 0], xy_coords_COM_arr[np.mod(j,3), i, 1] = temp_means
        
        xy_clst_dim_arr[np.mod(j,3), i, 0] = temp_coords_arr[:,0].max() - temp_coords_arr[:,0].min() + 1
        xy_clst_dim_arr[np.mod(j,3), i, 1] = temp_coords_arr[:,1].max() - temp_coords_arr[:,1].min() + 1

print("Done")


#%% RESIDUALS

## --        (x,y) ==> (rows, cols)        -- ##
## -- 1 pixel = 25 um  ==>  1mm = 40 pixel -- ##

## -- Distances: pixels --> mm -- ##
x2_arr = xy_coords_COM_arr[2, :, 0] / 40
x0_arr = xy_coords_COM_arr[0, :, 0] / 40

y2_arr = xy_coords_COM_arr[2, :, 1] / 40
y0_arr = xy_coords_COM_arr[0, :, 1] / 40

x1_true_arr = xy_coords_COM_arr[1, :, 0] / 40
y1_true_arr = xy_coords_COM_arr[1, :, 1] / 40

# --- 3D Geometry Formulas --- #
x1_expc_arr = (z_01 / z_02) * x2_arr + (1 - z_01 / z_02) * x0_arr # <-- already in mm
y1_expc_arr = (z_01 / z_02) * y2_arr + (1 - z_01 / z_02) * y0_arr

res_x_arr = (x1_expc_arr - x1_true_arr) 
res_y_arr = (y1_expc_arr - y1_true_arr)

dist_xy_arr = sqrt(res_x_arr ** 2 + res_y_arr ** 2)

    
n_bins_2D = 2000
   
temp_extent_factor = -0.3
 
# - Building x vs x Distribution - #
range_2D_xy = ((min(res_x_arr.min(), res_y_arr.min()), max(res_x_arr.max(), res_y_arr.max())),
               (min(res_x_arr.min(), res_y_arr.min()), max(res_x_arr.max(), res_y_arr.max())))
 
offset_v = np.abs(temp_extent_factor * (range_2D_xy[0][1] - range_2D_xy[0][0]))
offset_h = np.abs(temp_extent_factor * (range_2D_xy[1][1] - range_2D_xy[1][0]))
 
range_2D_xy = ((range_2D_xy[0][0] - offset_h, range_2D_xy[0][1] + offset_h),
               (range_2D_xy[1][0] - offset_v, range_2D_xy[1][1] + offset_v))


counts_2D_xy, bins_2D_xz, bins_2D_yz = np.histogram2d(res_x_arr, res_y_arr, bins = n_bins_2D, 
                                                      range = range_2D_xy)

binc_2D_xz = bins_2D_xz[:-1] + 0.5 * np.diff(bins_2D_xz)
binc_2D_yz = bins_2D_yz[:-1] + 0.5 * np.diff(bins_2D_yz)



# -- Plotting -- #

# - Some graphic parameters to draw a circle - #
theta = np.linspace(0 , 2*np.pi, num = 100)
 
radius = 0.09 # <-- udm of radius to draw around noisy pixels
 
a = radius * np.cos(theta)
b = radius * np.sin(theta)
# ---------------------------------------------------------------- #


last_fig_idx += 1
    
fig, axs = plt.subplots(num = last_fig_idx, figsize = my_sqrd_fig_size)      
   
fig.suptitle("Residuals", fontsize = my_sup_title_fs, fontdict = my_font_sup) 
 
axs.set_title("$n_{bins}$ =" + f"{n_bins_2D}"
             "; $n_{evs}$ = " + f"{counts_2D_xy.sum():.0f}", fontdict = my_std_font)
 
axs.set_xlabel('Res$_x$ [mm]', fontdict = my_std_font)
axs.set_ylabel('Res$_y$ [mm]', fontdict = my_std_font)


temp_image = axs.imshow(counts_2D_xy.T, norm = mpl.colors.LogNorm(), cmap = my_cmap, 
                        origin = 'lower', extent = range_2D_xy[0] + range_2D_xy[1])

axs.plot(a, b, c = 'lime', marker = ',', ms = 5, ls = '--', lw = 1.5)
  
axs.hlines(0, range_2D_xy[0][0], range_2D_xy[0][1], colors = "c", ls = '--', lw = 1)
axs.vlines(0, range_2D_xy[1][0], range_2D_xy[1][1], colors = "c", ls = '--', lw = 1)
      
# ****************** #
axs.set_xlim([-1, 1])
axs.set_ylim([-1, 1])
# ****************** #

fig.colorbar(temp_image, ax = axs, fraction = 0.10, shrink = 0.75, location = 'right', extend = 'both') 
   
# ax.legend(prop = my_leg_font, title_fontsize = my_leg_title_fs, title = "Legend", loc = "best")
    
fig.show()       


#%% ROTATION

temp_x1_arr = y1_true_arr.copy()
temp_y1_arr = res_x_arr.copy()

temp_x2_arr = x1_true_arr.copy()
temp_y2_arr = res_y_arr.copy()

# temp_extent_factor = 0.15
 
last_fig_idx += 1 
   
fig, (axs_1, axs_2) = plt.subplots(1, 2, num = last_fig_idx, figsize = my_rect_fig_size)
   
fig.suptitle("Multi-Sensor Analysis", fontsize = my_sup_title_fs, fontdict = my_font_sup)
  
  
axs_1.set_title ('Res$_x$ vs y', fontdict = my_std_font)
axs_1.set_xlabel('[mm]', fontdict = my_std_font)
axs_1.set_ylabel('[mm]', fontdict = my_std_font)
  
plt.rcParams['xtick.labelsize'] = my_xy_tick_size
plt.rcParams['ytick.labelsize'] = my_xy_tick_size  
  
  
temp_x_lim_sx = temp_x1_arr.min() - temp_extent_factor * np.abs(temp_x1_arr.max() - temp_x1_arr.min())
temp_x_lim_dx = temp_x1_arr.max() + temp_extent_factor * np.abs(temp_x1_arr.max() - temp_x1_arr.min())
      
temp_y_lim_sx = temp_y1_arr.min() - temp_extent_factor * np.abs(temp_y1_arr.max() - temp_y1_arr.min())
temp_y_lim_dx = temp_y1_arr.max() + temp_extent_factor * np.abs(temp_y1_arr.max() - temp_y1_arr.min())
      
temp_x_lim = (temp_x_lim_sx, temp_x_lim_dx)
temp_y_lim = (temp_y_lim_sx, temp_y_lim_dx)   
  
# axs_1.set_xlim(temp_x_lim)
# axs_1.set_ylim(temp_y_lim)
  
# axs_1.set_xlim([-15, 15])
axs_1.set_ylim([-15, 15])
  
axs_1.plot(temp_x1_arr, temp_y1_arr, c = 'c', marker = '+', ms = 10, ls = '', lw = 1,
           label = "trend plot")
  
axs_1.hlines(0, temp_x_lim_sx, temp_x_lim_dx, colors = "k", ls = '--', lw = 1)

axs_1.grid(ls = ':')
axs_1.legend(prop = my_leg_font, title_fontsize = my_leg_title_fs, title = "Legend", loc = "best")
      
  
axs_2.set_title ('Res$_y$ vs x', fontdict = my_std_font)
axs_2.set_xlabel('[mm]', fontdict = my_std_font)
axs_2.set_ylabel('[mm]', fontdict = my_std_font)
 
plt.rcParams['xtick.labelsize'] = my_xy_tick_size
plt.rcParams['ytick.labelsize'] = my_xy_tick_size  
  
  
temp_x_lim_sx = temp_x2_arr.min() - temp_extent_factor * np.abs(temp_x2_arr.max() - temp_x2_arr.min())
temp_x_lim_dx = temp_x2_arr.max() + temp_extent_factor * np.abs(temp_x2_arr.max() - temp_x2_arr.min())
      
temp_y_lim_sx = temp_y2_arr.min() - temp_extent_factor * np.abs(temp_y2_arr.max() - temp_y2_arr.min())
temp_y_lim_dx = temp_y2_arr.max() + temp_extent_factor * np.abs(temp_y2_arr.max() - temp_y2_arr.min())
      
temp_x_lim = (temp_x_lim_sx, temp_x_lim_dx)
temp_y_lim = (temp_y_lim_sx, temp_y_lim_dx)   
  
# axs_2.set_xlim([-15, 15])
axs_2.set_ylim([-10, 10])
  
axs_2.plot(temp_x2_arr, temp_y2_arr, c = 'm', marker = '+', ms = 10, ls = '', lw = 1,
           label = 'trend plot')

axs_2.hlines(0, temp_x_lim_sx, temp_x_lim_dx, colors = "k", ls = '--', lw = 1)
  
axs_2.grid(ls = ':')
axs_2.legend(prop = my_leg_font, title_fontsize = my_leg_title_fs, title = "Legend", loc = "best")
 
  
fig.show()

#%%

# -- First consider all angles, regardless of the pixel number -- # 
ang_inc_arr = 90 - arctan(z_02 / sqrt((x2_arr - x0_arr)**2 + (y2_arr - y0_arr)**2)) * 180/pi

ang_inc_x_arr = np.zeros_like(ang_inc_arr)
ang_inc_y_arr = np.zeros_like(ang_inc_arr)

for i in range(ang_inc_x_arr.size):

    if x2_arr[i] > x0_arr[i]:
   
        ang_inc_x_arr[i] = 90  - arctan(z_02 / (x2_arr[i] - x0_arr[i])) * 180/pi

    elif x2_arr[i] < x0_arr[i]:
   
        ang_inc_x_arr[i] = - (90 - np.abs(arctan(z_02 / (x2_arr[i] - x0_arr[i])) * 180/pi))


for i in range(ang_inc_y_arr.size):

    if y2_arr[i] > y0_arr[i]:
   
        ang_inc_y_arr[i] = 90  - arctan(z_02 / (y2_arr[i] - y0_arr[i])) * 180/pi

    elif y2_arr[i] < y0_arr[i]:
   
        ang_inc_y_arr[i] = - (90 - np.abs(arctan(z_02 / (y2_arr[i] - y0_arr[i])) * 180/pi))






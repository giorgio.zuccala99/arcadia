
""" Script to test the PACMAN alogortithm and its sub-functions """

import numpy as np

import os, time

from itertools import combinations

from matplotlib import pyplot as plt

import seaborn as sns

last_fig_idx  = -1

#%% GENERATE RANDOM MATRIX

# *** Function 0 *** #
def rand_mat(n_pixels_edge, n_fired): 

    """ 
        Generates squared matrices of boolean values to test PACMAN algorithm
    
        n_pixels_edge = # of pixels per row or per column
        n_fired  = n. of random pixels that will be set to 1

    """
    
    # --- INPUT CHECKS --- # 
    if (n_fired > (n_pixels_edge ** 2)): raise(Exception("Il n. di pixel da accendere deve essere minore del numero di celle della matrice"))
    if str(type(n_pixels_edge)) == "<class 'float'>" or n_pixels_edge <= 0: raise(Exception(f"{n_pixels_edge} must be a positive integer\nyou passed n_pixels_edge = {n_pixels_edge}"))
    if str(type(n_fired))  == "<class 'float'>" or n_fired <= 0:  raise(Exception(f"{n_fired} must be a positive integer\nyou passed n_fired = {n_fired}"))
    # -------------------- # 
    
    # - Initialize empty boolean matrix - #
    rand_pix_mat = np.zeros(shape = [n_pixels_edge, n_pixels_edge], dtype = 'bool')
    
    # - Initialize variable to count random fired pixels - #
    pix_count = 0
    
    while pix_count < n_fired: # <-- loops until the matrix is filled with the desired n. of impacts
    
        for i in range(n_pixels_edge): # <-- loop on matrix rows
            
            for j in range(n_pixels_edge): # <-- loop on matrix cols
                
                # - Next condition distributes fired pixels uniformly by rows and avoids to stop on already fired pixels - #
                if (np.random.rand() < 1/n_pixels_edge) and rand_pix_mat[i,j] != 1: 
                   
                   rand_pix_mat[i,j] = 1 # <-- fire this pixel (turns into 1)
        
                   pix_count += 1 # <-- update counter of fired pixels
                   
                   if pix_count == n_fired: # <-- exit loop when n. of desired pixels is reached
                      break
               
            if pix_count >= n_fired: # <-- exit loop when n. of desired pixels is reached
               break
    
    return rand_pix_mat


#%% PACMAN sub-functions

# *** Function 1 *** #
def put_crust(frame, n_layers):
    
    """ Create a "n_layers-crust" of zeros around a given 2D array (not necessarily squared) """
    
    ###        ----------        INPUT CHECKS        ----------        ###
    if len(frame.shape) != 2:             raise(Exception(f"You've passed a {len(frame.shape)}-D array! Only 2D arrays are allowed"))  
    if "int" not in str(type(n_layers)):  raise(Exception(f"'n_layers' must be an integer!\nYou've passed a {str(type(n_layers))} type"))
    if n_layers < 0:                      raise(Exception(f"'n_layers' must be >= 0\nYou've passed n_layers = {n_layers}"))    
    # ------------------------------------------------------------------ #
 
    # ** New array's indexes (nr_f = 'n_rows_final', nc_f = 'n_cols_final') ** #
    nr_f, nc_f = np.array(frame.shape) + 2 * n_layers 
     
    # ** Initialize new array ** #
    frame_crusted = np.zeros(shape = (nr_f, nc_f), dtype = str(frame.dtype)) # <-- Keeps the same type of input array

    # ** Fill the new array with the old one in the middle of the crust ** #
    frame_crusted[n_layers:(nr_f - n_layers), n_layers:(nc_f - n_layers)] = frame.copy() 

    return frame_crusted


# *** Function 2 *** #
def combs(arr, k):
    """ Samples combinations without repetitions from input array elements into class k """
    return list(combinations(arr, k)) # <-- 'combinations' come from itertools python module


# *** Function 3 *** #
def pixel_compatibles(coords_1, coords_2, pix_radius = 1, look_mode = 'cross'):
 
    """ Tells if the two pixels with coordinates coords_1 and coords_2 are compatible to join the same cluster
    
        coords_1 = python iterable with 2 elements: 1st element = row coord, 2nd element = column coord
        coords_2 = ...
        
        pix_radius = radius of distance sorrounding the two pixels where the function looks. Must be a positive integer
    
        look_mode = choose wether to look in the space sorrounding pixels 'diagonally' or like a cross    <- ^ ->
    """ 

    ###        ----------        INPUT CHECKS        ----------        ###
    if "int" not in str(type(pix_radius)):           raise(Exception(f"'pix_radius' must be an integer!\nYou've passed a {str(type(pix_radius))} type"))
    if pix_radius <= 0:                              raise(Exception(f"'pix_radius' must be a positive integer\nYou've passed pix_radius = {pix_radius}"))    
    if look_mode != 'diag' and look_mode != 'cross': raise(Exception(f"Watch out the spelling of 'look_mode': allowed args are 'cross' o 'diag'\nTou've written {look_mode}"))
    # ------------------------------------------------------------------ #
        
    r1, c1 = coords_1         ;  r2, c2 = coords_2        # <-- Unpacking the input coordinates for each iterable
    
    dist_r = np.abs(r1 - r2)  ;  dist_c = np.abs(c1 - c2) # <-- Get distances along rows and columns
    
    if dist_r <= pix_radius and dist_c <= pix_radius and (dist_r != 0 or dist_c != 0): # <-- They must not be both equal to 0, otherwise they're the same pixel
        
       if look_mode == 'cross':
           
          if dist_r != dist_c:   compatibles = True

          else:                  compatibles = False

       elif look_mode == 'diag': compatibles = True
       
    else:                        compatibles = False

    return compatibles


# *** Function 4 *** #
def delete_rep(input_seq): 
    
    """ Returns a list-like copy of the input object (a python iterable) but without repeated elements"""
    
    output_seq = [] # <-- initialize output list
    
    for el in input_seq: # <-- scan input elements
        
        if el not in output_seq: output_seq.append(el) # <-- attach them if not already attached

    return output_seq


# *** Function 5 *** #
def share_element(seq_1, seq_2):

    """ Returns "True" if seq_1 and seq_2 (lists or tuples) share at least 1 element """    

    check_common = False

    for el in seq_1:
        
        if el in seq_2: check_common = True 
            
    return check_common # , el # <-- un-comment last output if wanna return also the common element 


#%% PACMAN

def ZG_PACMAN(frame, pix_radius = 1, look_mode = 'cross'):
    
    """ PACMAN ALGORITHM:    
        -) frame = 2D squared array
        -) pix_radius = n. of pixels to include inside the cluster starting from the centre
        -) look_mode = 'cross' or 'diag' --> specifies wether to look in the space sorrounding pixels 'diagonally' or like a cross

        Algorithm's tested speed [s] = (n_pix_fired / 1000) ^ 2
        
        Returns a list containing coordinates for all the different clusters types
    """
    
    ###        ---        Checks for INPUT Conditions        ---        ###
    if str(type(frame)) != "<class 'numpy.ndarray'>": raise(Exception(f"Input frame must be a numpy array\nYou passed a {type(frame)} type"))
    if len(frame.shape) != 2:                         raise(Exception(f"You've passed a {len(frame.shape)}-D array! Only 2D arrays are allowed"))  
    if frame.shape[0] != frame.shape[1]:              raise(Exception("INPUT array must be an array with equal number of rows and columns\nYou passed an object with shape {frame.shape}"))    
    if "int" not in str(type(pix_radius)):            raise(Exception(f"'pix_radius' must be an integer!\nYou've passed a {str(type(pix_radius))} type"))
    if pix_radius <= 0:                               raise(Exception(f"'pix_radius' must be a positive integer\nYou've passed pix_radius = {pix_radius}"))    
    if look_mode != 'diag' and look_mode != 'cross':  raise(Exception(f"Watch out the spelling of 'look_mode': allowed args are 'cross' o 'diag'\nYou've written {look_mode}"))
    # -------------------------------------------------------------------- #
   
    # - I look for fired pixels inside the input frame - #
    coords_ones = np.nonzero(frame) # <-- returns 2 array: rows idxs and cols idxs
    
    # - I join the un-paired coordinates in a list of (row, col) pairs  - #
    coords_lst = [[coords_ones[0][i], coords_ones[1][i]] for i in range(coords_ones[0].size)]
    
    # - I create all the possible pairs (without repetition) of fired pixels - # 
    coords_combs_lst = combs(coords_lst, 2) # <-- *** (calling external function)
    
    friends_lst = [] # <-- dynamic list to contain the couples of "friend" pixels (i.e. compatibles to join the same cluster)
    
    for pair in coords_combs_lst: # <-- Loops on the sampled combinations
        
        # - Compatibility check - #  
        check = pixel_compatibles(pair[0], pair[1], pix_radius = pix_radius, look_mode = look_mode) # <-- *** (calling external function)
    
        if check == True: friends_lst.append(pair)


    # -- Now there are common elements inside the updated list --- The iterative part begins -- #
    finish = False # <-- Constrain to enter next 'while' loop
    
    # - Next loop goes on as long as there are common elements to pair inside 'friends_lst'   #
    #   otherwise the variable 'finish' is not set again to "False" and so the loop breaks    #
    #   ending the search for clusters                                                      - #
    
    while finish is False: # <-- Let's search
                
        common = False ; finish = True # <-- Conditions for next 'for' loops
        
        for i, friends_fixed in enumerate(friends_lst): # <-- Fixed element
            
               for j, friends_mobile in enumerate(friends_lst): # <-- Mobile element
        
                   if i != j: # <-- to avoid comparing an element with itself
                                     
                      common = share_element(friends_fixed, friends_mobile) # <-- *** (calling external function) 
        
                      if common == True: # <-- In case there is a common element
            
                         finish = False # <-- Tells the loop not to stop, as some common friends were found                
                         
                         # - The two lists are joined together removing the repeated common friend - #
                         friends_lst[i] = delete_rep(list(friends_fixed) + list(friends_mobile)) # <-- *** (calling external function)

                         friends_lst.remove(friends_mobile) # <-- Removes the mobile coordinates that just joined the fixed ones
                                                  
                         break # <-- As the list has been modified I must restart the fixed-element loop
                                    
                         
    # -- Once the search has finished I put the arrays of clusters' coordinates inside a list -- #         
    clusters_lst = [np.array(temp_lst, dtype = 'int64') for temp_lst in friends_lst] # <-- 1st OUTPUT
    
    # - I can also save coordinates from all clusters in one single array - #
    clst_coords_arr = []  ;  clst_coords_lst = []

    if len(clusters_lst) > 0: # <-- If I found at least 1 cluster I join all their coordinates in one array
        
       clst_coords_arr = np.vstack(clusters_lst) # <-- 2nd OUTPUT
        
       clst_coords_lst = [list(row) for row in clst_coords_arr] # <-- I also convert it into a list for later use
       
       
    # - This last part saves also the array of coordinates of alone pixels (i.e. not belonging to any found cluster) - #    
    coords_alone_lst = [coords for coords in coords_lst if coords not in clst_coords_lst]
    
    # -- It's good to store the alone pix coordinates inside an array, as I don't have to differentiate the single-pix-clusters they belong to -- #
    coords_alone_arr = np.array([]) # <-- 3rd OUTPUT 
    
    if len(coords_alone_lst) > 0: # <-- If I've actually found single clusters
    
       coords_alone_arr = np.array(coords_alone_lst, dtype = 'int64') # <-- As I don't need to keep them separated (as for the clusters) I make them an array
    
    return clusters_lst, clst_coords_arr, coords_alone_arr


#%% Generate Random Matrix

plt.close('all')

dim = 30

frame = rand_mat(n_pixels_edge = dim, n_fired = 30)

frame_crust = put_crust(frame = frame, n_layers = 0)


#%% Finding clusters

t_0 = time.time_ns()

clst_lst, clst_coords_arr, coords_alone = ZG_PACMAN(frame, pix_radius = 15, look_mode = 'diag')

print(f"{dim} x {dim} matrix: {(time.time_ns() - t_0)/10**6:.3f} ms")

#%% Plot boolean matrix with COMS

x_COMs = []
y_COMs = []


if len(clst_lst) > 0: # <-- Caso con clusters
    
   for temp_arr in clst_lst:
       
       x_COMs.append(np.mean(temp_arr, axis = 0)[1] + 0.5)
       y_COMs.append(np.mean(temp_arr, axis = 0)[0] + 0.5)



if str(type(coords_alone)) == "<class 'numpy.ndarray'>": # <-- Se ci sono anche pixel singoli accesi
    
   for temp_pix_row in coords_alone:
       
       x_COMs.append(temp_pix_row[1] + 0.5)
       y_COMs.append(temp_pix_row[0] + 0.5)

last_fig_idx += 1


plt.figure(last_fig_idx, figsize=(10,10))

plt.title(f'Test Pacman - Boolean random matrix {last_fig_idx}', fontsize = 25)
sns.heatmap(frame, cmap = "inferno", cbar = False) 

plt.plot(x_COMs, y_COMs, c = 'r', marker = 'o', ms = 15, ls = '')

if len(clst_lst) > 0: # <-- just for plotting pixels inside clusters
    
   for temp_arr in clst_lst:
       
       plt.plot(temp_arr[:, 1] + 0.5, temp_arr[:, 0] + 0.5, c = 'c', marker = 'o', ms = 10, ls = '-')

plt.show()

#%% Test Computational Time

dim_lst = [el for el in range(10, 50 + 10, 10)]

# dim_lst.append(200) # <-- aggiungere anche sqrt(30000) e sqrt(35000) per completare meglio il plot


fired_perc = 0.5 # <-- percentage of pix randomly fired inside the empty matrix

n_fired_lst = [int(fired_perc * (el**2)) for el in dim_lst]


time_pacman_lst  = [] # <-- contains time in ms


for i, temp_dim in enumerate(dim_lst):
    
    temp_n_fired = n_fired_lst[i]
        
    print(f"Iteration {i+1}/{len(dim_lst)}")

    temp_frame = rand_mat(row_dim = temp_dim, n_fired = temp_n_fired)
    
    temp_t_0 = time.time_ns()
    
    temp_outputs = ZG_PACMAN(temp_frame, pix_radius = 1, look_mode = 'diag')

    time_pacman_lst.append((time.time_ns() - temp_t_0)/10**9)
 

time_pacman_arr = np.array(time_pacman_lst)

# np.save(main_folder + "Arcadia - Codes\Test Codes\saved arrays/" + "time_pacman_arr.npy", time_pacman_arr) 

#%%

dim_lst = [el for el in range(10, 150 + 10, 10)]

dim_lst.append(200) # <-- aggiungere anche sqrt(30000) e sqrt(35000) per completare meglio il plot

fired_perc = 0.5 # <-- percentage of pix randomly fired inside the empty matrix

n_fired_lst = [int(fired_perc * (el**2)) for el in dim_lst]


n_fired_arr = np.array(n_fired_lst)

time_pacman_arr = np.load(main_folder + "Arcadia - Codes\Test Codes\saved arrays/" + "time_pacman_arr.npy")

last_fig_idx += 1

plt.figure(last_fig_idx, figsize = my_rect_fig_size)

plt.suptitle('Test Pacman', fontsize = my_sup_title_fs, fontdict = my_font_sup)

plt.title('Computational Time trend', fontdict = my_std_font)
plt.xlabel('# pix fired', fontdict = my_std_font)
plt.ylabel('Elapsed time (s)', fontdict = my_std_font)

plt.xscale('log')
plt.yscale('log')

temp_err = np.zeros_like(time_pacman_arr)
# temp_err = np.sqrt(time_pacman_arr)

plt.errorbar(n_fired_arr, time_pacman_arr, temp_err, c = 'b', ecolor = 'k', marker = '<', ms = 15, ls = '', 
             barsabove = True, capsize = 1, elinewidth = 1, markeredgewidth = 1,
             label = 'fired pixels = 50% of matrix')

plt.grid(ls = 'solid')
plt.legend(prop = my_leg_font, title_fontsize = my_leg_title_fs, title = "Legend", loc = "best")

plt.show()
    
### Plot Display
if show_plots == True:    
  plt.show()

elif show_plots == False:    
  plt.close(last_fig_idx)

### Salvataggio figure
if show_plots == True and save_figs == True:
   plt.savefig(figures_folder + "PACMAN_comp_time")
       
#%% FITTING

from scipy.optimize import curve_fit as cf

plt.close('all')

# def time_func(x, a, b, c): 
#     return a*(x**2)*np.log(b * x) + c
    
def time_func(x_arr, a, b, c): 
    
    output_arr = np.empty(shape = [0,])
    
    for el in x_arr: 
        
        output_arr = np.concatenate((output_arr, np.array([a*np.math.factorial(el-b) + c,])))
    
    return output_arr

temp_y, temp_x = np.array(time_pacman_lst, dtype = 'uint32'), np.array(n_fired_lst, dtype = 'uint32')
    
# temp_x_range = (0, 1e6)

# temp_cond_x = (temp_x >= temp_x_range[0]) & (temp_x <= temp_x_range[1])

# temp_x = temp_x[temp_cond_x]
# temp_y = temp_y[temp_cond_x] 

# temp_cond_y = (temp_y > 0)
 
# temp_x = temp_x[temp_cond_y]
# temp_y = temp_y[temp_cond_y]

err_temp_y = np.sqrt(temp_y)

temp_p0 = (0.1, 0, 0)

temp_popt, temp_pcov =  cf(time_func, temp_x, temp_y, p0 = temp_p0, 
                           bounds = ((0.00001, 0, 0), (1, 0.5, np.inf)), 
                           absolute_sigma = True, sigma = err_temp_y, maxfev = 1000) 

err_temp_popt = np.sqrt(np.diag(temp_pcov))

temp_chi_sq_red = ((((temp_y - time_func(temp_x, *temp_popt))**2)/temp_y).sum()) / (temp_y.size - 2)
        
#% FITTING + PLOT
  
plt.close('all')

# temp_x_fit = np.linspace(temp_x[0], temp_x[-1] + 5000, 10000)
temp_x_fit = temp_x.copy()


last_fig_idx += 1

plt.figure(last_fig_idx, figsize = my_rect_fig_size)

plt.suptitle('Test Pacman', fontsize = my_sup_title_fs, fontdict = my_font_sup)

plt.title('Computational Time trend', fontdict = my_std_font)
plt.xlabel('# pix fired', fontdict = my_std_font)
plt.ylabel('Elapsed time (s)', fontdict = my_std_font)

plt.plot(temp_x_fit, time_func(temp_x_fit, *temp_p0), c = 'g', marker = 'P', ms = 10, ls = '--', lw = 2, 
          label = "Starting Points")

plt.errorbar(temp_x, temp_y, err_temp_y, c = 'b', ecolor = 'k', marker = 'v', ms = 10, ls = '', 
             barsabove = True, capsize = 5, elinewidth = 2, markeredgewidth = 2,
             label = 'fired pixels = 50% of matrix')

plt.plot(temp_x_fit, time_func(temp_x_fit, *temp_popt), c = 'r', ms = 5, ls = '--', lw = 2, 
          label = "fitting \t $y = a \cdot x \cdot ln(b \cdot x) + c$\n$\chi^{2}_{red}$ = " + f"{temp_chi_sq_red:.2f}")

plt.grid(ls = 'solid')
plt.legend(prop = my_leg_font, title_fontsize = my_leg_title_fs, title = "Legend", loc = "best")

plt.show()
    
### Plot Display
if show_plots == True:    
  plt.show()

elif show_plots == False:    
  plt.close(last_fig_idx)

### Salvataggio figure
if show_plots == True and save_figs == True:
   plt.savefig(figures_folder + "PACMAN_comp_time_expfit")
       


""" 
    Code to unpack manually single sensors data

-) Unpack from .csv --> 1st array and 2nd DF
-) Unpack from .txt --> 1st DF    and 2nd array

Single sensors data contain too many spurious events to be decoded singularly;
therefore they are only decoded when comparing their match with detsync data
    
"""

# -) txt data have been sample starting from .csv data <-- (cfr. code ZG_Arcadia_funcs.py)

import os, time

print("Ciao Giorgio")

#%% 0) PREPARATION

""" Personal Folders Handling """
PC_str = r'C:\GIORGIO pc Acer'
# PC_str = r'D:\Giorgio\+++ Giorgio PC Acer - Cantiere +++\Backup PC - (28-01-2023)'

PC_str = PC_str + r'/'

main_folder = PC_str + r'UNIVERSITÀ\INSUBRIA\MAGISTRALE\1° ANNO\2° semestre\ADVANCED DATA ANALYSIS\Final Analysis/'

csv_folder   = main_folder + r'ARCADIA - Analyzed Data\Old sw\Single Sensor Data\csv format/'
txt_folder   = main_folder + r'ARCADIA - Analyzed Data\Old sw\Single Sensor Data\txt format/'

if (os.path.isdir(csv_folder)   == False): raise(Exception(f"Path \n{csv_folder}\n Not found"))   # <-- Check if input path is valid
if (os.path.isdir(txt_folder)   == False): raise(Exception(f"Path \n{txt_folder}\n Not found"))   # <-- Check if input path is valid

os.chdir(main_folder + 'ARCADIA - Codes/')

#%% 1) MODULES - LIBRARIES

import numpy   as np
import pandas  as pd

#%% 2) Select Acqusition-Decode type

# --- Choose filename to Open --- #
file_to_open = '3_det2'

n_sensors = 3 # <-- n. of sensors implied in the analysis

if  n_sensors == 1:
    columns_lst = ['SR bit',  'Hitmap Byte',  'Core pr', 'Col',   'Sez',         'Ser', 
                   'Falling', 'ts chip',      'ts FPGA', 'ts SW', 'ts extended', 'trigger']
            
elif n_sensors > 1:        
     columns_lst = ['SR bit',  'Hitmap Byte',  'Core pr', 'Col',   'Sez',         'Ser', 
                    'Falling', 'ts chip',      'ts FPGA', 'ts SW', 'ts extended', 'trigger', 'sensor']
 
#%% Unpack --- .csv

t_0 = time.time_ns() # <-- tracks the time


DS_file_original = open(csv_folder + file_to_open + r".csv", "r").readlines() # <-- Apro il DS come file di testo e separo le righe

DS_file = DS_file_original.copy()[1:] # <-- Lavoro su una copia e mantengo l'originale; salto inoltre la prima riga dato che contiene l'header

rows_arrs_lst = [] # <-- Lista dinamica che conterrà gli array 2D

for i, temp_row in enumerate(DS_file): # <-- Scan dei packets per riga

    temp_evs_in_row_str_lst = temp_row[2:-3].split(sep = ')","(') # <-- "[2:-3]" fa skippare i caratteri ' "( ' all'inizio e ' )"\n ' in fondo
    
    print(f"Packet {i+1}/{len(DS_file)}", f"{len(temp_evs_in_row_str_lst)} events")

    temp_evs_arr_lst = [] # <-- Lista di array 1D che andranno a formare il 2D da appendere a 'rows_arrs_lst = []'
    
    for temp_ev_str in temp_evs_in_row_str_lst:
        
        temp_values_lst = temp_ev_str.split(sep = ", ")

        temp_values_lst[6] = 0 # <-- rendo il 'False' = 0
        
        temp_evs_arr_lst.append(np.array(temp_values_lst, dtype = 'uint64')) # <-- Uso 64 bit (mi sembra che non abbia un costo di memoria superiore ai 32)

    rows_arrs_lst.append(np.stack(temp_evs_arr_lst))

DS_arr_csv = np.vstack(rows_arrs_lst) # <-- Merge all the events in a big 2D array

DS_df_csv = pd.DataFrame(DS_arr_csv, columns = columns_lst)


print(f"Elapsed seconds for .csv decode: {((time.time_ns() - t_0)/(10**9)):.2f} seconds")

#%% Unpack --- .txt

t_0 = time.time_ns() # <-- tracks the time


DS_df_txt = pd.read_csv(txt_folder + file_to_open + r".txt", sep = '\t')

DS_arr_txt = DS_df_txt.iloc[:,:].values # <-- Merge all the events in a big 2D array


print(f"SINGLE SENSORS DATA:\nElapsed seconds for .txt unpack: {((time.time_ns() - t_0)/(10**9)):.2f} seconds")



""" 
    Code to unpack manually multiple sensors detsync data

-) Unpack from .txt --> 1st DF and 2nd array
-) Decode unpacked array to build frames

This process is then included in the function

"""

# -) txt data have been sample starting from .csv data <-- (cfr. code ZG_Arcadia_funcs.py)

import os, time

print("Ciao Giorgio")

#%% 0) PREPARATION

""" Personal Folders Handling """
PC_str = r'C:\GIORGIO pc Acer'
# PC_str = r'D:\Giorgio\+++ Giorgio PC Acer - Cantiere +++\Backup PC - (28-01-2023)'

PC_str = PC_str + r'/'

main_folder = PC_str + r'UNIVERSITÀ\INSUBRIA\MAGISTRALE\1° ANNO\2° semestre\ADVANCED DATA ANALYSIS\Final Analysis/'

csv_folder   = main_folder + r'ARCADIA - Analyzed Data\Old sw\Detsync Data\csv format/'
txt_folder   = main_folder + r'ARCADIA - Analyzed Data\Old sw\Detsync Data\txt format/'

if (os.path.isdir(csv_folder)   == False): raise(Exception(f"Path \n{csv_folder}\n Not found"))   # <-- Check if input path is valid
if (os.path.isdir(txt_folder)   == False): raise(Exception(f"Path \n{txt_folder}\n Not found"))   # <-- Check if input path is valid

os.chdir(main_folder + 'ARCADIA - Codes/')

#%% 1) MODULES - LIBRARIES

import numpy   as np
import pandas  as pd

#%% Unpack --- .txt

# --- Choose filename to Open --- #
file_to_open = '5_detsync'


t_0 = time.time_ns() # <-- tracks the time


DS_df_txt = pd.read_csv(txt_folder + file_to_open + r".txt", sep = '\t')

DS_arr_txt = DS_df_txt.iloc[:,:].values # <-- Merge all the events in a big 2D array


print(f"DETSYNC DATA:\nElapsed seconds for .txt unpack: {((time.time_ns() - t_0)/(10**9)):.2f} seconds")

#%% File Decoding --- Getting 512 x 512 frames
 
print("DECODING...")

# --- FIRED PIXEL DECODING --- #
frame_arr_lst = []

pix_info_lst = []

t_0 = time.time_ns()

i = -1

while True:   

    i += 1
            
    print(f"\nRiga i = {i}")
    
    temp_frame_arr = np.zeros(shape = [512, 512], dtype = "uint8") # <-- Cerco di occupare meno memoria possibile
    
    j = i # <-- Posizione del pixel mobile per il controllo dell'elemento corrente con quello successivo
    
    if i < (DS_arr_txt.shape[0] - 1): # <-- Passo a controllare l'elemento successivo solo se non sono arrivato in fondo all'array
    
       while (DS_arr_txt[j, -1] == DS_arr_txt[j + 1, -1]): # <-- Confronto l'ultimo valore di una riga con quello della riga successiva
                                                   # Se le colonne sono 12 (sensore singolo) sto guardando il trigger,
                                                   # se invece sono 13 (sensore multiplo) sto guardando il n. del sensore
                           
          j += 1 # <-- Se i due trigger sono uguali incremento il contatore per guardare anche il successivo
           
          if j == (DS_arr_txt.shape[0] - 1): # <-- Se raggiungo la fine del DS devo interrompere l'incremento
             print("\n ##### End of DS Reached --> Stop comparing current trigger with next trigger #####") 
             break # <-- Esco dal while
       
    print(f"\nRows with same trig = {j - i + 1}\n") # <-- Dico quanti erano gli eventi con lo stesso trigger
   
    temp_pix_fired_num = 0 
   
    for i in range(i, j + 1): # <-- Ciclo sui trigger uguali rinvenuti

        ### Estraggo i parametri geometrici per decodificare i pixel accesi
        sez = np.uint64(DS_arr_txt[i, 4]) # <-- Valore (0, 15)
        col = np.uint64(DS_arr_txt[i, 3]) # <-- Valore (0, 15) 
                
        core_pr = np.uint64(DS_arr_txt[i, 2]) # <-- Valore (0, 127)    
    
        x_start = np.uint64(32*sez + 2*col) # BOTTOM LEFT
        y_start = np.uint64(4*core_pr)      # BOTTOM LEFT
    
        hitmap = np.uint64(DS_arr_txt[i, 1]) # <-- Valore (0, 255)
    
        hitmap_bit_str = np.binary_repr(hitmap) # <-- Stringa di cifre booleane in MSBfirst (i.e. il bit più a SX ha il massimo esponente)
        
        temp_byte_arr = np.flip(np.array([int(bool_digit) for bool_digit in hitmap_bit_str])) # <-- La stringa di booleani diventa un array in LSBFIRST
        
        pix_fired = np.where(temp_byte_arr == 1)[0]

        for bit_pos in pix_fired: # <-- Loops along the found pix fired positions   
             
            x_fired = np.uint64(x_start + np.mod(bit_pos, 2))     # <-- Setting col of fired pixel
            y_fired = np.uint64(511 - (y_start + (bit_pos // 2))) # <-- Setting row of fired pixel: y --> col = 511 - y
  
            temp_frame_arr[y_fired, x_fired] = 1            
        
        temp_pix_fired_num += pix_fired.size
        
        print(f"Pixel Decode --> ROW i = {i} --> {pix_fired.size} pix fired")              
                            
    print(f"\nEvent {len(frame_arr_lst)} saved --- {temp_pix_fired_num} total pix fired\n") # <-- Essendo in Python il 1° evento lo conto come '0'
 
    temp_pix_fired = np.nonzero(temp_frame_arr)[0].size

    pix_info_lst.append(temp_pix_fired)
    
    frame_arr_lst.append(temp_frame_arr) # aggiungo alla lista di frame solo quelli in cui si è acceso un n. di pixel sopra soglia
  
    if i == (DS_arr_txt.shape[0] - 1): 
       print(f"Elapsed seconds for decoding: {((time.time_ns() - t_0)/(10**9)):.2f}")  
       break    



""" Personal functions for ARCADIA data analysis """

#%% MODULES

import os, json # <-- modules to navigate in directories and storing information

import numpy as np, pandas as pd # <-- modules to represent and deal with array of data

from itertools import combinations # <-- essential for clustering (PACMAN) function

from scipy.optimize import curve_fit # <-- used in Cook's distance outliers handling


#%% SAVING ELEMENTS

def ZG_save_element(element, path, name, ovwr = True, save_sep = "\t", save_idx = False, print_msgs = False):
    
    """ Saves element in 'path' with 'name'
        and overwrites it only if cond_ovwr == True.
        Accepted data types:
            
            dictionary      --> .json        
            numpy array     --> .npy
            pandas DataFame --> .csv
        
        'save_sep' and 'save_idx' are used only if saving pandas.DataFrames
        
        No need to specify extension when saving """
        
    # --- INPUT CHECKS --- #    
    if path[-1] != '/': raise(Exception("Output path must be a string-like path ending with a slash /"))    
    # -------------------- #    
    
    data_type_str = str(type(element)) 
     
    # - Dictionary case - #
    if data_type_str == "<class 'dict'>":                        
        
       file_extns = '.json'
       
       if name + file_extns not in os.listdir(path):   

          if print_msgs == True: print(f"{name + file_extns} not present in {path} ==> saving it...")     
           
          with open(path + name + file_extns, "w") as temp_obj:
                        
               json.dump(element, temp_obj) # <-- encode dictionary into JSON file            
       
       elif name + file_extns in os.listdir(path):   
         
          if print_msgs == True: print(f"{name + file_extns} already present in {path}")     

          if ovwr == True:
           
             if print_msgs == True: print("overwrite it") 
           
             with open(path + name + file_extns, "w") as temp_obj:
                            
                  json.dump(element, temp_obj) # <-- encode dictionary into JSON file               

          elif ovwr == False:
           
             if print_msgs == True: print("==> not overwrite it")

    # - nupy array case - #   
    elif data_type_str == "<class 'numpy.ndarray'>":               
        
       file_extns = '.npy'
       
       if name + file_extns not in os.listdir(path):   

          if print_msgs == True: print(f"{name + file_extns} not present in {path} ==> saving it...")     
           
          np.save(path + name + file_extns, element) # <-- save element as numpy array
       
       elif name + file_extns in os.listdir(path):   
         
          if print_msgs == True: print(f"{name + file_extns} already present in {path}")     

          if ovwr == True:
           
             if print_msgs == True: print("==> overwrite it") 
           
             np.save(path + name + file_extns, element) # <-- save element as numpy array               

          elif ovwr == False:
           
             if print_msgs == True: print("==> not overwrite it")        
        
    # - pandas.DataFrame case - #    
    elif data_type_str == "<class 'pandas.core.frame.DataFrame'>": 
        
       file_extns = '.csv'
       
       if name + file_extns not in os.listdir(path):   

          if print_msgs == True: print(f"{name + file_extns} not present in {path} ==> saving it...")     
           
          element.to_csv(path + name + file_extns, sep = save_sep, index = save_idx) # <-- save element as .csv file
          
       elif name + file_extns in os.listdir(path):   
         
          if print_msgs == True: print(f"{name + file_extns} already present in {path}")     

          if ovwr == True:
           
             if print_msgs == True: print("==> overwrite it") 
           
             element.to_csv(path + name + file_extns, sep = save_sep, index = save_idx) # <-- save element as .csv file               

          elif ovwr == False:
           
             if print_msgs == True: print("==> not overwrite it")                 
    
    else: raise(Exception(f"Function only supports dictionaries, numpy.arrays and pandas.dataframes\nYou passed a {data_type_str} data type"))
     
    return None  


#%% PACMAN sub-functions


# *** Function 1 *** #
def combs(arr, k):
    """ Samples combinations without repetitions from input array elements into class k """
    return list(combinations(arr, k)) # <-- 'combinations' come from itertools python module


# *** Function 2 *** #
def pixel_compatibles(coords_1, coords_2, pix_radius = 1, look_mode = 'cross'):
 
    """ Tells if the two pixels with coordinates coords_1 and coords_2 are compatible to join the same cluster
    
        coords_1 = python iterable with 2 elements: 1st element = row coord, 2nd element = column coord
        coords_2 = ...
        
        pix_radius = radius of distance sorrounding the two pixels where the function looks. Must be a positive integer
    
        look_mode = choose wether to look in the space sorrounding pixels 'diagonally' or like a cross    <- ^ ->
    """ 

    ###        ----------        INPUT CHECKS        ----------        ###
    if "int" not in str(type(pix_radius)):           raise(Exception(f"'pix_radius' must be an integer!\nYou've passed a {str(type(pix_radius))} type"))
    if pix_radius <= 0:                              raise(Exception(f"'pix_radius' must be a positive integer\nYou've passed pix_radius = {pix_radius}"))    
    if look_mode != 'diag' and look_mode != 'cross': raise(Exception(f"Watch out the spelling of 'look_mode': allowed args are 'cross' o 'diag'\nTou've written {look_mode}"))
    # ------------------------------------------------------------------ #
        
    r1, c1 = coords_1         ;  r2, c2 = coords_2        # <-- Unpacking the input coordinates for each iterable
    
    dist_r = np.abs(r1 - r2)  ;  dist_c = np.abs(c1 - c2) # <-- Get distances along rows and columns
    
    if dist_r <= pix_radius and dist_c <= pix_radius and (dist_r != 0 or dist_c != 0): # <-- They must not be both equal to 0, otherwise they're the same pixel
        
       if look_mode == 'cross':
           
          if dist_r != dist_c:   compatibles = True

          else:                  compatibles = False

       elif look_mode == 'diag': compatibles = True
       
    else:                        compatibles = False

    return compatibles


# *** Function 3 *** #
def delete_rep(input_seq): 
    
    """ Returns a list-like copy of the input object (a python iterable) but without repeated elements"""
    
    output_seq = [] # <-- initialize output list
    
    for el in input_seq: # <-- scan input elements
        
        if el not in output_seq: output_seq.append(el) # <-- attach them if not already attached

    return output_seq


# *** Function 4 *** #
def share_element(seq_1, seq_2):

    """ Returns "True" if seq_1 and seq_2 (lists or tuples) share at least 1 element """    

    check_common = False

    for el in seq_1:
        
        if el in seq_2: check_common = True 
            
    return check_common # , el # <-- un-comment last output if wanna return also the common element 


#%% PACMAN

def ZG_PACMAN(frame, pix_radius = 1, look_mode = 'cross'):
    
    """ PACMAN ALGORITHM:    
        -) frame = 2D squared array
        -) pix_radius = n. of pixels to include inside the cluster starting from the centre
        -) look_mode = 'cross' or 'diag' --> specifies wether to look in the space sorrounding pixels 'diagonally' or like a cross

        Algorithm's tested speed [s] = (n_pix_fired / 1000) ^ 2
        
        Returns a list containing coordinates for all the different clusters types
    """
    
    ###        ----------        INPUT CHECKS        ----------        ###
    if str(type(frame)) != "<class 'numpy.ndarray'>": raise(Exception(f"Input frame must be a numpy array\nYou passed a {type(frame)} type"))
    if len(frame.shape) != 2:                         raise(Exception(f"You've passed a {len(frame.shape)}-D array! Only 2D arrays are allowed"))  
    if frame.shape[0] != frame.shape[1]:              raise(Exception("INPUT array must be an array with equal number of rows and columns\nYou passed an object with shape {frame.shape}"))    
    if "int" not in str(type(pix_radius)):            raise(Exception(f"'pix_radius' must be an integer!\nYou've passed a {str(type(pix_radius))} type"))
    if pix_radius <= 0:                               raise(Exception(f"'pix_radius' must be a positive integer\nYou've passed pix_radius = {pix_radius}"))    
    if look_mode != 'diag' and look_mode != 'cross':  raise(Exception(f"Watch out the spelling of 'look_mode': allowed args are 'cross' o 'diag'\nYou've written {look_mode}"))
    # ------------------------------------------------------------------ #
   
    # - I look for fired pixels inside the input frame - #
    coords_ones = np.nonzero(frame) # <-- returns 2 array: rows idxs and cols idxs
    
    # - I join the un-paired coordinates in a list of (row, col) pairs  - #
    coords_lst = [[coords_ones[0][i], coords_ones[1][i]] for i in range(coords_ones[0].size)]
    
    # - I create all the possible pairs (without repetition) of fired pixels - # 
    coords_combs_lst = combs(coords_lst, 2) # <-- *** (calling external function)
    
    friends_lst = [] # <-- dynamic list to contain the couples of "friend" pixels (i.e. compatibles to join the same cluster)
    
    for pair in coords_combs_lst: # <-- Loops on the sampled combinations
        
        # - Compatibility check - #  
        check = pixel_compatibles(pair[0], pair[1], pix_radius = pix_radius, look_mode = look_mode) # <-- *** (calling external function)
    
        if check == True: friends_lst.append(pair)


    # -- Now there are common elements inside the updated list --- The iterative part begins -- #
    finish = False # <-- Constrain to enter next 'while' loop
    
    # - Next loop goes on as long as there are common elements to pair inside 'friends_lst'   #
    #   otherwise the variable 'finish' is not set again to "False" and so the loop breaks    #
    #   ending the search for clusters                                                      - #
    
    while finish is False: # <-- Let's search
                
        common = False ; finish = True # <-- Conditions for next 'for' loops
        
        for i, friends_fixed in enumerate(friends_lst): # <-- Fixed element
            
               for j, friends_mobile in enumerate(friends_lst): # <-- Mobile element
        
                   if i != j: # <-- to avoid comparing an element with itself
                                     
                      common = share_element(friends_fixed, friends_mobile) # <-- *** (calling external function) 
        
                      if common == True: # <-- In case there is a common element
            
                         finish = False # <-- Tells the loop not to stop, as some common friends were found                
                         
                         # - The two lists are joined together removing the repeated common friend - #
                         friends_lst[i] = delete_rep(list(friends_fixed) + list(friends_mobile)) # <-- *** (calling external function)

                         friends_lst.remove(friends_mobile) # <-- Removes the mobile coordinates that just joined the fixed ones
                                                  
                         break # <-- As the list has been modified I must restart the fixed-element loop
                                    
                         
    # -- Once the search has finished I put the arrays of clusters' coordinates inside a list -- #         
    clusters_lst = [np.array(temp_lst, dtype = 'int64') for temp_lst in friends_lst] # <-- 1st OUTPUT
    
    # - I can also save coordinates from all clusters in one single array - #
    clst_coords_arr = []  ;  clst_coords_lst = []

    if len(clusters_lst) > 0: # <-- If I found at least 1 cluster I join all their coordinates in one array
        
       clst_coords_arr = np.vstack(clusters_lst) # <-- 2nd OUTPUT
        
       clst_coords_lst = [list(row) for row in clst_coords_arr] # <-- I also convert it into a list for later use
       
       
    # - This last part saves also the array of coordinates of alone pixels (i.e. not belonging to any found cluster) - #    
    coords_alone_lst = [coords for coords in coords_lst if coords not in clst_coords_lst]
    
    # -- It's good to store the alone pix coordinates inside an array, as I don't have to differentiate the single-pix-clusters they belong to -- #
    coords_alone_arr = np.empty(shape = [0, 2]) # <-- 3rd OUTPUT 
    
    if len(coords_alone_lst) > 0: # <-- If I've actually found single clusters
    
       coords_alone_arr = np.array(coords_alone_lst, dtype = 'int64') # <-- As I don't need to keep them separated (as for the clusters) I make them an array
    
    return clusters_lst, clst_coords_arr, coords_alone_arr


#%% DATA CONVERSION .csv --> .txt 

def csv_to_txt_DS(path_to_load, filename, path_to_save, ask_ovwr = True, print_msgs = True):
    
    """
        Takes filename.csv file in 'path_to_load', opens it, adjusts the rows removing the parenthesis
        and unnecessary commas and writes it onto a filename.txt version in 'path_to_save'. The .txt
        version contains columns' names as well, only \t separations and rows with the same length, thus
        making more immediate and convenient a data opening through pandas dataframes or numpy arrays.
        
        'ask_ovwr' = boolean that asks the user if wants to overwrite an already existing file (default is True)
        
    """
  
    ###        ----------        INPUT CHECKS        ----------        ###    
    if (os.path.isdir(path_to_load) == False): raise(Exception("path_to_load specified not found:\n{path_to_load}"))
    if (os.path.isdir(path_to_save) == False): raise(Exception("path_to_save specified not found:\n{path_to_save}"))
   
    if (filename + r".csv" not in os.listdir(path_to_load)): raise(Exception("specified file to convert " + filename + r".csv" + " not found"))
    
    if ask_ovwr   != True and ask_ovwr != False :          raise(Exception(f"ask_ovwr can be set either to True or False\nYou passed ask_ovwr = {ask_ovwr}"))     
   
    if ask_ovwr == True: # <-- if ask_ovwr == False converts directly and overwriting existing files with the same name
   
       if (filename + r".txt" in os.listdir(path_to_save)):
            
          user_char = str(input("\n{filename}.txt already exists --- overwrite?\n" + r"[Y/N]" + "\n")) 
    
          while user_char not in ["y", "Y", "n", "N"]: 
             print("\nInvalid INPUT --- allowed inputs are y,Y or n,N \n")
             user_char = str(input("\n" + r"[Y/N]" + "\n"))
              
          if user_char == 'n' or user_char == 'N': raise(Exception("\nConversion stopped by user\n"))
    
    if print_msgs != True and print_msgs != False :          raise(Exception(f"print_msgs can be set either to True or False\nYou passed print_msgs = {print_msgs}")) 
    # ------------------------------------------------------------------ #
          
    
    ## -- First Unpacking process -- ##
    
    # - Open DS as text and separate lines in a list - #
    DS_file_original = open(path_to_load + filename + r".csv", "r").readlines() # <-- some elements of the list might contain more than group of events
        
    # - Sometimes data don't start with the very first row ==> I must tell to skip until first " indicator - #
    rows_to_skip = 0 # <-- initialize index of rows to skip until I reach the true info
    
    while '"' not in DS_file_original[rows_to_skip]: rows_to_skip += 1 

    DS_file = DS_file_original[rows_to_skip:] # <-- cut the rows with no information


    # - Unpacking DS file: list of strings --> numpy array - #
    columns_lst = ['SR bit',  'Hitmap Byte',  'Core pr', 'Col',   'Sez',         'Ser', 
                   'Falling', 'ts chip',      'ts FPGA', 'ts SW', 'ts extended', 'trigger', 'sensor']
      
    n_cols = len(columns_lst)
    
    DS_arr = np.zeros(shape = [0, n_cols]) # <-- initialize dynamic array
    
    
    if print_msgs == True: print("Begin .csv --> .numpy.array --> .txt conversion")
 
    
    for e, temp_row in enumerate(DS_file): # <-- Loop on data information
                    
        temp_split_lst = temp_row.split(sep = "(")[1:] # <-- get separated where there's "(" and skips it
    
        # - Remove unnecessary characters from the strings in the specified list - #     
        temp_split_lst[-1] = temp_split_lst[-1][:-3] # <-- remove "\n at the end of just the last element
        
        for i in range(len(temp_split_lst) - 1): # <-- removes )"," characters of all the other elements
            
            temp_split_lst[i] = temp_split_lst[i][:-4]
        
        # Now I need a split for ", " between numbers in the text elements --> returns a list of lists    
        temp_sets_str_lst = [el.split(sep = ", ") for el in temp_split_lst]
        
        # - In every sub-list of the list convert True/False in boolean numerical values - #    
        temp_sets_num_lst = [] # <-- new list just for the numerical values
        
        # - Loop to fill the new list - #    
        for i in range(len(temp_sets_str_lst)):
            
            temp_lst = [] # <-- temporary just inside the loop
            
            for j in range(6): # <-- stops before the "False"
                
                temp_lst.append(int(temp_sets_str_lst[i][j])) # <-- conversion to integer
        
            # - Conversion of boolean values - #
            if   (temp_sets_str_lst[i][6]) == "False": temp_lst.append(False)
               
            elif (temp_sets_str_lst[i][6]) == "True":  temp_lst.append(True)
               
            for j in range(7, n_cols): # <-- go on after the boolean values
                temp_lst.append(int(temp_sets_str_lst[i][j]))
        
            temp_sets_num_lst.append(temp_lst)
        
        # - From list to 2D array (boolean --> integer) - #
        temp_arr = np.array(temp_sets_num_lst)
    
        DS_arr = np.concatenate((DS_arr, temp_arr), axis = 0)
        
    # - Get also a DF from array - #
    DS_df = pd.DataFrame(data = DS_arr, columns = columns_lst)
    
    # - Write DF on .txt file - #
    temp_df_str = DS_df.to_csv(None, sep = '\t', index = False) # <-- .csv conversion
    
    temp_txt_file = open(path_to_save + filename + r".txt", "w") # <-- copy-past the .csv object into new .txt file
    
    temp_txt_file.write(temp_df_str)
    
    temp_txt_file.close()
    
    if print_msgs == True: print("Done")
    
    return DS_df


#%% DECODE .txt detsync Data

def decode_detsync_from_txt(path_to_load, filename, max_events = 80000, print_msgs = False):
        
    """ Load .txt detsync data into DataFrame
    
        Works with multi-sensor detsync files without trigger  (13 columns). 
        As there's a data unpacking, many syntaxes are in common with the 
        function 'csv_to_txt_DS'.
        
        Returns: list of frames, array with n. of pixels fired per frame, DS_df, DS_arr
        
        No need to specify extension in 'filename'
        max_events = 80000 represents an empirical threshold to avoid memory overflow
    """
    
    # -) Acquisition software writes an event when there's at least a fired pixel for all 3 planes
    # -) Length of the output list must be a multiple of the n. of sensors

    ###        ----------        INPUT CHECKS        ----------        ###  
    if (os.path.isdir(path_to_load) == False):               raise(Exception("path_to_load specified not found:\n{path_to_load}"))
    if "int" not in str(type(max_events)):                   raise(Exception(f"'max_events' must be an integer!\nYou've passed a {str(type(max_events))} type"))
    if max_events <= 0:                                      raise(Exception(f"'max_events' must be a positive integer\nYou've passed max_events = {max_events}"))    
    if (filename + r".txt" not in os.listdir(path_to_load)): raise(Exception("Cannot find file " + filename + f".txt in specified folder\n{path_to_load}"))
    if print_msgs != True and print_msgs != False :          raise(Exception(f"print_msgs can be set either to True or False\nYou passed print_msgs = {print_msgs}")) 
    # ------------------------------------------------------------------ #
     
    DS_df = pd.read_csv(path_to_load + filename + r".txt", sep = '\t') # <-- Open .txt file as it was saved by 'merged_2D_frame' function
         
    DS_arr = DS_df.iloc[:,:].values # <-- get array from DF
    
    if print_msgs == True: print("Decoding...")
    
    # --- FIRED PIXEL DECODING --- #
    frame_arr_lst = [] # <-- will contain piled-up decoded frames
    
    pix_fired_arr  = np.array([], dtype = 'int64') # <-- will contain information about the index frame and the corresponding number of fired pixels
        
    i = -1 # <-- initialize fixed index
    
    while True:   
    
        i += 1
                
        if print_msgs == True: print(f"\nRow i = {i}")
        
        temp_frame_arr = np.zeros(shape = [512, 512], dtype = "uint8") # <-- 'uint8' tries to store as less memory as possible
        
        j = i # <-- initialize mobile index
        
        if i < (DS_arr.shape[0] - 1): # <-- if I'm still far from the end of array
        
           while DS_arr[j, -1] == DS_arr[j + 1, -1]: # <-- Compare idxs of Det (0,1,2) between a row and the next one
                                                        
              j += 1 # <-- increment index to keep checking also next Det
               
              if j == (DS_arr.shape[0] - 1): # <-- if I get at the end of array I must stop

                 break # <-- exit while
           
            
        # - Now initialize variable for fired pixels number - #         
        temp_pix_fired_num = 0 
       
        for i in range(i, j + 1): # <-- Loop over the same det idx

            # - Extract geometric parameters to decode fired pixels - #
            sez = np.uint64(DS_arr[i, 4]) # <-- Val (0, 15)
            col = np.uint64(DS_arr[i, 3]) # <-- Val (0, 15) 
                    
            core_pr = np.uint64(DS_arr[i, 2]) # <-- Val (0, 127)    
        
            # - Spot the (x,y) starting position of the fired region (origin is BOTTOM LEFT) - #
            x_start = np.uint64(32 * sez + 2*col)  
            y_start = np.uint64(4  * core_pr)      
        
            hitmap = np.uint64(DS_arr[i, 1]) # <-- Val (0, 255)
        
            hitmap_bit_str = np.binary_repr(hitmap) # <-- Stringa di cifre booleane in MSBfirst (i.e. il bit più a SX ha il massimo esponente)
            
            temp_byte_arr = np.flip(np.array([int(bool_digit) for bool_digit in hitmap_bit_str])) # <-- La stringa di booleani diventa un array in LSBFIRST
            
            pix_fired = np.where(temp_byte_arr == 1)[0]

            for bit_pos in pix_fired: # <-- Loops along the found pix fired positions   
                 
                x_fired = np.uint64(x_start + np.mod(bit_pos, 2))     # <-- Setting col of fired pixel
                y_fired = np.uint64(511 - (y_start + (bit_pos // 2))) # <-- Setting row of fired pixel: y --> col = 511 - y
  
                temp_frame_arr[y_fired, x_fired] = 1            
            
            temp_pix_fired_num += pix_fired.size
            
            if print_msgs == True: print(f"Pixel Decode --> ROW i = {i} --> {pix_fired.size} pix fired")              
        
                        
        if print_msgs == True: print(f"\nEvent {len(frame_arr_lst)} saved --- {temp_pix_fired_num} total pix fired\n") # <-- Essendo in Python il 1° evento lo conto come '0'
 
        temp_pix_fired = np.nonzero(temp_frame_arr)[0].size

        pix_fired_arr = np.concatenate((pix_fired_arr, np.array([temp_pix_fired])), axis = 0)
        
        frame_arr_lst.append(temp_frame_arr) 
      
        if i == (DS_arr.shape[0] - 1): # <-- if I get at the end of array I must stop  
           break 
      
        if len(frame_arr_lst) == max_events: # <-- Emergency overflow stop
        
           if print_msgs == True: print(f"Storage of {max_events} events reached --- Forcing interruption at row {i} to avoid memory overflow")
           
           break

    return frame_arr_lst, pix_fired_arr, DS_df, DS_arr   


#%% MERGE ALL EVENTS IN 1 FRAME
    
def merged_2D_frame(DS_arr, print_msgs = False):
    
    """ For single sensor analysis: starting from 1 big array of events merges 
        all of them in a flattened frame
        (useful for noisy pixels identification)
    """
    
    ###        ----------        INPUT CHECKS        ----------        ###
    if str(type(DS_arr)) != "<class 'numpy.ndarray'>": raise(Exception(f"1st Input sequence must be a numpy array\nYou passed a {type(DS_arr)} type"))
    if len(DS_arr.shape) != 2:                         raise(Exception(f"You've passed a {len(DS_arr.shape)}-D array in input\nOnly 2D are allowed"))  
    if print_msgs != True and print_msgs != False :    raise(Exception(f"print_msgs can be set either to True or False\nYou passed print_msgs = {print_msgs}")) 
    # ------------------------------------------------------------------ #

    all_evs_sum = np.zeros(shape = [512, 512], dtype = "uint8") # <-- initialize output 2D squared array
            
    for i, row in enumerate(DS_arr): # <-- Loops along the whole merged DS
    
        if print_msgs == True: print(f"Row {i+1}/{DS_arr.shape[0]}")

        temp_frame = np.zeros(shape = [512, 512], dtype = "uint8") # <-- initialize temporary frame
       
        temp_sez = row[4] # <-- Val (0, 15)
        temp_col = row[3] # <-- Val (0, 15) 
                
        temp_core_pr = row[2] # <-- Val (0, 127)    
    
        # - Spot the (x,y) starting position of the fired region (origin is BOTTOM LEFT) - #
        temp_x_start = 32 * temp_sez + 2 * temp_col  
        temp_y_start =  4 * temp_core_pr           
    
        temp_hitmap = np.uint64(row[1]) # <-- Val (0, 255)
    
        temp_hitmap_bit_str = np.binary_repr(temp_hitmap) # <-- str of boolean digits in MSBfirst (i.e. highest exponent on the LEFT)
        
        temp_byte_arr = np.flip(np.array([int(temp_bool_digit) for temp_bool_digit in temp_hitmap_bit_str])) # <-- str of boolean digits is flipped (LSBfirst) and converted into array 
        
        temp_pix_fired = np.where(temp_byte_arr == 1)[0] # <-- extract the positions of fired pixels inside the core_pr
                 
        for temp_bit_pos in temp_pix_fired: # <-- Loops along the found pix fired positions   
            
            temp_x_fired = np.uint64(temp_x_start + np.mod(temp_bit_pos, 2))     # <-- Setting col of fired pixel
            temp_y_fired = np.uint64(511 - (temp_y_start + (temp_bit_pos // 2))) # <-- Setting row of fired pixel: y --> col = 511 - y
             
            temp_frame[temp_y_fired, temp_x_fired] = 1 # <-- fire the corresponding pixel in the temporary frame
            
            all_evs_sum += temp_frame # <-- add temporary frame to the output one
        
    return all_evs_sum



#%% CLUSTER DENSITY

# - Useful to identify kinky clusters from PACMAN analysis - #
def density_clst_coords(clst_coords, precision = 3):
    
    """ Takes as input the coordinates of the fired pixels inside a cluster,
        in the form of a 2D numpy array with 2 columns (1st col = rows = x, 2nd col = cols = y).
        Returns the ratio between the numer of fired pixels and the total number of pixels 
        present inside the rectangle mad up by the farest fired pixels.
        
        'precision' = number of decimals to round the returned ratio
    """
    
    ###        ----------        INPUT CHECKS        ----------        ###
    if str(type(clst_coords)) != "<class 'numpy.ndarray'>": raise(Exception(f"1st Input sequence must be a numpy array\nYou passed a {type(clst_coords)} type"))
    if len(clst_coords.shape) != 2:                         raise(Exception(f"You've passed a {len(clst_coords.shape)}-D array in input\nOnly 2D arrays with 2 columns are allowed"))  
    if precision <= 0:                                      raise(Exception(f"'precision' must be a positive integer\nYou've passed precision = {precision}"))    
    if clst_coords.shape[1] != 2:                           raise(Exception("clst_coords must be a numpy array with 2 columns\nYou passed an array with shape {clst_coords.shape}"))    
    # ------------------------------------------------------------------ #
        
    c = clst_coords.copy() # <-- temporary variable to simplify calculations
    
    temp_row_i = c[:,0].min() ; temp_col_i = c[:,1].min() # <-- compute delimiters of rectangle
    temp_row_f = c[:,0].max() ; temp_col_f = c[:,1].max()

    temp_dim_v = temp_row_f - temp_row_i + 1 # <-- height of rectangle
    temp_dim_h = temp_col_f - temp_col_i + 1 # <-- base   of rectangle 

    temp_n_pix = clst_coords.shape[0] # <-- get number of fired pixels

    area = temp_dim_v * temp_dim_h # <-- rectangular area in pixels units
    
    density = np.round(temp_n_pix/area, decimals = precision) # <-- computes ratio

    return density, area # <-- it can be useful to return the total area as well


#%% SINGLE VS DETSYNC ANALYSIS

def group_idxs(ts_arr, ts_thr):

    """ Used to scan the ts_ext column of the data, grouping the first row and last
        row idx of a column with compatible values of ts, within a thr specified
        as 'ts_thr'. A 2-D array containing the bounds idxs of the time-grouped events is returned """    
        
    ###        ----------        INPUT CHECKS        ----------        ###
    if str(type(ts_arr)) != "<class 'numpy.ndarray'>": raise(Exception(f"1st Input sequence must be a numpy array\nYou passed a {type(ts_arr)} type"))
    if len(ts_arr.shape) != 1:                         raise(Exception(f"You've passed a {len(ts_arr.shape)}-D array as the 1st input\nOnly 1D arrays are allowed"))  
    if ts_thr <= 0:                                    raise(Exception(f"'ts_thr' must be positive \nYou've passed ts_thr = {ts_thr}"))    
    # ------------------------------------------------------------------ #
    
    # - Initialize output array - #
    idxs_bros_arr = np.empty(shape = [0, 2], dtype = 'int64')  # <-- 1st column: first idx of the time group
                                                               # <-- 2nd column: last  idx of the time group  
    
    j = 0 # <-- Initialize mobile idx
    
    cond_while = True # <-- Initialize while condition
    
    while cond_while == True:
        
        i = j # <-- Initialize fixed idx as the last registered mobile idx
        
        # - Initialize idx bounds of the compatible ts_values - #
        temp_idx_arr = np.array([i, j], dtype = 'int64')
        
        j = i + 1 # <-- move mobile idx
            
        if j == ts_arr.size: # <-- if the array dimension border is reached I must stop
            
           # - save the last update before escaping - #
           idxs_bros_arr = np.concatenate((idxs_bros_arr, temp_idx_arr.reshape([1, 2])), 
                                          axis = 0) # <-- save the bound idxs of the array
            
           
           break # <-- exit 1st while
           
        while (ts_arr[j] - ts_arr[j-1]) <= ts_thr: # <-- if two subsequent ts_values of the array are compatibles (within thr)
    
            temp_idx_arr[1] = j # <-- update last idx of the bounded interval
    
            j += 1 # <-- increment mobile idx to keep checking next ts_value
            
            if j == ts_arr.size: # <-- if the array dimension border is reached I must stop
                
               cond_while = False # <-- exit 1st while
               
               break # <-- exit current while
    
        idxs_bros_arr = np.concatenate((idxs_bros_arr, temp_idx_arr.reshape([1, 2])), 
                                       axis = 0) # <-- save the bound idxs of the array
        
    return idxs_bros_arr
    

def ts_compatibles(ts_A_arr, ts_B_arr, ts_thr):
    
    """ Given the two arrays in input and the 'ts_thr', output returns TRUE if
        at least 2 elements from different arrays are closer than
        the specified thr, otherwise returns FALSE.
        
        Input array might also have different size """
        
    ###        ----------        INPUT CHECKS        ----------        ###
    if str(type(ts_A_arr)) != "<class 'numpy.ndarray'>": raise(Exception(f"1st Input sequence must be a numpy array\nYou passed a {type(ts_A_arr)} type"))
    if len(ts_A_arr.shape) != 1:                         raise(Exception(f"You've passed a {len(ts_A_arr.shape)}-D array as the 1st input\nOnly 1D arrays are allowed"))  
    if str(type(ts_B_arr)) != "<class 'numpy.ndarray'>": raise(Exception(f"2nd Input sequence must be a numpy array\nYou passed a {type(ts_B_arr)} type"))
    if len(ts_B_arr.shape) != 1:                         raise(Exception(f"You've passed a {len(ts_B_arr.shape)}-D array as the 2nd input\nOnly 1D arrays are allowed"))  
    if ts_thr <= 0:                                      raise(Exception(f"'ts_thr' must be positive \nYou've passed ts_thr = {ts_thr}"))    
    # ------------------------------------------------------------------ #
    
    output = False # <-- Initialize output
    
    for ts_A in ts_A_arr: # <-- scan 1st array elements
        
        temp_diff_arr = np.abs(ts_B_arr - ts_A) # <-- compute an array of distances
        
        if np.sum(temp_diff_arr <= ts_thr) != 0: # <-- check if at least 1 dstance is lower than threshold
            
           output = True # <-- set final output
           
           break # <-- no need to go on: exit loop

    return output




#%% Linear Regression --- Cook's Distance

def retta(x, m, q):
    return m*x + q


def cook_dist_func (x_0, y_0, el_pos, n_par = 2):
    
    """ Computes Cook's distance associated to point in
        position 'el_pos' inside arrays x_0,y_0
        n_par = 2 default for linear regression  """

    ###        ---        Checks for INPUT Conditions        ---        ###
    if str(type(x_0)) != "<class 'numpy.ndarray'>" or str(type(y_0)) != "<class 'numpy.ndarray'>":
                                                   raise(Exception(f"1st and 2nd input must be numpy arrays\nYou have passed a {str(type(x_0))} and a {str(type(y_0))} types"))
   
    if len(x_0.shape) > 1 or len(y_0.shape) > 1 :  raise(Exception(f"Input Arrays must be 1D\nYou have passed dim(input 1) = {x_0.shape} and dim(input 2) = {y_0.shape}"))
    if x_0.size!= y_0.size :                       raise(Exception(f"Input Arrays must have same lenghts\nYou passed len(input 1) = {x_0.size} and len(input 2) = {y_0.size}"))
    
    if el_pos < 0: raise(Exception("Position of the element associated to the distance cannot be negative\nYou have passed el_pos = {el_pos}"))
    if el_pos >= y_0.size: raise(Exception("Position of the element associated to the distance cannot be bigger or equal to the array size\nYou have passed el_pos = {el_pos} for size {y_0.size}"))
    # ------------------------------------------------------------------- #

    N = y_0.size
    
    # - Linear Model 1 - #

    temp_x, temp_y = x_0, y_0

    (b_1, a_1), temp_pcov = curve_fit(retta, temp_x, temp_y, maxfev = 1000)

    y_1 = a_1 + b_1*x_0

    # - Linear Model 2 (single point removed) - #
    
    temp_x, temp_y = np.delete(x_0, el_pos), np.delete(y_0, el_pos)

    (b_2, a_2), temp_pcov = curve_fit(retta, temp_x, temp_y, maxfev = 1000)

    y_2 = a_2 + b_2*x_0

    # Cook's Distance associated to point in position 'el_pos' #
    
    MSE = (1 / N) * (((y_1 - y_0)**2)).sum() # <-- Mean Squared Error
    
    dist = (((y_1 - y_2)**2).sum()) / (n_par * MSE)
    
    return dist


def cook_outliers_rm (x_in, y_in, thr_cook = "cook_default", thr_rho = 0.8, thr_n_iter = 100, msgs = True):

    """ Iterated Removal of outliers inside x_in, y_in arrays.
        Algorithm can be stopped in different ways, if at least a thr is activated:
        
        -) "thr_cook"   : can be 1 or 4 / data.size (cook_default in ths case)
        -) "thr_rho"    : if the abs value of Pearson corr coeff is over a given value
        -) "thr_n_iter" : stops the algorithm in any case if the n. of iterations exceeds the given threshold
    """    

    ###        ---        Checks for INPUT Conditions        ---        ###
    if str(type(x_in)) != "<class 'numpy.ndarray'>" or str(type(y_in)) != "<class 'numpy.ndarray'>":
                                                     raise(Exception(f"1st and 2nd input must be numpy arrays\nYou have passed a {str(type(x_in))} and a {str(type(y_in))} types"))
   
    if len(x_in.shape) > 1 or len(y_in.shape) > 1 :  raise(Exception(f"Input Arrays must be 1D\nYou have passed dim(input 1) = {x_in.shape} and dim(input 2) = {y_in.shape}"))
    if x_in.size!= y_in.size :                       raise(Exception(f"Input Arrays must have same lenghts\nYou passed len(input 1) = {x_in.size} and len(input 2) = {y_in.size}"))
   
    if thr_cook != "cook_default" and thr_cook != 1 and thr_cook != None : 
                                                     raise(Exception(f"Cook's threshold ('thr_cook') must be set either to 'cook_default' or to 1 or can be set to 'None'\nYou have set thr_cook = {thr_cook}"))    
    
    if thr_rho != None and (thr_rho > 1 or thr_rho < -1):
                                                     raise(Exception(f"Pearson Corr Coeff ('thr_rho') must belong to the interval [-1, 1] or can be set to 'None'\nYou have set thr_rho = {thr_rho}"))
   
    if thr_cook == None and thr_rho == None:         raise(Exception("'thr_cook' and 'thr_rho' cannot be set both to 'None'"))
    #######################################################################
   
    temp_x, temp_y = x_in, y_in
    
    n_it = 0
    
    cook_dists_lst = []  ;  rho_lst = []  ;  pos_rm_lst = []
 
    while True:
     
        n_it += 1    
  
        temp_size = temp_y.size # <-- Variable inside loop
        
        if thr_cook == "cook_default":
        
           temp_cook_thr = 4 / temp_size # <-- Suggested threshold for Cook's distance algorithm
        
        elif thr_cook == 1:
        
           temp_cook_thr = 1  
        ############### Eventualmente aggiungere le casistiche pensate per altre thresholds   
           
        # - Compute Cook's distance Point by Point - #
        cook_dst_arr = np.array([cook_dist_func(temp_x, temp_y, i) for i in range(temp_size)]) # <-- Contains Cook's distances associated to each point in the input array
       
        temp_dst_max = cook_dst_arr.max() # <-- Extracts max value from the built array
        
        temp_dst_pos = cook_dst_arr.argmax() # <-- Extract position of the max value
    
        cook_dists_lst.append(temp_dst_max) # <-- Store the max value for monitoring Cook's distance
        
        
        # - Compute Pearson Correlation Coefficient - #
        temp_rho = np.cov(temp_x, temp_y)[0,1] / (temp_x.std() * temp_y.std()) 
        
        rho_lst.append(temp_rho)                
     
        if n_it == thr_n_iter: break

        else:    
     
           if thr_cook != None and thr_rho == None:
                
              if temp_dst_max <= temp_cook_thr: break
           
              else: pass
          
           elif thr_cook == None and thr_rho != None:
                
              if (temp_rho > 0 and temp_rho >= np.abs(thr_rho)) or (temp_rho < 0 and temp_rho <= -np.abs(thr_rho)): break
                
              else: pass
          
           elif thr_cook != None and thr_rho != None: 
               
              if temp_dst_max <= temp_cook_thr or \
                 ((temp_rho > 0 and temp_rho >= np.abs(thr_rho)) or (temp_rho < 0 and temp_rho <= -np.abs(thr_rho))): break 
    
              else: pass
          
        # - Prepare iterated data for next loop - #
        temp_x, temp_y = np.delete(temp_x, temp_dst_pos), np.delete(temp_y, temp_dst_pos)       
     
        
        # - Save the idx of the iteratively removed position - # 
        pos_rm_lst.append(temp_dst_pos) # <-- in case I want to obtain the corresponding positions in the original array
       
        if msgs == True:
            
           print(f"Iteration {n_it}")
        
    # - Assigns Outputs when the while loop is ended - #
    x_out, y_out = temp_x, temp_y

    return x_out, y_out, cook_dists_lst, rho_lst, pos_rm_lst


#%% Rebuild Original IDXS

def rebuild_original_idxs(pos_x_arr):
    
    """ pos_x_arr is the sequence of iteratively removed idxs """

    ###        ---        Checks for INPUT Conditions        ---        ###
    if str(type(pos_x_arr)) != "<class 'numpy.ndarray'>":
       
       raise(Exception(f"Input must be a numpy array\nYou have passed a {str(type(pos_x_arr))}"))
    
    if (pos_x_arr < 0).sum() != 0: 
        
       raise(Exception("WARNING: Input array of positions seems to contain negative elements"))
    #######################################################################
        
    pos_0_arr = np.zeros_like(pos_x_arr) # <-- Initialize output array
    
    pos_0_arr[0] = pos_x_arr[0] # <-- 1st element is already correct
    
    for i in range(1, pos_0_arr.size, 1):
        
        temp_idx = pos_x_arr[i]
    
        for j in range(i-1, 0-1, -1):
            
            if temp_idx >= pos_x_arr[j]: 
                
               temp_idx += 1 
    
        pos_0_arr[i] = temp_idx    
    
    return pos_0_arr






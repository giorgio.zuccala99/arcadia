
"""

ARCADIA algorithms and codes work better with .txt data containing already split-by-row events

This code executes the conversion .csv --> .txt based on the file specified by user
    
"""

import os # <-- operative system module: used to work with directories

#%% INTRO

""" PC personal paths --- Change them according to your directories """

PC_str = r'C:\GIORGIO PC'

main_folder = PC_str + r'\UNI\INSUBRIA\Attività Extra\ARCADIA - Recap/'

codes_folder = main_folder + 'Codes/'

# --- Cartelle x file singoli --- #
csv_folder_sing = main_folder + r'Data\Single Sensor Data\csv format/'
txt_folder_sing = main_folder + r'Data\Single Sensor Data\txt format/'

# --- Cartelle x file detsync --- #
csv_folder_sync = main_folder + r'Data\Detsync Data\csv format/'
txt_folder_sync = main_folder + r'Data\Detsync Data\txt format/'

# - Check if specified paths are ok - #
if (os.path.isdir(csv_folder_sing) == False): raise(Exception(f"Path \n{csv_folder_sing}\n Not found")) # <-- Check if input path is valid
if (os.path.isdir(txt_folder_sing) == False): raise(Exception(f"Path \n{txt_folder_sing}\n Not found")) 

if (os.path.isdir(csv_folder_sync) == False): raise(Exception(f"Path \n{csv_folder_sync}\n Not found")) # <-- Check if input path is valid
if (os.path.isdir(txt_folder_sync) == False): raise(Exception(f"Path \n{txt_folder_sync}\n Not found")) 

os.chdir(main_folder)

#%% MODULES & LIBRARIES

os.chdir(codes_folder) # <-- temporary change of working path to import functions from file

from ARCADIA_funcs import csv_to_txt_DS

os.chdir(main_folder)

#%% SETUP VARIABLES

# - Select files to convert: comment/uncomment the rows between **** - #

# ************************** #
files_to_conv_sing = 'all' # <-- will convert all present .csv files
# files_to_conv_sing = ['13_det0', '13_det1'] # <-- index names' list of spcific files to convert. Make sure to use this name format
# ************************** #
if len(files_to_conv_sing) == 0: raise(Exception("files_to_conv_sing must not be empty. Please provide an input"))


# ************************** #
files_to_conv_sync = 'all' # <-- will convert all present detsync files
# files_to_conv_sync = ['15', ] # <-- index names' list of spcific detsync files to convert. Make sure to use this name format
# ************************** #
if len(files_to_conv_sync) == 0: raise(Exception("files_to_conv_sing must not be empty. Please provide an input"))


# - Choose wether to overwrite files or not - #
ovwr_files = True
# ovwr_files = False


#%% CONVERSION .csv ---> .txt      

""" Arguments of the function 'csv_to_txt_DS' used for conversion

  .)  ask      = False ==> Overwrites files without asking
  .) n_sensors = 1     <-- for data acquired with 1 sensor and external trigger (old material)
  .) n_sensors > 1     <-- for data acquired with > 1 sensor and without external trigger (new material: n = 3)

"""

#%% Single sensor data conversion

# - Case of all files conversion - #
if files_to_conv_sing == 'all':

   files_lst = os.listdir(csv_folder_sing)    

# - Case of all files conversion - #
elif files_to_conv_sing != 'all':

   files_lst = [temp_name + '.csv' for temp_name in files_to_conv_sing] # <-- ad extension to the specified file
   
   
print("\nConverting single sensor data files")   
   
# - Start converion loop - #
for k, temp_name in enumerate(files_lst):
 
    temp_name = temp_name[:-4] # <-- [:-4] cuts .csv
 
    if os.path.isfile(os.path.join(txt_folder_sing, temp_name + ".txt")) == True:
         
       if ovwr_files == True:
            
          print(f"\n{temp_name + '.txt'} already present --- overwriting it as specified") 
            
          temp_df = csv_to_txt_DS(path_to_load = csv_folder_sing, filename = temp_name, 
                                  path_to_save = txt_folder_sing, 
                                  ask_ovwr = False, print_msgs = True) 
 
       elif ovwr_files == False: 
             
          print(f"\n{temp_name + '.txt'} already present --- not overwritten as specified") 
            
            
    
    elif os.path.isfile(os.path.join(txt_folder_sing, temp_name[:-4] + ".txt")) == False:
 
       print(f"\n{temp_name + '.txt'} not present --- creating it")      
            
       temp_df = csv_to_txt_DS(path_to_load = csv_folder_sing, filename = temp_name, 
                               path_to_save = txt_folder_sing, n_sensors = 3, 
                               ask_ovwr = False, print_msgs = True)  


#%% Detsync data conversion -- #

# - Case of all files conversion - #
if files_to_conv_sync == 'all':

   files_lst = os.listdir(csv_folder_sync) 

# - Case of all files conversion - #
elif files_to_conv_sync != 'all':

   files_lst = [temp_name + '_detsync.csv' for temp_name in files_to_conv_sync] # <-- ad extension to the specified file
   
   
print("\nConverting detsync data files")   

# - Start converion loop - #
for k, temp_name in enumerate(files_lst):
 
    temp_name = temp_name[:-4] # <-- [:-4] cuts .csv
 
    if os.path.isfile(os.path.join(txt_folder_sync, temp_name + ".txt")) == True:
         
       if ovwr_files == True:
            
          print(f"\n{temp_name + '.txt'} already present --- overwriting it as specified") 
            
          temp_df = csv_to_txt_DS(path_to_load = csv_folder_sync, filename = temp_name, 
                                  path_to_save = txt_folder_sync, 
                                  ask_ovwr = False, print_msgs = True) 
 
       elif ovwr_files == False: 
             
          print(f"\n{temp_name + '.txt'} already present --- not overwritten as specified") 
            
            
    
    elif os.path.isfile(os.path.join(txt_folder_sync, temp_name[:-4] + ".txt")) == False:
 
       print(f"\n{temp_name + '.txt'} not present --- creating it")      
            
       temp_df = csv_to_txt_DS(path_to_load = csv_folder_sync, filename = temp_name, 
                               path_to_save = txt_folder_sync, n_sensors = 3, 
                               ask_ovwr = False, print_msgs = True)  

    
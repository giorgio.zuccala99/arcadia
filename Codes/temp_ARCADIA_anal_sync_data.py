
""" Multi-plain configuration --- Analysis of .detsync files:

CONSIDERATIONS:

-) .detsync data are written by the sw when an event is read simultaneously by all three plains --> advantages:
    
  .) noisy pixels' contribution is neglected
  .) no empty frames --> no spurious events that overload memory when decoding 

-) trigger is present inside data <-- useless (set to 0)
 
    
HYPOTHESIS:
    
-) No noisy pixels are fired
-) In the case of misalignment external plains are fixed and aligned between each other
-) Misalignment causes: traslation (x,y) and azimutal rotation (yaw); no ptich & roll   
    
HOW DECODE WORKS:
    
-) All .detsync files are unpacked and decoded
-) frame_all_lst is a big list of 512x512 frames made by groups of 3 frames 
   piled up altogether, in the order det0-det1-det2    
    
    
INITIAL PROCEDURE --- SAFETY CHECKS:
    
-) Verification of proper behaviour:
    
  .) ts_ext always increasing
  .) alternance of det0, det1, det2 never broken in decoded file

-) Verify that noisy pixels don't turn on
      
-) Sample peak of ts_ext_diff distribution <-- useful for next comparison analysis with manual merge of data
  
    
DATA HANDLING AND SELECTION:
    
-) Removal of corrupted events (verify impact on statistics), i.e. with:
    
  .) Multiple clusters (made up by even 1 pixel)
  .) kinky clusters **** (rough recognition through area factor) ***
  .) Dsitributions of angle of incidence (devised from det0-det2) for kinky evs in det1  

After cleaning bad evs:
    
-) For each plane:
    
  .) Clusters shape (x,y) and size (area) vs angle of incidence  
  .) Clusters Areas distribution <-- useful for select optimal pix number of gold events
    
-) GOLD events selection (4-5 pixels per plane) <-- (verify impact on statistics)
 
  .) Verify through hist2D that distributions of fired pixels match  
  .) Distribution of incident angles  
    

ANALYSIS OF SENSOR RESOLUTION: 
  
-) 1st measure of residuals divergence <-- Gaussian Fit    
 
-) Evaluation of Res xy vs yx

  .) Cook's algorithm for outliers *** (in linear approximation, i.e. small angles) ***
  .) Linear fit for yaw-rotation angle assessment
     
-) Re-alignment --> Roto-traslation *** (arbitrary fulcrum for the rotation) ***

-) 2nd measure of residual divergence --> check with pixel dimension's resolution    
    
-- ITERATING PREVIOUS STEPS (see code ZG_gold_evs_ITERATION) --
   
Vary:

    .) gold n. of pixels <-- show angles distribution

Show resolution changes    
    
"""

# *** Unpacking of all DS must return a list long as a multiple of the n. of plains (3) *** #

# -) CALCOLO DEI RESIDUI:   expc - true
# -) CORREZIONE COORDINATE: expc --> true

# -) Sovrapponi hist ang inc prima e dopo i tagli per vedere i cambiamenti
# -) Anziché un taglio in gold evs prova a fare un taglio in angoli di incidenza
# -) Capire perché dopo la gold evs selection non scompare l'erbetta finale nell'istogramma


import os, time


#%% INTRO

""" Personal Folders Handling """
PC_str = r'C:\Giorgio PC/'

main_folder = PC_str + r'UNI\INSUBRIA\Attività Extra\ARCADIA - Recap/' # <-- Codes and Data folder

codes_folder = main_folder + 'Codes/'

txt_sync_folder = main_folder + r'Data\Detsync Data\txt format/'

figs_folder    = main_folder + r'Results\local_analysis\Figs\ts consistency/'

arrs_folder    = main_folder + r'Results\local_analysis\Vals\ts consistency/'

# --- Check if folders exist --- #
if (os.path.isdir(codes_folder) == False): raise(Exception(f"Path \n{codes_folder}\n Not found"))

if (os.path.isdir(txt_sync_folder) == False): raise(Exception(f"Path \n{txt_sync_folder}\n Not found"))
if (os.path.isdir(figs_folder)     == False): raise(Exception(f"Path \n{figs_folder}\n Not found")) 
if (os.path.isdir(arrs_folder)     == False): raise(Exception(f"Path \n{arrs_folder}\n Not found")) 

os.chdir(main_folder)

""" Graphic Parameters """
from matplotlib import pyplot as plt

plt.close('all')


last_fig_idx = -1 # <-- keeps count of opened figures

my_rect_fig_size = (15, 8) # <-- figures box dimensions
my_sqrd_fig_size = (8, 8)

my_std_font = {'family': 'Verdana',  'color':'black','weight':'normal','size': 20} # <-- std font for titles and labels 
my_font_sup = {'family': 'garamond', 'color':'darkblue','weight':'normal'} # <-- suptitles font
my_leg_font = {'family': 'Georgia', "size": 15} # <-- legend font

my_sup_title_fs = 25 # <-- suptitle size
my_leg_title_fs = 15 # <-- legend title size
my_txt_fs       = 15 # <-- printed text size

my_xy_tick_size = 18 # <-- labels tick size


# --- Ajusting cmap for 2D plots --- #
import copy, matplotlib as mpl

my_cmap = copy.copy(mpl.cm.inferno) # <-- copy the default cmap

my_cmap.set_bad(color = 'k') 


# - Fitting - #
run_fits  = True
# run_fits  = False

# - Figures - #
run_figs = True
# run_figs = False


# - Boolean parameters to save/ovwr_arrs files (figures/arrays/json files) - #

save_figs = True
# save_figs = False

save_arrs = True
# save_arrs = False


# save_dfs = True
save_dfs = False

# save_pars = True
save_pars = False


ovwr_figs = True
# ovwr_figs = False

ovwr_arrs = True
# ovwr_arrs = False

ovwr_dfs = True
# ovwr_dfs = False

ovwr_pars = True
# ovwr_pars = False


print_msgs = True
# print_msgs = False

if print_msgs == True:
   print("\nspecified folders are OK")
   

#%% MODULES & LIBRARIES

import numpy   as np
import pandas  as pd
import seaborn as sns

os.chdir(codes_folder)

from ARCADIA_funcs import decode_detsync_from_txt, ZG_PACMAN, density_single_clst #, \
                         # cook_outliers_rm, rebuild_original_idxs

os.chdir(main_folder)

from scipy.optimize import curve_fit as cf

from numpy import sqrt, pi, arctan


#%% SETUP VARIABLES

## -- Distances in mm -- ##
# z_1 = 16.7 # det0 - det1
# z_2 = 33.4 # det0 - det2

z_1 = 19.7        # det0 - det1
z_2 = 19.7 + 19.1 # det0 - det2

which_file = '14_detsync'
# which_file = 'all'

#%% DATA LOADING

# --- Nomi delle colonne nel sw di acquisizione --- #
columns_lst = ['SR bit',  'Hitmap Byte',  'Core pr', 'Col',   'Sez',         'Ser', 
               'Falling', 'ts chip',      'ts FPGA', 'ts SW', 'ts extended', 'trigger', 'sensor']

data_all_df = pd.DataFrame(data = np.zeros(shape = [0, len(columns_lst)]),
                           columns = columns_lst)

frames_all_lst = []

pix_fired_all_arr = np.empty(shape = [0,])

len_of_ds_lst = [["", 0], ] # <-- keep track the lenghts of the decoded unpacked files 

# - Initialize file decode - #
n_file_to_read = len(os.listdir(txt_sync_folder)) 

for i in range(1, n_file_to_read + 1):
         
    temp_filename = f"{i}_detsync"
    
    if which_file != 'all':    

       if temp_filename != str(which_file): continue  # <-- skips loop until desired file is encountered

    frames_lst, pix_fired_arr, DS_df, DS_arr = decode_detsync_from_txt(path_to_load = txt_sync_folder, filename = temp_filename) 

    len_of_ds_lst.append([temp_filename, DS_arr.shape[0]]) 
    
    data_all_df = pd.concat(objs = (data_all_df, DS_df))

    frames_all_lst += frames_lst

    pix_fired_all_arr = np.concatenate((pix_fired_all_arr, pix_fired_arr))
    
print(f"Decoded events for {which_file} file: {(len(frames_all_lst)/3):.0f}")

data_all_arr = data_all_df.iloc[:,:].values
    

#%% CHECK DET_0-1-2

det_arr = np.array(data_all_df['sensor'])

print("\nCheck correct alternance det0-det1-det2")

for i in range(1, det_arr.size - 1, 2): # <-- Ciclo dal 1° al penultimo elemento, a step di 2

    temp_prv = det_arr[i-1]
    temp_det = det_arr[i]
    temp_nxt = det_arr[i+1]

    if temp_det == 0:
        
       if (temp_prv == 0 or temp_prv == 2) and (temp_nxt == 0 or temp_nxt == 1): pass
       else:           
          print(f"PROBLEM at row {i}") 
          print(temp_prv, temp_det, temp_nxt)
   
    elif temp_det == 1:
        
       if (temp_prv == 0 or temp_prv == 1) and (temp_nxt == 1 or temp_nxt == 2): pass
       else:           
          print(f"PROBLEM at row {i}") 
          print(temp_prv, temp_det, temp_nxt)

    elif temp_det == 2:
        
       if (temp_prv == 1 or temp_prv == 2) and (temp_nxt == 2 or temp_nxt == 0): pass
       else:           
          print(f"PROBLEM at row {i}") 
          print(temp_prv, temp_det, temp_nxt)
          break              
              
print("Alternance is OK")      
    
#%% CHECK TS #######################à

Dt_0_arr_lst = []
Dt_1_arr_lst = []
Dt_2_arr_lst = []

freq_0_arr_lst = []
freq_1_arr_lst = []
freq_2_arr_lst = []

temp_start_count = 0
    
temp_n_bins = 100

temp_hist_range = (0, 10)

temp_bin_width = 0.75

for i in range(1, len(len_of_ds_lst), 1):
# for i in range(1, 2, 1):

    temp_start_count += len_of_ds_lst[i-1][1]    

    temp_ts_arr = data_all_arr[temp_start_count : temp_start_count + len_of_ds_lst[i][1], 10] # <-- colonna ts_extended   
    
    temp_det_arr = data_all_arr[temp_start_count : temp_start_count + len_of_ds_lst[i][1], 12] # <-- det 0,1,2 
    
    temp_ts_0_arr = temp_ts_arr[temp_det_arr == 0]
    temp_ts_1_arr = temp_ts_arr[temp_det_arr == 1]
    temp_ts_2_arr = temp_ts_arr[temp_det_arr == 2]
    
    temp_ts_0_diff_arr = np.diff(np.int64(temp_ts_0_arr), n = 1, prepend = temp_ts_0_arr[0]) # <-- CONVERSIONE IN int64! altrimenti non può fare la sottrazione di uint64
    # --- A volte ci sono comunque valori talmente grandi che nemmeno int64 basta, e quindi la derivata viene resa in float64 --- #
    temp_ts_1_diff_arr = np.diff(np.int64(temp_ts_1_arr), n = 1, prepend = temp_ts_1_arr[0])
    temp_ts_2_diff_arr = np.diff(np.int64(temp_ts_2_arr), n = 1, prepend = temp_ts_2_arr[0])

    temp_counts_0, temp_bins_0 = np.histogram(temp_ts_0_diff_arr, bins = temp_n_bins, range = temp_hist_range)  
    temp_binc_0 = temp_bins_0[:-1] + (temp_bins_0[1] - temp_bins_0[0])/2

    temp_counts_1, temp_bins_1 = np.histogram(temp_ts_1_diff_arr, bins = temp_n_bins, range = temp_hist_range)  
    temp_binc_1 = temp_bins_0[:-1] + (temp_bins_1[1] - temp_bins_1[0])/2

    temp_counts_2, temp_bins_2 = np.histogram(temp_ts_2_diff_arr, bins = temp_n_bins, range = temp_hist_range)  
    temp_binc_2 = temp_bins_2[:-1] + (temp_bins_2[1] - temp_bins_2[0])/2
         
    plt.close(last_fig_idx)
    
    last_fig_idx += 1
    
    plt.figure(last_fig_idx, figsize = my_rect_fig_size)
    
    plt.suptitle('Detsync Timestamp analysis - $ts_{ext}$' + "\t" + "$n_{bins}$ = " + f"{temp_n_bins}" + "\t", 
                 fontdict = my_font_sup, fontsize = my_sup_title_fs)
    
    plt.subplot(1,3,1)
    
    plt.title(f"file {len_of_ds_lst[i][0]} - Det0", fontdict = my_std_font)
    plt.xlabel("ts differences (cc)", fontdict = my_std_font)
    plt.ylabel("Entries", fontdict = my_std_font)

    # plt.xlim(temp_hist_range)
    # plt.ylim(0.001, 1e4)
    
    plt.yscale('log')
    
    # plt.hist(temp_ts_0_diff_arr, bins = n_bins, range = temp_hist_range, color = 'c', edgecolor = 'k', 
    #          alpha = 1, histtype = 'bar', orientation = 'vertical', 
    #          label = "n$_{bins}$ = " + f"{n_bins}" + "\n$n_{events}$ = " + f"{temp_ts_0_diff_arr.size}")

    plt.bar(temp_binc_0, temp_counts_0, width = temp_bin_width, align = "center", color = 'c', edgecolor = 'k',
            label = "$n_{bins}$ = " + f"{temp_n_bins}\n" + "$n_{events}$ = " + f"{temp_counts_0.sum()}\n" + \
            f"range = {temp_hist_range}")
    
    plt.grid(ls = '-', lw = 0.25, c = 'k', which = 'both', axis = 'both') #
    plt.legend(prop = my_leg_font, title_fontsize = my_leg_title_fs, title = "Legend", loc = "upper right")
     
    
    plt.subplot(1,3,2)
    
    plt.title(f"file {len_of_ds_lst[i][0]} - Det1", fontdict = my_std_font)
    plt.xlabel("ts differences (cc)", fontdict = my_std_font)
    # plt.ylabel("Entries", fontdict = my_std_font)

    # plt.xlim(temp_hist_range)
    # plt.ylim(0.001, 1e4)
    
    plt.yscale('log')
    
    # plt.hist(temp_ts_1_diff_arr, bins = n_bins, range = temp_hist_range, color = 'c', edgecolor = 'k', 
    #          alpha = 1, histtype = 'bar', orientation = 'vertical', 
    #          label = "n$_{bins}$ = " + f"{n_bins}" + "\n$n_{events}$ = " + f"{temp_ts_1_diff_arr.size}")

    plt.bar(temp_binc_1, temp_counts_1, width = temp_bin_width, align = "center", color = 'c', edgecolor = 'k',
            label = "$n_{bins}$ = " + f"{temp_n_bins}\n" + "$n_{events}$ = " + f"{temp_counts_1.sum()}\n" + \
            f"range = {temp_hist_range}")
      
    plt.grid(ls = '-', lw = 0.25, c = 'k', which = 'both', axis = 'both') #
    plt.legend(prop = my_leg_font, title_fontsize = my_leg_title_fs, title = "Legend", loc = "upper right")
     
    
    plt.subplot(1,3,3)
    
    plt.title(f"file {len_of_ds_lst[i][0]} - Det2", fontdict = my_std_font)
    plt.xlabel("ts differences (cc)", fontdict = my_std_font)
    # plt.ylabel("Entries", fontdict = my_std_font)

    # plt.xlim(temp_hist_range)
    # plt.ylim(0.001, 1e4)
    
    plt.yscale('log')
    
    # plt.hist(temp_ts_2_diff_arr, bins = n_bins, range = temp_hist_range, color = 'c', edgecolor = 'k', 
    #          alpha = 1, histtype = 'bar', orientation = 'vertical', 
    #          label = "n$_{bins}$ = " + f"{n_bins}" + "\n$n_{events}$ = " + f"{temp_ts_2_diff_arr.size}")

    plt.bar(temp_binc_2, temp_counts_2, width = temp_bin_width, align = "center", color = 'c', edgecolor = 'k',
            label = "$n_{bins}$ = " + f"{temp_n_bins}\n" + "$n_{events}$ = " + f"{temp_counts_2.sum()}\n" + \
            f"range = {temp_hist_range}")
      
    plt.grid(ls = '-', lw = 0.25, c = 'k', which = 'both', axis = 'both') #
    plt.legend(prop = my_leg_font, title_fontsize = my_leg_title_fs, title = "Legend", loc = "upper right")


    Dt_0_arr_lst.append(temp_ts_0_diff_arr)
    Dt_1_arr_lst.append(temp_ts_1_diff_arr)
    Dt_2_arr_lst.append(temp_ts_2_diff_arr)
    
    # - Calcolo frequenza eventi - # (1 / cc)
    
    temp_Dt_evs_0_arr = np.diff(temp_ts_0_arr[temp_ts_0_diff_arr > 1000], n = 1) * 250 * 10**(-9) # <-- conversione in s
    temp_Dt_evs_1_arr = np.diff(temp_ts_1_arr[temp_ts_1_diff_arr > 1000], n = 1) * 250 * 10**(-9)
    temp_Dt_evs_2_arr = np.diff(temp_ts_2_arr[temp_ts_2_diff_arr > 1000], n = 1) * 250 * 10**(-9)

    freq_0_arr_lst.append(1 / temp_Dt_evs_0_arr)
    freq_1_arr_lst.append(1 / temp_Dt_evs_1_arr)
    freq_2_arr_lst.append(1 / temp_Dt_evs_2_arr)
    
    ### Warning if not consistent
    w0 = np.where(temp_ts_0_diff_arr < 0)[0]
    w1 = np.where(temp_ts_1_diff_arr < 0)[0]
    w2 = np.where(temp_ts_2_diff_arr < 0)[0]
    
    if w0.size > 0:  print(f"WARNING: file {i}:\t Det0")
    if w1.size > 0:  print(f"WARNING: file {i}:\t Det1")
    if w2.size > 0:  print(f"WARNING: file {i}:\t Det2")

    elif w0.size == 0 and w1.size == 0 and w2.size == 0:
        print(f"ts_ext on file {i} is OK")            

    ### Plot Display
    if run_figs == True:    
      plt.show()
    
    elif run_figs == False:    
      plt.close(last_fig_idx)
    
    ### Salvataggio figure
    if run_figs == True and save_figs == True:
       plt.savefig(figs_folder + "ts_diff_" + len_of_ds_lst[i][0])

# plt.close(last_fig_idx)

#%% Focus frequenza eventi per ciascun detector ###############

freq_0_lst = [np.median(temp_arr) for temp_arr in freq_0_arr_lst]
freq_1_lst = [np.median(temp_arr) for temp_arr in freq_1_arr_lst]
freq_2_lst = [np.median(temp_arr) for temp_arr in freq_2_arr_lst]

#%% 5) Check if Noisy Pixels have been fired ###############

plt.close('all')

# --- Load of Noisy pixels coordinates --- #
noisy_coords_0_arr = np.load(main_folder + "ARCADIA - Codes\Presentation Codes\saved arrays\Single Sensors Analysis/" + "noisy_coords_0_arr.npy")
noisy_coords_1_arr = np.load(main_folder + "ARCADIA - Codes\Presentation Codes\saved arrays\Single Sensors Analysis/" + "noisy_coords_1_arr.npy")
noisy_coords_2_arr = np.load(main_folder + "ARCADIA - Codes\Presentation Codes\saved arrays\Single Sensors Analysis/" + "noisy_coords_2_arr.npy")

idx_noise_fired_lst = [] # <-- idxs of frames that contained noisy pixel fired

print("Scanning through all events...")

t_0 = time.time_ns()

for i in range(0, len(frames_all_lst), 3): 
    
    for j in range(i, i+2 + 1, 1):

        temp_frame = frames_all_lst[j]

        temp_pix_fired_coords = np.transpose(np.vstack(np.nonzero(temp_frame)))
        
        for row_i in temp_pix_fired_coords:
            
            if np.mod(j,3) == 0:
    
               for row_j in noisy_coords_0_arr:
                    
                   if (row_i == row_j).sum() == 2: # <-- if xy_coords match
                       
                       print("Noisy pixel fired in Det 0") 
            
                       idx_noise_fired_lst.append(j)
                       
            elif np.mod(j,3) == 1:
    
               for row_j in noisy_coords_1_arr:
                    
                   if (row_i == row_j).sum() == 2: # <-- if xy_coords match
                       
                       print("Noisy pixel fired in Det 1")                        
                       
                       idx_noise_fired_lst.append(j)
            
            elif np.mod(j,3) == 2:
    
               for row_j in noisy_coords_2_arr:
                    
                   if (row_i == row_j).sum() == 2: # <-- if xy_coords match
                       
                       print("Noisy pixel fired in Det 2")                             
                       
                       idx_noise_fired_lst.append(j)
 
print(f"Elapsed seconds for noisy pixels verification: {((time.time_ns() - t_0)/(10**9)):.2f} seconds")                

if len(idx_noise_fired_lst) == 0:
    
   print("No noisy pixels were ever fired") 


#%% 6) Corrupt Frames recognition ###############

#### ---- User Variable ---- ####
kink_thr = 0.4 # <-- Area factor for kinky cluster discrimination
#################################

bad_occur_lst = []
worth_idx_lst = [] # <-- Indici multipli di 3 dei gruppi di 3 eventi da analizzare (in quanto senza errori)

""" Legenda Messaggi d'Errore:
    
   0 --> Empty Matrix frame  (<== must no occur with .detsync data! But let's just be sure)
   1 --> multiple isolated single pixels
   2 --> multiple isolated single pixels + Clusters
   3 --> Possible kinky
   4 --> multiple clusters
"""

### --- Gli indici salvati nel seguente modo corrispondono agli eventi con 1 solo cluster (anche da 1 pixel) per tutti e 3 i piani --- ###

j = 0

print("Identification of corrupted events ...")

while j < len(frames_all_lst):
    
    print(j, len(frames_all_lst))
    
    temp_frame = frames_all_lst[j]
    
    clst_lst, coords_arr, coords_alone = ZG_PACMAN(frame = temp_frame, pix_radius = 1, look_mode = 'diag')
    
    if len(clst_lst) == 0: # <-- Caso senza clusters
    
       # Se non vengono trovati pixel isolati coords_alone rimane una lista
       
       if str(type(coords_alone)) == "<class 'list'>": # NON DEVE SUCCEDERE
          # print(j, "ATTENZIONE: Matrice vuota trovata") # <-- Negli eventi detsync non ci devono essere frame vuoti
          bad_occur_lst.append([j, 0])
          break
    
       elif str(type(coords_alone)) == "<class 'numpy.ndarray'>": # <-- Se invece ha trovato pixel isolati
    
            if coords_alone.shape[0] > 1: # <-- questi non devono essere più di uno
               # print(j, "Più pixel isolati --> Scartare")
               bad_occur_lst.append([j, 1]) 
               
               j += 3 - np.mod(j,3) # <-- Passo al gruppo di (3) frame successivo 
               
            elif coords_alone.shape[0] == 1: 
               # print(j, "OK")                
               
               if np.mod(j,3) == 2: worth_idx_lst.append(j-2) # <-- i.e. se sono arrivato in fondo al gruppetto di 3 frame salvo l'indice del primo (multiplo di 3)

               j += 1
    
    elif len(clst_lst) == 1: # <-- Caso con 1 cluster
          
       if str(type(coords_alone)) == "<class 'numpy.ndarray'>": # <-- Se ci sono anche pixel singoli accesi
          # print(j, "Cluster + Pixel isolati --> Scartare")
          bad_occur_lst.append([j, 2]) 
          
          j += 3 - np.mod(j,3)   
      
       elif str(type(coords_alone)) == "<class 'list'>": # <-- Se non ci sono altri pixel accesi oltre al cluster
           
          temp_dens, temp_area = density_single_clst(coords_arr) 
           
          if temp_dens < kink_thr: 
             # print(j, "Possible kinky --> Scartare")
             bad_occur_lst.append([j, 3, temp_dens]) 
             
             j += 3 - np.mod(j,3)   
          
          elif temp_dens >= kink_thr:
             # print(j, "OK") 
             
             if np.mod(j,3) == 2: worth_idx_lst.append(j-2)

             j += 1
 
    elif len(clst_lst) > 1: # <-- Caso con più clusters
       # print(j, "Più clusters --> Scartare")
       bad_occur_lst.append([j, 4]) 
       
       j += 3 - np.mod(j,3)


worth_idx_arr = np.array(worth_idx_lst)


#%%
# - Save idxs of Healty events - #
if ovwr_arrs == True or "worth_idx_arr.npy" not in os.listdir(arrs_folder):
   np.save(arrs_folder + "worth_idx_arr" + ".npy", worth_idx_arr)

# --- Valuto le occorrenze di ciascun evento problematico ("bad") --- #

bad_occur_dct = {'Error 0': 0, 'Error 1': 0, 'Error 2': 0, 'Error 3': 0, 'Error 4': 0}

for temp_lst in bad_occur_lst:
    
    c = temp_lst[1]

    bad_occur_dct[f"Error {c}"] += 1


if bad_occur_dct["Error 0"] == 0 and bad_occur_dct["Error 1"] == 0:

   print("There are no empty matrices or multiple isolated pixels\n")

print(f"{len(frames_all_lst)/3:.0f} evs --> {len(worth_idx_lst)} evs\nLost Statistics = {((len(frames_all_lst)/3 - len(worth_idx_lst))/(len(frames_all_lst)/3))*100:.2f} %")

# -- Bad evs will be neglected later through only considering worth_idxs -- #

#%% PLOTTING: Pie Chart Corrupt Frames

plt.close('all')

n_good_evs = int((len(frames_all_lst)/3) - len(bad_occur_lst))

bad_type_arr = np.array([n_good_evs,]) # <-- starts with the number of good events

for i in range(4+1):
    
    bad_type_arr = np.concatenate((bad_type_arr, np.array([bad_occur_dct[f"Error {i}"],])))

bad_type_arr = np.delete(bad_type_arr, obj = (1,2)) # <-- skip 0 elements

### Plot
last_fig_idx += 1 ; plt.figure(last_fig_idx, figsize = my_sqrd_fig_size)
    
plt.suptitle('Detsync analysis', fontdict = my_font_sup, fontsize = my_sup_title_fs)

plt.title('Types of corrupt frames', fontdict = my_std_font)

plt.pie(bad_type_arr, 
        
        labels = [f"Good Events: {100*(n_good_evs/(len(frames_all_lst)/3)):.2f} %",
                  f"Clusters + Multiple isolated pixels: {100*(bad_type_arr[1]/(len(frames_all_lst)/3)):.2f} %", 
                  f"Kinky clusters: {100*(bad_type_arr[2]/(len(frames_all_lst)/3)):.2f} %", 
                  f"Multiple clusters: {100*(bad_type_arr[3]/(len(frames_all_lst)/3)):.2f} %"],
        
        explode = [0, 0, 0, 0], autopct= '%1.1f%%', labeldistance = 2,
        colors = sns.color_palette('Set1'), shadow = False, 
        textprops = {'family': 'garamond', "fontsize":0})

plt.legend(prop = my_leg_font, title_fontsize = my_leg_title_fs, title = f"Total events: {int(len(frames_all_lst)/3)}", loc = "upper center")

### Plot Display
if run_figs == True:    
   plt.show()

elif run_figs == False:    
   plt.close(last_fig_idx)
 
## Salvataggio figure
if run_figs == True and save_figs == True:
   plt.savefig(figs_folder + 'corrupt_frames_pie_chart')

#%% 7a) Focus just on kinky clusters ###############

plt.close('all')

kink_idx_lst  = [] # <-- for study of relations kink / angle of incidence

j = -1

print("Saving again only idxs of kinky clusters ...")
print("I'M ALSO INCLUDING FRAMES WITH CLUSTERS AND SINGLE PIXELS FIRED")

while j < len(frames_all_lst) - 1:
    
    j += 1
    
    # print(j+1, len(frames_all_lst))
    
    temp_frame = frames_all_lst[j]
    
    clst_lst, coords_arr, coords_alone = ZG_PACMAN(frame = temp_frame, pix_radius = 1, look_mode = 'diag')
    
    if len(clst_lst) == 0: # <-- Case without clusters
    
       continue
    
    elif len(clst_lst) == 1: # <-- Case of 1 cluster
          
       if str(type(coords_alone)) == "<class 'numpy.ndarray'>": # <-- if there are also single pixels fired
          
          temp_dens, temp_area = density_single_clst(clst_lst[0]) 
      
       elif str(type(coords_alone)) == "<class 'list'>": # <-- if there are NO other single pixels fired
           
          temp_dens, temp_area = density_single_clst(coords_arr) 
           
          
       if temp_dens < kink_thr: 

          kink_idx_lst.append(j) 

 
    elif len(clst_lst) > 1: # <-- Case with multiple clusters
      
       for temp_clst in clst_lst:
           
           temp_dens, temp_area = density_single_clst(temp_clst) 

           if temp_dens < kink_thr: 
               
              kink_idx_lst.append(j) 

              break  

print(f"I've found {len(kink_idx_lst)}/{len(frames_all_lst)} kinky events, with single clsts or clsts + isolated pixels")

''' HP: Evs with kinky clst and multiple single pixels are actually
        one big fragmented cluster '''
     
# -- Compute COMs (mean of coordinates) for kinky and associate them an angle of incidence -- #

kink_pars_arr = np.empty(shape = [0, 5]) # <-- frame idx, plain (0,1,2), n of pix, Dx, Dy 

for idx in kink_idx_lst:
    
    temp_arr = np.empty(shape = [1, 5])

    temp_frame = frames_all_lst[idx]

    temp_coords_arr = np.transpose(np.vstack(np.nonzero(temp_frame)))
    
    temp_means = np.mean(temp_coords_arr, axis = 0) + 0.5 # <-- 0.5 needed to match with the geometrical center of the pixel                      

    temp_Dx = temp_coords_arr[:,0].max() - temp_coords_arr[:,0].min() + 1
    temp_Dy = temp_coords_arr[:,1].max() - temp_coords_arr[:,1].min() + 1
    
    temp_arr[0, 0] = idx
    temp_arr[0, 1] = np.mod(idx, 3)
    temp_arr[0, 2] = temp_coords_arr.shape[0]
    temp_arr[0, 3] = temp_Dx
    temp_arr[0, 4] = temp_Dy
    
    kink_pars_arr = np.concatenate((kink_pars_arr, temp_arr), axis = 0)
    
kink_pars_df = pd.DataFrame(kink_pars_arr, columns = ["ev idx", "det", "# fired", "Dx", "Dy"])
    
# -- There are some cases of HP violation: clusters very far from each other -- #

# I identify them as follows --> area = Dx * Dy:

kink_area_arr = kink_pars_arr[:,-2] * kink_pars_arr[:,-1]

# clusters/pixels very distant (e.g. 30 pixels) generate 1000 pixels areas

kink_idxs_to_cut = np.where(kink_area_arr >= 1e3)[0] # <-- 1000 pix thr to remove evs with very distant clsts

print(f"\nI remove those frames whose isolated pixels are very far from the main cluster --> {kink_idxs_to_cut.size} evs")
   
kink_pars_arr = np.delete(kink_pars_arr, obj = kink_idxs_to_cut, axis = 0)


#%% 7b) Kinky clsts on det1 / ang_inc

plt.close('all')

bad_occur_arr = np.array([el[0] for el in bad_occur_lst])

idx_kinks_1_evs_lst = [] # <-- idxs of kinky evs on det1 with det0 and det2 safe

idx_arr = np.int64(kink_pars_arr[:,0])
det_arr = np.int64(kink_pars_arr[:,1])

print("Sampling angles of incidence using healthy evs on det0 and det2 \
      \n(<-- I need a centre for clusters and thus I can draw a line)\n \
      and kinky evs on det1")

for i, temp_det in enumerate(det_arr): # <-- Loop over det idxs (0,1,2)

    if temp_det != 1: continue

    else: 
       
       temp_idx = idx_arr[i] 
       
       # - I need kinky frames on det1 with healthy frames on det0 and det 2 - #
       # - If the considered central (det1) frame belongs alone to the kinky evs, without his brothers det0 and det2 - #  
       if np.abs(temp_idx - idx_arr[i-1]) != 1 and np.abs(temp_idx - idx_arr[i+1]) != 1:

          control_0 = False # <-- Becomes 1 if the det0 clst is a single pix or a single non kinky clst            

          temp_frame = frames_all_lst[temp_idx - 1]             

          clst_lst, coords_arr, coords_alone = ZG_PACMAN(frame = temp_frame, pix_radius = 1, look_mode = 'diag')

          if len(clst_lst) == 0: # <-- Caso senza clusters
            
             # Se non vengono trovati pixel isolati coords_alone rimane una lista
          
             if str(type(coords_alone)) == "<class 'numpy.ndarray'>": # <-- Se invece ha trovato pixel isolati
                  
                if coords_alone.shape[0] == 1:  
                
                   control_0 = True 
           
          elif len(clst_lst) == 1: # <-- Caso con 1 cluster
                  
             if str(type(coords_alone)) == "<class 'list'>": # <-- Se non ci sono altri pixel accesi oltre al cluster
                   
                temp_dens, temp_area = density_single_clst(coords_arr) 
                  
                if temp_dens >= kink_thr:
                   
                   control_0 = True 



          control_2 = False # <-- Becomes 1 if the det2 clst is a single pix or a single non kinky clst            

          temp_frame = frames_all_lst[temp_idx + 1]             

          clst_lst, coords_arr, coords_alone = ZG_PACMAN(frame = temp_frame, pix_radius = 1, look_mode = 'diag')

          if len(clst_lst) == 0: # <-- Caso senza clusters
            
             # Se non vengono trovati pixel isolati coords_alone rimane una lista
          
             if str(type(coords_alone)) == "<class 'numpy.ndarray'>": # <-- Se invece ha trovato pixel isolati
                  
                if coords_alone.shape[0] == 1:  
                
                   control_2 = True 
           
          elif len(clst_lst) == 1: # <-- Caso con 1 cluster
                  
             if str(type(coords_alone)) == "<class 'list'>": # <-- Se non ci sono altri pixel accesi oltre al cluster
                   
                temp_dens, temp_area = density_single_clst(coords_arr) 
                  
                if temp_dens >= kink_thr:
                   
                   control_2 = True 

          if control_0 == True and control_2 == True:
               
             idx_kinks_1_evs_lst.append(temp_idx)

print(f"{len(idx_kinks_1_evs_lst)} evs have a kinky cluster on det1 and healthy clusters on det0 and det2")

x0_kink_arr = np.empty(shape = [len(idx_kinks_1_evs_lst), ])

x2_kink_arr = np.empty_like(x0_kink_arr)
y0_kink_arr = np.empty_like(x0_kink_arr)
y2_kink_arr = np.empty_like(x0_kink_arr)

for i, idx in enumerate(idx_kinks_1_evs_lst):

    temp_frame_0 = frames_all_lst[idx-1]
    temp_frame_2 = frames_all_lst[idx+1]
    
    temp_coords_0_arr = np.transpose(np.vstack(np.nonzero(temp_frame_0)))
    temp_coords_2_arr = np.transpose(np.vstack(np.nonzero(temp_frame_2)))
    
    temp_means_0 = np.mean(temp_coords_0_arr, axis = 0) + 0.5 # <-- 0.5 needed to match with the geometrical center of the pixel                      
    temp_means_2 = np.mean(temp_coords_2_arr, axis = 0) + 0.5 # <-- 0.5 needed to match with the geometrical center of the pixel                      

    x0_kink_arr[i], y0_kink_arr[i] = temp_means_0 / 40 # <-- pix to mm conversion
    x2_kink_arr[i], y2_kink_arr[i] = temp_means_2 / 40

ang_inc_kinky_evs = 90 - arctan(z_2 / sqrt((x2_kink_arr - x0_kink_arr)**2 + \
                                      (y2_kink_arr - y0_kink_arr)**2))*180/pi

    
#%% PLOTTING: kinky angles distribution  
   
plt.close('all')

n_bins_kink = 25

counts_kink, bins_kink = np.histogram(ang_inc_kinky_evs, bins = n_bins_kink, range = None)

binc_kink = bins_kink[:-1] + 0.5

bin_width_kink = (ang_inc_kinky_evs.max() - ang_inc_kinky_evs.min()) / n_bins_kink

# - Save distribution of angles of incidence for ALL events - #
if overwrite == True or ("binc_kink_arr.npy" not in os.listdir(arrs_folder) and "counts_kink_arr.npy" not in os.listdir(arrs_folder)):
   np.save(arrs_folder + "binc_kink"   + ".npy", binc_kink)
   np.save(arrs_folder + "counts_kink" + ".npy", counts_kink)



last_fig_idx += 1 ; plt.figure(last_fig_idx, figsize = my_rect_fig_size)
plt.suptitle('Detsync Analysis - Clusters analysis', fontsize = my_sup_title_fs, fontdict = my_font_sup)
        
plt.title("Distribution for Det1 Kinky clusters", fontdict = my_std_font)
plt.xlabel('Angles [°]', fontdict = my_std_font)
plt.ylabel('Entries', fontdict = my_std_font)

# plt.xlim([-0.5, 0.5])

plt.bar(binc_kink, counts_kink, width = bin_width_kink, align = "center", color = 'c', edgecolor = 'k',
        label = "$n_{bins}$ = " + f"{n_bins_kink}\n" + "$n_{events}$ = " + f"{counts_kink.sum()}\n")

plt.grid(ls = 'solid')
plt.legend(prop = my_leg_font, title_fontsize = my_leg_title_fs, title = "Legend", loc = "best")

### Plot Display
if run_figs == True:    
  plt.show()

elif run_figs == False:    
  plt.close(last_fig_idx)

### Salvataggio figure
if run_figs == True and save_figs == True:
   plt.savefig(figs_folder + "hist_angs_inc_det1_kinky_evs")

print("All kinky events on det1 seem to happen at high angle")


#%% 8a) Counting n. of pixels for each plain - All events

plt.close('all')

''' I analyze geometrical distributions on all healthy events (without selecting gold evs yet) '''

n_pix_012_arr = np.zeros(shape = [worth_idx_arr.size, 3])

print("PACMAN running again...")

for i, idx in enumerate(worth_idx_arr): # <-- Now considered events just have 1 pixel fired or 1 cluster
    
    for j in range(idx, idx+2 + 1, 1):

        temp_frame = frames_all_lst[j]

        clst_lst, coords_arr, coords_alone = ZG_PACMAN(frame = temp_frame, pix_radius = 1, look_mode = 'diag')

        if   len(clst_lst) == 0: # <-- Case of just 1 fired pixel
        
            n_pix_012_arr[i, np.mod(j,3)] = coords_alone.shape[0]
        
        elif len(clst_lst) == 1: # <-- Caso of just 1 cluster

            n_pix_012_arr[i, np.mod(j,3)] = clst_lst[0].shape[0]

        else: print("WARNING: I should not enter here!")

n_pix_012_df = pd.DataFrame(n_pix_012_arr, columns = ["Det0: # fired", "Det1: # fired", "Det2: # fired"])

print("I've counted all pixels")

#%% PLOTTING: pix_fired distributions - All events

plt.close('all')

n_bins_distr_0 = np.arange(n_pix_012_arr[:, 0].min(), n_pix_012_arr[:, 0].max() + 2, 1) - 0.5
n_bins_distr_1 = np.arange(n_pix_012_arr[:, 1].min(), n_pix_012_arr[:, 1].max() + 2, 1) - 0.5
n_bins_distr_2 = np.arange(n_pix_012_arr[:, 2].min(), n_pix_012_arr[:, 2].max() + 2, 1) - 0.5

range_n_clst =  (0, n_pix_012_arr.max()) # None

bin_width_n_clst_0 = 0.75
bin_width_n_clst_1 = 0.75 
bin_width_n_clst_2 = 0.75

# Det 0
counts_n_clst_0, bins_n_clst_0 = np.histogram(n_pix_012_arr[:,0], bins = n_bins_distr_0, range = range_n_clst)

binc_n_clst_0 = bins_n_clst_0[:-1] + 0.5

# Det 1
counts_n_clst_1, bins_n_clst_1 = np.histogram(n_pix_012_arr[:,1], bins = n_bins_distr_1, range = range_n_clst)

binc_n_clst_1 = bins_n_clst_1[:-1] + 0.5

# Det 2
counts_n_clst_2, bins_n_clst_2 = np.histogram(n_pix_012_arr[:,2], bins = n_bins_distr_2, range = range_n_clst)

binc_n_clst_2 = bins_n_clst_2[:-1] + 0.5


last_fig_idx += 1 ; plt.figure(last_fig_idx, figsize = my_rect_fig_size)
plt.suptitle('Detsync Analysis - Clusters analysis - All healthy events', fontsize = my_sup_title_fs, fontdict = my_font_sup)
        
plt.subplot(1,3,1)

plt.title("Det 0", fontdict = my_std_font)
plt.xlabel('pix fired', fontdict = my_std_font)
plt.ylabel('Entries', fontdict = my_std_font)

# plt.xlim([-0.5, 0.5])
plt.ylim([-0.5, max(counts_n_clst_0.max(), counts_n_clst_0.max(), counts_n_clst_2.max()) + 0.1*max(counts_n_clst_0.max(), counts_n_clst_1.max(), counts_n_clst_2.max())])

plt.bar(binc_n_clst_0, counts_n_clst_0, width = bin_width_n_clst_0, align = "center", color = 'c', edgecolor = 'k',
        label = "$n_{bins}$ = " + f"{n_bins_distr_0.size}\n" + "$n_{events}$ = " + f"{counts_n_clst_0.sum()}\n" + \
        f"range = {range_n_clst}")

plt.grid(ls = 'solid')
# plt.legend(prop = my_leg_font, title_fontsize = my_leg_title_fs, title = "Legend", loc = "lower center")


plt.subplot(1,3,2)

plt.title("Det 1", fontdict = my_std_font)
plt.xlabel('pix fired', fontdict = my_std_font)
# plt.ylabel('Entries', fontdict = my_std_font)

# plt.xlim([-0.5, 0.5])
plt.ylim([-0.5, max(counts_n_clst_0.max(), counts_n_clst_0.max(), counts_n_clst_2.max()) + 0.1*max(counts_n_clst_0.max(), counts_n_clst_1.max(), counts_n_clst_2.max())])

plt.bar(binc_n_clst_1, counts_n_clst_1, width = bin_width_n_clst_1, align = "center", color = 'c', edgecolor = 'k',
        label = "$n_{bins}$ = " + f"{n_bins_distr_1.size}\n" + "$n_{events}$ = " + f"{counts_n_clst_1.sum()}\n" + \
        f"range = {range_n_clst}")

plt.grid(ls = 'solid')
# plt.legend(prop = my_leg_font, title_fontsize = my_leg_title_fs, title = "Legend", loc = "lower center")


plt.subplot(1,3,3)

plt.title("Det 2", fontdict = my_std_font)
plt.xlabel('pix fired', fontdict = my_std_font)
# plt.ylabel('Entries', fontdict = my_std_font)

# plt.xlim([-0.5, 0.5])
plt.ylim([-0.5, max(counts_n_clst_0.max(), counts_n_clst_0.max(), counts_n_clst_2.max()) + 0.1*max(counts_n_clst_0.max(), counts_n_clst_1.max(), counts_n_clst_2.max())])

plt.bar(binc_n_clst_2, counts_n_clst_2, width = bin_width_n_clst_2, align = "center", color = 'c', edgecolor = 'k',
        label = "$n_{bins}$ = " + f"{n_bins_distr_2.size}\n" + "$n_{events}$ = " + f"{counts_n_clst_2.sum()}\n" + \
        f"range = {range_n_clst}")

plt.grid(ls = 'solid')
# plt.legend(prop = my_leg_font, title_fontsize = my_leg_title_fs, title = "Legend", loc = "lower center")


### Plot Display
if run_figs == True:    
  plt.show()

elif run_figs == False:    
  plt.close(last_fig_idx)

### Salvataggio figure
if run_figs == True and save_figs == True:
   plt.savefig(figs_folder + "pix_fired_012_distr_all_evs")

print(f"Most frequent n. of fired pixels for det0, det1, det2 are\n \
      {binc_n_clst_0[np.argmax(counts_n_clst_0)]}, {binc_n_clst_1[np.argmax(counts_n_clst_1)]}, {binc_n_clst_2[np.argmax(counts_n_clst_2)]}")

#%% 8b) Building COMs --- Points of Impact - All events

plt.close('all')

# --- Array per contenere coordinate centro di massa dei cluster ---#
xy_coords_COM_arr = np.zeros(shape = [3, worth_idx_arr.size, 2]) # <-- Array 3D: n. di piani (da 0 a 2); n. indici buoni; x,y

# --- Array per contenere le dimensioni delle coordinate dei cluster ---#
xy_clst_dim_arr = np.zeros(shape = [3, worth_idx_arr.size, 2]) # <-- Array 3D: n. di piani (da 0 a 2); n. indici buoni; x,y

print("Saving points of impact and clusters' dimensions ...")

for i, idx in enumerate(worth_idx_arr): # <-- Gli eventi considerati hanno 1 pixel o 1 cluster ==>  Non c'è più bisogno di implementare un altro pacman per ricavare il baricentro 
    
    for j in range(idx, idx+2 + 1, 1):

        temp_frame = frames_all_lst[j]

        temp_coords_arr = np.transpose(np.vstack(np.nonzero(temp_frame)))

        temp_means = np.mean(temp_coords_arr, axis = 0) + 0.5 # <-- 0.5 necessario per combaciare con il centro del pixel                          

        xy_coords_COM_arr[np.mod(j,3), i, 0], xy_coords_COM_arr[np.mod(j,3), i, 1] = temp_means
        
        xy_clst_dim_arr[np.mod(j,3), i, 0] = temp_coords_arr[:,0].max() - temp_coords_arr[:,0].min() + 1
        xy_clst_dim_arr[np.mod(j,3), i, 1] = temp_coords_arr[:,1].max() - temp_coords_arr[:,1].min() + 1

print("Done")

#%% PLOTTING: Dx-02, Dy-02 distributions

Dx_arr = (xy_coords_COM_arr[2,:,0] - xy_coords_COM_arr[0,:,0]) / 40 # <-- conversion in mm
Dy_arr = (xy_coords_COM_arr[2,:,1] - xy_coords_COM_arr[0,:,1]) / 40

n_bins_distr = 100

range_res_x = None  ;  bin_width_x = None
range_res_y = None  ;  bin_width_y = None

counts_resx, bins_resx = np.histogram(Dx_arr, bins = n_bins_distr, range = range_res_x)  
counts_resy, bins_resy = np.histogram(Dy_arr, bins = n_bins_distr, range = range_res_y)  

binc_resx = bins_resx[:-1] + (bins_resx[1] - bins_resx[0])/2
binc_resy = bins_resy[:-1] + (bins_resy[1] - bins_resy[0])/2

# - Bin Width x - #   
   
if range_res_x == None:
    
    bin_width_x = (Dx_arr.max() - Dx_arr.min()) / n_bins_distr

else: 
    
    bin_width_x = (range_res_x[1] - range_res_x[0]) / n_bins_distr 


# - Bin Width y - #   
   
if range_res_y == None:
    
    bin_width_y = (Dy_arr.max() - Dy_arr.min()) / n_bins_distr

else: 
    
    bin_width_y = (Dy_arr[1] - Dy_arr[0]) / n_bins_distr 
   

last_fig_idx += 1 ; plt.figure(last_fig_idx, figsize = my_rect_fig_size)
plt.suptitle('Detsync Analysis - All healthy events', fontsize = my_sup_title_fs, fontdict = my_font_sup)
        
plt.subplot(1,2,1)

plt.title("Det0 vs Det2: x differences", fontdict = my_std_font)
plt.xlabel('$\Delta_{x}$ (mm)', fontdict = my_std_font)
plt.ylabel('Entries', fontdict = my_std_font)

# plt.xlim([-0.5, 0.6])
plt.ylim([-0.5, max(counts_resx.max(), counts_resy.max()) + 0.1*max(counts_resx.max(), counts_resy.max())])

plt.bar(binc_resx, counts_resx, width = bin_width_x, align = "center", color = 'c', edgecolor = 'k',
        label = "$n_{bins}$ = " + f"{n_bins_distr}\n" + "$n_{events}$ = " + f"{counts_resx.sum()}\n" + \
        f"range = {range_res_x}")

plt.grid(ls = 'solid')
plt.legend(prop = my_leg_font, title_fontsize = my_leg_title_fs, title = "Legend", loc = "upper left")


plt.subplot(1,2,2)

plt.title("Det0 vs Det2: y differences", fontdict = my_std_font)
plt.xlabel('$\Delta_{y}$ (mm)', fontdict = my_std_font)
plt.ylabel('Entries', fontdict = my_std_font)

# plt.xlim([-0.5, 0.6])
plt.ylim([-0.5, max(counts_resx.max(), counts_resy.max()) + 0.1*max(counts_resx.max(), counts_resy.max())])

plt.bar(binc_resy, counts_resy, width = bin_width_y, align = "center", color = 'c', edgecolor = 'k',
        label = "$n_{bins}$ = " + f"{n_bins_distr}\n" + "$n_{events}$ = " + f"{counts_resy.sum()}\n" + \
        f"range = {range_res_y}")

plt.grid(ls = 'solid')
plt.legend(prop = my_leg_font, title_fontsize = my_leg_title_fs, title = "Legend", loc = "upper left")

### Plot Display
if run_figs == True:    
  plt.show()

elif run_figs == False:    
  plt.close(last_fig_idx)

### Salvataggio figure
if run_figs == True and save_figs == True:
    plt.savefig(figs_folder + "Dxy_02_distr_Detsync")

#%% PLOTTING: Dxy vs Dyx Clusters Coordinates - All events

plt.close('all')

import copy, matplotlib as mpl # <-- Better handling of histdD figures

my_cmap = copy.copy(mpl.cm.inferno) 
my_cmap.set_bad(my_cmap(0))

n_bins_2D = 10

range_2D_max = 10

range_2D_0 = ((0, range_2D_max), (0, range_2D_max)) # None
range_2D_1 = ((0, range_2D_max), (0, range_2D_max)) # None
range_2D_2 = ((0, range_2D_max), (0, range_2D_max)) # None

counts_2D_0, bins_x_0, bins_y_0 = np.histogram2d(xy_clst_dim_arr[0, :, 0], xy_clst_dim_arr[0, :, 1],
                                                 bins = n_bins_2D, range = range_2D_0)

counts_2D_1, bins_x_1, bins_y_1 = np.histogram2d(xy_clst_dim_arr[1, :, 0], xy_clst_dim_arr[1, :, 1],
                                                 bins = n_bins_2D, range = range_2D_1)

counts_2D_2, bins_x_2, bins_y_2 = np.histogram2d(xy_clst_dim_arr[2, :, 0], xy_clst_dim_arr[2, :, 1],
                                                 bins = n_bins_2D, range = range_2D_2)

last_fig_idx += 1 ; plt.figure(last_fig_idx, figsize = my_rect_fig_size)
plt.suptitle(f'Detsync Analysis - Clusters analysis - All healthy events: {worth_idx_arr.size}', fontsize = my_sup_title_fs, fontdict = my_font_sup)
        
plt.subplot(1,3,1)

plt.title("Det 0", fontdict = my_std_font)
plt.xlabel('$\Delta_{x}$ [pixels]', fontdict = my_std_font)
plt.ylabel('$\Delta_{y}$ [pixels]', fontdict = my_std_font)

plt.imshow(counts_2D_0, cmap = "viridis", norm = 'linear', origin = 'lower')

# plt.legend(prop = my_leg_font, title_fontsize = my_leg_title_fs, title = "Legend", loc = "lower center")


plt.subplot(1,3,2)

plt.title("Det 1", fontdict = my_std_font)
plt.xlabel('$\Delta_{x}$ [pixels]', fontdict = my_std_font)
# plt.ylabel('$\Delta_{y}$ [pixels]', fontdict = my_std_font)

# plt.xlim([, ])
# plt.ylim([, ])

plt.imshow(counts_2D_1, cmap = "viridis", norm = 'linear', origin = 'lower')

# plt.legend(prop = my_leg_font, title_fontsize = my_leg_title_fs, title = "Legend", loc = "lower center")


plt.subplot(1,3,3)

plt.title("Det 2", fontdict = my_std_font)
plt.xlabel('$\Delta_{x}$ [pixels]', fontdict = my_std_font)
# plt.ylabel('$\Delta_{y}$ [pixels]', fontdict = my_std_font)

# plt.xlim([, ])
# plt.ylim([, ])

plt.imshow(counts_2D_2, cmap = "viridis", norm = 'linear', origin = 'lower')
plt.colorbar(fraction = 0.05, shrink = 0.75)

# plt.legend(prop = my_leg_font, title_fontsize = my_leg_title_fs, title = "Legend", loc = "lower center")


### Plot Display
if run_figs == True:    
  plt.show()

elif run_figs == False:    
  plt.close(last_fig_idx)

### Salvataggio figure
if run_figs == True and save_figs == True:
   plt.savefig(figs_folder + "DxDy_clst_012_all_evs")

#%% 8c) Coordinates Extraction - All events

plt.close('all')

## --        (x,y) ==> (rows, cols)        -- ##
## -- 1 pixel = 25 um  ==>  1mm = 40 pixel -- ##

## -- Distances in pixels -- ##
x2_arr = xy_coords_COM_arr[2, :, 0] / 40
x0_arr = xy_coords_COM_arr[0, :, 0] / 40

y2_arr = xy_coords_COM_arr[2, :, 1] / 40
y0_arr = xy_coords_COM_arr[0, :, 1] / 40

x1_true_arr = xy_coords_COM_arr[1, :, 0] / 40
y1_true_arr = xy_coords_COM_arr[1, :, 1] / 40

# --- 3D Geometry Formulas --- #
x1_expc_arr = (z_1/z_2)*x2_arr + (1 - z_1/z_2)*x0_arr
y1_expc_arr = (z_1/z_2)*y2_arr + (1 - z_1/z_2)*y0_arr

res_x_arr = (x1_expc_arr - x1_true_arr) # <-- pix => mm conversion
res_y_arr = (y1_expc_arr - y1_true_arr)

dist_xy_arr = sqrt(res_x_arr**2 + res_y_arr**2)

# -- First consider all angles, regardless of the pixel number -- # 
ang_inc_arr = 90 - arctan(z_2 / sqrt((x2_arr - x0_arr)**2 + (y2_arr - y0_arr)**2)) * 180/pi

ang_inc_x_arr = np.zeros_like(ang_inc_arr)
ang_inc_y_arr = np.zeros_like(ang_inc_arr)

for i in range(ang_inc_x_arr.size):
    
    if x2_arr[i] > x0_arr[i]:
       
       ang_inc_x_arr[i] = 90  - arctan(z_2 / (x2_arr[i] - x0_arr[i])) * 180/pi
    
    elif x2_arr[i] < x0_arr[i]:
       
       ang_inc_x_arr[i] = - (90 - np.abs(arctan(z_2 / (x2_arr[i] - x0_arr[i])) * 180/pi))


for i in range(ang_inc_y_arr.size):
    
    if y2_arr[i] > y0_arr[i]:
       
       ang_inc_y_arr[i] = 90  - arctan(z_2 / (y2_arr[i] - y0_arr[i])) * 180/pi
    
    elif y2_arr[i] < y0_arr[i]:
       
       ang_inc_y_arr[i] = - (90 - np.abs(arctan(z_2 / (y2_arr[i] - y0_arr[i])) * 180/pi))

#%% PLOTTING: Angles of incidence - All events

plt.close('all')

n_bins_ang_inc = 80

range_ang_inc = None

bin_width_ang_inc = None

counts_ang_inc, bins_ang_inc = np.histogram(ang_inc_arr, bins = n_bins_ang_inc, range = range_ang_inc)

binc_ang_inc = bins_ang_inc[:-1] + (bins_ang_inc[1] - bins_ang_inc[0])/2
# binc_ang_inc = bins_ang_inc[:-1]


binc_ang_inc_all_evs   = binc_ang_inc.copy()   # <-- Save these for later
counts_ang_inc_all_evs = counts_ang_inc.copy()


# - Save distribution of angles of incidence for ALL events - #
if overwrite == True or ("binc_ang_inc_all_evs.npy" not in os.listdir(arrs_folder) and "counts_ang_inc_all_evs.npy" not in os.listdir(arrs_folder)):

   np.save(arrs_folder + "binc_ang_inc_all_evs"   + ".npy", binc_ang_inc_all_evs)
   np.save(arrs_folder + "counts_ang_inc_all_evs" + ".npy", counts_ang_inc_all_evs)

# - Bin Width x - #   
   
if range_ang_inc == None:
    
   bin_width_ang_inc = (ang_inc_arr.max() - ang_inc_arr.min()) / n_bins_ang_inc

else: 
    
   bin_width_ang_inc = (range_ang_inc[1] - range_ang_inc[0]) / n_bins_ang_inc 

last_fig_idx += 1 ; plt.figure(last_fig_idx, figsize = my_rect_fig_size)
plt.suptitle(f'Detsync Analysis - All healthy events: {worth_idx_arr.size}', fontsize = my_sup_title_fs, fontdict = my_font_sup)
        
plt.title("Angles of incidence distribution", fontdict = my_std_font)
plt.xlabel('Angles [°]', fontdict = my_std_font)
plt.ylabel('Entries', fontdict = my_std_font)

# plt.xlim([,])
# plt.ylim([,])

plt.bar(binc_ang_inc, counts_ang_inc, width = bin_width_ang_inc, align = "center", color = 'c', edgecolor = 'k',
        label = "n$_{bins}$ = " + f"{n_bins_ang_inc}" + f"\nevents: {counts_ang_inc.sum()} evs")

# plt.plot(binc_kink, counts_kink, ds = 'steps-mid', c = 'y', lw = 2, ls = '-', 
#           label = f"kinky clusters contribution: {counts_kink.sum()} evs")

plt.grid(ls = 'solid')
plt.legend(prop = my_leg_font, title_fontsize = my_leg_title_fs, title = "Legend", loc = "upper right")

### Plot Display
if run_figs == True:    
  plt.show()

elif run_figs == False:    
  plt.close(last_fig_idx)

### Salvataggio figure
if run_figs == True and save_figs == True:
   plt.savefig(figs_folder + "hist_ang_inc_all_evs")
   
# --- Follows in ITERATION code --- #

#%% PLOTTING: angles of incidence vs clusters size

plt.close('all')

import copy, matplotlib as mpl # <-- Better handling of histdD figures

my_cmap = copy.copy(mpl.cm.inferno) 
my_cmap.set_bad(my_cmap(0))

areas_0_arr = xy_clst_dim_arr[0,:,0]*xy_clst_dim_arr[0,:,1]
areas_1_arr = xy_clst_dim_arr[1,:,0]*xy_clst_dim_arr[1,:,1]
areas_2_arr = xy_clst_dim_arr[2,:,0]*xy_clst_dim_arr[2,:,1]


n_bins_2D = 15

range_x_2D_max = ang_inc_arr.max()
range_y_2D_max = xy_clst_dim_arr.max()

range_2D_0 = ((0, range_x_2D_max), (0, range_y_2D_max)) # None
range_2D_1 = ((0, range_x_2D_max), (0, range_y_2D_max)) # None
range_2D_2 = ((0, range_x_2D_max), (0, range_y_2D_max)) # None

counts_2D_0, bins_x_0, bins_y_0 = np.histogram2d(areas_0_arr, ang_inc_arr,
                                                 bins = n_bins_2D, range = range_2D_0)

counts_2D_1, bins_x_1, bins_y_1 = np.histogram2d(areas_1_arr, ang_inc_arr,
                                                 bins = n_bins_2D, range = range_2D_1)

counts_2D_2, bins_x_2, bins_y_2 = np.histogram2d(areas_2_arr, ang_inc_arr,
                                                 bins = n_bins_2D, range = range_2D_2)

last_fig_idx += 1 ; plt.figure(last_fig_idx, figsize = my_rect_fig_size)
plt.suptitle(f'Detsync Analysis - Clusters analysis - All healthy events: {worth_idx_arr.size}', fontsize = my_sup_title_fs, fontdict = my_font_sup)
        
plt.subplot(1,3,1)

plt.title("Det 0", fontdict = my_std_font)
plt.xlabel('Angle [°]', fontdict = my_std_font)
plt.ylabel('Cluster Size [pixels]', fontdict = my_std_font)

plt.hist2d(ang_inc_arr, areas_0_arr, bins = n_bins_2D, range = range_2D_0)

# plt.imshow(counts_2D_0, cmap = "inferno", norm = 'linear', origin = 'lower')
plt.yticks(ticks = np.arange(1, range_y_2D_max + 1, 1) - 1.2, 
           labels = [f"{i}" for i in range(16)])


# plt.legend(prop = my_leg_font, title_fontsize = my_leg_title_fs, title = "Legend", loc = "lower center")


plt.subplot(1,3,2)

plt.title("Det 1", fontdict = my_std_font)
plt.xlabel('Angle [°]', fontdict = my_std_font)
# plt.ylabel('Area [pixels]', fontdict = my_std_font)

# plt.xlim([, ])
# plt.ylim([, ])

plt.hist2d(ang_inc_arr, areas_1_arr, bins = n_bins_2D, range = range_2D_1)

# plt.imshow(counts_2D_1, cmap = "inferno", norm = 'linear', origin = 'lower')
plt.yticks(ticks = np.arange(1, range_y_2D_max + 1, 1) - 1.2, 
           labels = [f"{i}" for i in range(16)])

# plt.legend(prop = my_leg_font, title_fontsize = my_leg_title_fs, title = "Legend", loc = "lower center")


plt.subplot(1,3,3)

plt.title("Det 2", fontdict = my_std_font)
plt.xlabel('Angle [°]', fontdict = my_std_font)
# plt.ylabel('Area [pixels]', fontdict = my_std_font)


# plt.xlim([, ])
# plt.ylim([, ])

plt.hist2d(ang_inc_arr, areas_2_arr, bins = n_bins_2D, range = range_2D_2)

# plt.imshow(counts_2D_2, cmap = "inferno", norm = 'linear', origin = 'lower')
plt.yticks(ticks = np.arange(1, range_y_2D_max + 1, 1) - 1.2, 
           labels = [f"{i}" for i in range(16)])

# plt.colorbar(fraction = 0.05, shrink = 0.75)

# plt.legend(prop = my_leg_font, title_fontsize = my_leg_title_fs, title = "Legend", loc = "lower center")


### Plot Display
if run_figs == True:    
  plt.show()

elif run_figs == False:    
  plt.close(last_fig_idx)

### Salvataggio figure
if run_figs == True and save_figs == True:
   plt.savefig(figs_folder + "clst_area_vs_ang_all_evs")
   
   

#%%  - capire perché distribuzione non piccata su 0

# --- Plot di correlazione x vs x, y vs y --> NON HANNO SENSO SU UN RIV CON MILIONE DI PIXEL SPAZIATI DI CENTIMETRI


# --- Plot Display --- #

last_fig_idx += 1 ; plt.figure(last_fig_idx, figsize = my_rect_fig_size)
plt.suptitle('Angles Correlation', fontsize = my_sup_title_fs, fontdict = my_font_sup)

plt.subplot(1,3,1)

plt.title("angle x", fontdict = my_std_font)

plt.xlabel('angle x (°)', fontdict = my_std_font)
plt.ylabel('counts', fontdict = my_std_font)

plt.hist(ang_inc_x_arr, bins = 60, range = None, color = 'y')

plt.grid(ls = 'solid')

plt.subplot(1,3,2)

plt.title("angle y", fontdict = my_std_font)

plt.xlabel('angle y (°)', fontdict = my_std_font)
plt.ylabel('counts', fontdict = my_std_font)

plt.hist(ang_inc_y_arr, bins = 50, range = None, color = 'y')

plt.grid(ls = 'solid')

plt.subplot(1,3,3)

plt.title("angle x vs angle y", fontdict = my_std_font)

plt.xlabel('angle x (°)', fontdict = my_std_font)
plt.ylabel('angle y (°)', fontdict = my_std_font)

plt.hist2d(ang_inc_x_arr, ang_inc_y_arr, bins = 100, cmap = 'inferno', range = ((-50, 50), (-50, 50)))

plt.hlines(0, -50, 50, ls = '--', lw = 2, colors = 'g')
plt.vlines(0, -50, 50, ls = '--', lw = 2, colors = 'g')

plt.colorbar(fraction = 0.05, shrink = 0.75)

plt.show()

### Plot Display
if run_figs == True:    
  plt.show()

elif run_figs == False:    
  plt.close(last_fig_idx)

### Salvataggio figure
if run_figs == True and save_figs == True:
    plt.savefig(figs_folder + "")

#%%


# --- Plot Display --- #

last_fig_idx += 1 ; plt.figure(last_fig_idx, figsize = my_rect_fig_size)
plt.suptitle('Angles Correlation', fontsize = my_sup_title_fs, fontdict = my_font_sup)

plt.subplot(2,2,1)

plt.title("angle x", fontdict = my_std_font)

plt.xlabel('angle x (°)', fontdict = my_std_font)
plt.ylabel('y', fontdict = my_std_font)

plt.hist2d(ang_inc_x_arr, y2_arr, bins = 50, cmap = 'inferno')

plt.grid(ls = 'solid')

plt.subplot(2,2,2)

plt.title("angle y", fontdict = my_std_font)

plt.xlabel('angle y (°)', fontdict = my_std_font)
plt.ylabel('x', fontdict = my_std_font)

plt.hist2d(ang_inc_y_arr, x2_arr, bins = 50, cmap = 'inferno')

plt.grid(ls = 'solid')


plt.subplot(2,2,1)

plt.title("angle x", fontdict = my_std_font)

plt.xlabel('angle x (°)', fontdict = my_std_font)
plt.ylabel('y2', fontdict = my_std_font)

plt.hist2d(ang_inc_x_arr, y2_arr, bins = 20, cmap = 'inferno')

plt.grid(ls = 'solid')

plt.subplot(2,2,2)

plt.title("angle y", fontdict = my_std_font)

plt.xlabel('angle y (°)', fontdict = my_std_font)
plt.ylabel('x2', fontdict = my_std_font)

plt.hist2d(ang_inc_y_arr, x2_arr, bins = 50, cmap = 'inferno')

plt.grid(ls = 'solid')


plt.subplot(2,2,3)

# plt.title("angle x", fontdict = my_std_font)

plt.xlabel('angle x (°)', fontdict = my_std_font)
plt.ylabel('y0', fontdict = my_std_font)

plt.hist2d(ang_inc_x_arr, y0_arr, bins = 50, cmap = 'inferno')

plt.grid(ls = 'solid')

plt.subplot(2,2,4)

plt.title("angle y", fontdict = my_std_font)

plt.xlabel('angle y (°)', fontdict = my_std_font)
plt.ylabel('x0', fontdict = my_std_font)

plt.hist2d(ang_inc_y_arr, x0_arr, bins = 50, cmap = 'inferno')

plt.grid(ls = 'solid')


### Plot Display
if run_figs == True:    
  plt.show()

elif run_figs == False:    
  plt.close(last_fig_idx)

### Salvataggio figure
if run_figs == True and save_figs == True:
    plt.savefig(figs_folder + "")




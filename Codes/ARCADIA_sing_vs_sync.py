
"""
Single sensors data are opened separately; ts_ext columns are then analyzed
and joined inside the same plane, and then the corresponding groups of events
are compared between planes, to be merged in the same events

For both of the 2 previous steps there are 2 thr required:
    
.) thr_ts_int
.) thr_ts_ext
    
These thrs are established while analyzing the ts distributions inside all detsync data    

"""

import os, time


#%% INTRO

""" Personal Folders Handling """
PC_str = r'C:\Giorgio PC/'

main_folder = PC_str + r'UNI\INSUBRIA\Attività Extra\ARCADIA - Recap/' # <-- Codes and Data folder

codes_folder = main_folder + 'Codes/'

txt_sing_folder = main_folder + r'Data\Single Sensor Data\txt format/'
txt_sync_folder = main_folder + r'Data\Detsync Data\txt format/'

# --- Check if folders exist --- #
if (os.path.isdir(codes_folder) == False): raise(Exception(f"Path \n{codes_folder}\n Not found"))

if (os.path.isdir(txt_sing_folder)   == False): raise(Exception(f"Path \n{txt_sing_folder}\n Not found"))
if (os.path.isdir(txt_sync_folder)   == False): raise(Exception(f"Path \n{txt_sync_folder}\n Not found"))

os.chdir(main_folder)

""" Graphic Parameters """
from matplotlib import pyplot as plt

plt.close('all')


last_fig_idx = -1 # <-- keeps count of opened figures

my_rect_fig_size = (15, 8) # <-- figures box dimensions
my_sqrd_fig_size = (8, 8)

my_std_font = {'family': 'Verdana',  'color':'black','weight':'normal','size': 20} # <-- std font for titles and labels 
my_font_sup = {'family': 'garamond', 'color':'darkblue','weight':'normal'} # <-- suptitles font
my_leg_font = {'family': 'Georgia', "size": 15} # <-- legend font

my_sup_title_fs = 25 # <-- suptitle size
my_leg_title_fs = 15 # <-- legend title size
my_txt_fs       = 15 # <-- printed text size

my_xy_tick_size = 18 # <-- labels tick size


# --- Ajusting cmap for 2D plots --- #
import copy, matplotlib as mpl

my_cmap = copy.copy(mpl.cm.inferno) # <-- copy the default cmap

my_cmap.set_bad(color = 'k') 

# - Figures - #
run_figs = True
# run_figs = False

# - Boolean parameters to save/ovwr_arrs files (figures/arrays/json files) - #

save_arrs = True
# save_arrs = False

print_msgs = True
# print_msgs = False

if print_msgs == True:
   print("\nSpecified folders are OK")
   

#%% MODULES & LIBRARIES

import numpy   as np
import pandas  as pd

os.chdir(codes_folder)

from ARCADIA_funcs import decode_detsync_from_txt, group_idxs, ts_compatibles

proceed_save_idxs = True ; ovwr_arrs = False # <-- for saving arrays

n_file_to_read = 14

#%% DATA LOAD 

# - (same as for Detsync Analysis) - #

# --- Nomi delle colonne nel sw di acquisizione --- #
columns_lst = ['SR bit',  'Hitmap Byte',  'Core pr', 'Col',   'Sez',         'Ser', 
               'Falling', 'ts chip',      'ts FPGA', 'ts SW', 'ts extended', 'trigger', 'sensor']

data_all_df = pd.DataFrame(data = np.zeros(shape = [0, len(columns_lst)]), columns = columns_lst)

frames_all_lst = []

pix_fired_all_arr = np.empty(shape = [0,])


print(f"\nOpening {n_file_to_read}_detsync")    

temp_frames_lst, temp_pix_fired_arr, temp_DS_df, temp_DS_arr = decode_detsync_from_txt(path_to_load = txt_sync_folder, filename = f"{n_file_to_read}_detsync") 

frames_all_lst += temp_frames_lst

pix_fired_all_arr = np.concatenate((pix_fired_all_arr, temp_pix_fired_arr))


data_all_df = pd.concat(objs = (data_all_df, temp_DS_df))

data_all_arr = data_all_df.iloc[:,:].values


#%% Sampling ts_diffs (INT and EXT) of detsync data to evaluate proper thrs

max_ts_diff_int_lst = [] # <-- Raccolgo max differenze di ts tra eventi stesso piano
max_ts_diff_ext_lst = [] # <-- Raccolgo max differenze di ts tra eventi tra piani diversi

j = 0

i_0 = 0

while True:
    
    i = j
    
    j += 1

    if j == data_all_arr.shape[0]: break 

    temp_curr_det = data_all_arr[i, 12] # <-- current Det idx (0,1,2)

    temp_prox_det = data_all_arr[j, 12] # <-- next  Det idx (0,1,2)

    print(f"it n. {j}/{data_all_arr.shape[0]-1}")   

    while temp_curr_det == temp_prox_det:

        j += 1

        if j == data_all_arr.shape[0]: break 
            
        temp_prox_det = data_all_arr[j, 12]
        
        print(f"\tsubit. n. {j}/{data_all_arr.shape[0]-1}")   

    
    temp_ts_arr = data_all_arr[i : j, 10] # <-- ts_ext column

    temp_ts_diff_arr = np.diff(temp_ts_arr, n = 1)  
    
    # # # --- Collect max ts diff within a same plane of an event  --- # # #            
    if temp_ts_diff_arr.size != 0: # <-- if the array of ts has more than 1 element (i.e. the array of diff hasn't size 0)

       max_ts_diff_int_lst.append(temp_ts_diff_arr.max())

    # # # --- Collect max ts diff between separate planes in same event  --- # # #            
    if i != 0 and temp_prox_det != 1: # <-- Non posso farlo alla prima iterazione o quando si passa da det2 a det0

       temp_ts_prec_arr = data_all_arr[i_0 : i, 10]
       
       temp_ts_prec_diff_lst = []
       
       for t0 in temp_ts_prec_arr:
           
           for t in temp_ts_arr:
                   
               temp_ts_prec_diff_lst.append(np.abs(t - t0))

       max_ts_diff_ext_lst.append(max(temp_ts_prec_diff_lst))


    i_0 = i # <-- tengo traccia dell'indice di inizio array corrente

    if j == data_all_arr.shape[0]: break 

max_ts_diff_int_arr = np.array(max_ts_diff_int_lst)
max_ts_diff_ext_arr = np.array(max_ts_diff_ext_lst)

print(f"Max ts diff inside  plane  {max_ts_diff_int_arr.max()}  clock cycles") 
print(f"Max ts diff between planes {max_ts_diff_ext_arr.max()} clock cycles") 


#%% Plotting ts differences

plt.close("all")

last_fig_idx += 1

plt.figure(last_fig_idx, figsize = my_rect_fig_size)

plt.suptitle("Single vs Detsync analysis", fontdict = my_font_sup, fontsize = my_sup_title_fs)

plt.subplot(1,2,1)

plt.title("ts differences within plane", fontdict = my_std_font)
plt.xlabel("n. of samples", fontdict = my_std_font)
plt.ylabel("max ts differences (cc)", fontdict = my_std_font)

# plt.xlim()
plt.ylim([0, max(max_ts_diff_int_arr.max(), max_ts_diff_ext_arr.max()) + 20])


plt.plot(max_ts_diff_int_arr, c = 'c', marker = ',', ms = 1, ls = '-', lw = 1, label = "Trend plot")
  
plt.plot(max_ts_diff_int_arr.argmax(), max_ts_diff_int_arr.max(), c = 'm', marker = 'o', 
         ms = 7, ls = '', lw = 1, label = f"maximum = {max_ts_diff_int_arr.max():.0f}")

plt.grid(ls = '-', lw = 0.25, c = 'k', which = 'both', axis = 'both') #
plt.legend(prop = my_leg_font, title_fontsize = my_leg_title_fs, title = "Legend", loc = "upper right")
 

plt.subplot(1,2,2)

plt.title("ts differences among planes", fontdict = my_std_font)
plt.xlabel("n. of samples", fontdict = my_std_font)
plt.ylabel("max ts differences (cc)", fontdict = my_std_font)

# plt.xlim()
plt.ylim([0, max(max_ts_diff_int_arr.max(), max_ts_diff_ext_arr.max()) + 20])


plt.plot(max_ts_diff_ext_arr, c = 'c', marker = ',', ms = 1, ls = '-', lw = 1, label = "Trend plot")

plt.plot(max_ts_diff_ext_arr.argmax(), max_ts_diff_ext_arr.max(), c = 'm', marker = 'o', 
         ms = 7, ls = '', lw = 1, label = f"maximum = {max_ts_diff_ext_arr.max():.0f}")

plt.grid(ls = '-', lw = 0.25, c = 'k', which = 'both', axis = 'both') #
plt.legend(prop = my_leg_font, title_fontsize = my_leg_title_fs, title = "Legend", loc = "upper left")
        
 
plt.show()


#%% MERGING EVENTS

# -- Use the sampled max_ts_differences (int and ext) to group idxs of the same
#    events distributed among the 3 planes -- #

# - I add a small quantity (e.g. 5, much smaller than the average ts distance of separate evs)
#   to the top thrs just to be sure to include all the considered ts distances - #

thr_ts_int = max_ts_diff_int_arr.max() + 5 # <-- (ADC) value below which two rows of same plane are joined to make the same event

thr_ts_ext = max_ts_diff_ext_arr.max() + 5 # <--  value below which two events from different planes are joined

m = n_file_to_read 

print(f"Grouping events of file {m}")

data_all_det_0_df = pd.read_csv(txt_sing_folder + f'{m}_det0' + r".txt", sep = '\t')
data_all_det_1_df = pd.read_csv(txt_sing_folder + f'{m}_det1' + r".txt", sep = '\t')
data_all_det_2_df = pd.read_csv(txt_sing_folder + f'{m}_det2' + r".txt", sep = '\t')

data_all_det_0_arr = data_all_det_0_df.iloc[:, :].values
data_all_det_1_arr = data_all_det_1_df.iloc[:, :].values
data_all_det_2_arr = data_all_det_2_df.iloc[:, :].values    

ts_col_0_arr = data_all_det_0_arr[:, 10] # <-- ts_ext column
ts_col_1_arr = data_all_det_1_arr[:, 10]
ts_col_2_arr = data_all_det_2_arr[:, 10]


## -- NB: Nelle prossime iterazioni lasciare 'evs_{}_lst = []' come liste (a quanto pare qui sono + veloci degli array) -- ##
# - Det 0 - #
idxs_bros_0_arr = group_idxs(ts_col_0_arr, thr_ts_int) # <-- join rows by ts inside same plane

evs_0_lst = [] # <-- Dynamic list containing separate rows belonging to same event
   
for i, idx_pair in enumerate(idxs_bros_0_arr):
 
    evs_0_lst.append(data_all_det_0_arr[int(idx_pair[0]) : int(idx_pair[1]) + 1, :])
    
  
# - Det 1 - #  
idxs_bros_1_arr = group_idxs(ts_col_1_arr, thr_ts_int)

evs_1_lst = [] 
   
for i, idx_pair in enumerate(idxs_bros_1_arr):
 
    evs_1_lst.append(data_all_det_1_arr[int(idx_pair[0]) : int(idx_pair[1]) + 1, :])


# - Det 2 - #
idxs_bros_2_arr = group_idxs(ts_col_2_arr, thr_ts_int)

evs_2_lst = [] 
   
for i, idx_pair in enumerate(idxs_bros_2_arr):

    evs_2_lst.append(data_all_det_2_arr[int(idx_pair[0]) : int(idx_pair[1]) + 1, :])


# -- Creo elenco degli ordini di grandezza dei ts salvati -- # (utile per velocizzare gli abbinamenti successivi)

# - Det 0 - #  
magn_ord_0_lst = []

for group_0 in evs_0_lst:
    
    temp_ts_repr = group_0[:, 10][-1] # <-- considero l'ultimo dei ts (il più elevato)

    temp_n_digit = len(str(temp_ts_repr)) - 2 # <-- il (-2) serve a rimuovere il ".0" posto alla fine di ogni ts

    magn_ord_0_lst.append([temp_n_digit, np.uint(str(temp_ts_repr)[0])]) # <-- salvo il numero di cifre e anche la prima
    
magn_ord_0_arr = np.array(magn_ord_0_lst)   


# - Det 1 - #  
magn_ord_1_lst = []

for group_1 in evs_1_lst:
    
    temp_ts_repr = group_1[:, 10][-1] 

    temp_n_digit = len(str(temp_ts_repr)) - 2 

    magn_ord_1_lst.append([temp_n_digit, np.uint(str(temp_ts_repr)[0])]) # <-- 2nd value is the first digit of the number
    
magn_ord_1_arr = np.array(magn_ord_1_lst)   


# - Det 2 - #  
magn_ord_2_lst = []

for group_2 in evs_2_lst:
    
    temp_ts_repr = group_2[:, 10][-1] 

    temp_n_digit = len(str(temp_ts_repr)) - 2 

    magn_ord_2_lst.append([temp_n_digit, np.uint(str(temp_ts_repr)[0])])
    
magn_ord_2_arr = np.array(magn_ord_2_lst)   


#%% --- Merging 012 (Long Procedure) --- #

t_0 = time.time_ns() # <-- tracks the time

idxs_grouped_02_arr  = np.empty(shape = [0, 2])

idxs_grouped_012_arr = np.empty(shape = [0, 3])

idxs_1_to_skip_arr = np.empty(shape = [0,])
idxs_2_to_skip_arr = np.empty(shape = [0,])


# - I exploit the fact that ts inside single planes are always increasing - #    
for i, group_0 in enumerate(evs_0_lst):
    
    t = time.time_ns()
    
    if np.mod(i+1, 50) == 0: 
       print(f"{i+1}/{len(evs_0_lst)} --- {idxs_grouped_02_arr.shape[0]} 02 idxs --- {idxs_grouped_012_arr.shape[0]} 012 idxs")
           
    temp_n_digit_0 = magn_ord_0_arr[i, 0]
    
    temp_first_digit_0 = magn_ord_0_arr[i, 1]
    
    temp_ts_arr_0 = group_0[:, 10]

    for k, group_2 in enumerate(evs_2_lst):
                                           
        temp_n_digit_2 = magn_ord_2_arr[k, 0]
        
        temp_first_digit_2 = magn_ord_2_arr[k, 1]
        
        temp_ts_arr_2 = group_2[:, 10]    

        # - risparmio tempo computazionale skippando gruppi di eventi già controllati o che non serve controllare - #
        if k in idxs_2_to_skip_arr or ((temp_n_digit_0 == temp_n_digit_2) and (temp_first_digit_0 != temp_first_digit_2)):
            continue
            
        if temp_n_digit_0 > temp_n_digit_2: # <-- se temp_n_digit_0 > temp_n_digit_1 lo saranno anche i prossimi temp_n_digit_0
           idxs_2_to_skip_arr = np.concatenate((idxs_2_to_skip_arr, np.array([k,]))) 
           continue
 
        # --- Se group_2 ha dei ts maggiori di group_0 + thr_ts_ext, o il suo ts minore ha
        #     più cifre del ts maggiore di group_0, vuol dire che l'opportunità di match è già passata
        #     e quindi non mi metto a checkare tutti i group_2 successivi --- #   
        if temp_ts_arr_2[0] > (temp_ts_arr_0[-1] + thr_ts_ext) or temp_n_digit_2 > temp_n_digit_0: break 
     
        # if ts_compatibles(temp_ts_arr_0, temp_ts_arr_2, thr_ts_ext) == True:
        if np.abs(temp_ts_arr_0[0] - temp_ts_arr_2[0]) <= thr_ts_ext:

           idxs_grouped_02_arr = np.vstack((idxs_grouped_02_arr, np.array([[i,  k]]))) 
            
           idxs_2_to_skip_arr = np.concatenate((idxs_2_to_skip_arr, np.array([k,])))            

           for j, group_1 in enumerate(evs_1_lst):
                                                                       
                temp_n_digit_1 = magn_ord_1_arr[j, 0]
               
                temp_first_digit_1 = magn_ord_1_arr[j, 1]
             
                temp_ts_arr_1 = group_1[:, 10]                

                # - risparmio tempo computazionale skippando gruppi di eventi già controllati o che non serve controllare - #
                if j in idxs_1_to_skip_arr or ((temp_n_digit_0 == temp_n_digit_1) and (temp_first_digit_0 != temp_first_digit_1)):
                    continue
                    
                if temp_n_digit_0 > temp_n_digit_1: # <-- se temp_n_digit_0 > temp_n_digit_2 lo saranno anche i prossimi temp_n_digit_0
                   idxs_1_to_skip_arr = np.concatenate((idxs_1_to_skip_arr, np.array([j,]))) 
                   continue

                 
                # if ts_compatibles(temp_ts_arr_0, temp_ts_arr_1, thr_ts_ext) == True and ts_compatibles(temp_ts_arr_1, temp_ts_arr_2, thr_ts_ext) == True:
                if np.abs(temp_ts_arr_0[0] - temp_ts_arr_1[0]) <= thr_ts_ext and np.abs(temp_ts_arr_1[0] - temp_ts_arr_2[0]) <= thr_ts_ext: 
                   
                   idxs_grouped_012_arr = np.vstack((idxs_grouped_012_arr, np.array([[i,j,k]]))) 
                       
                   idxs_1_to_skip_arr = np.concatenate((idxs_1_to_skip_arr, np.array([j,])))

    # print(f"{((time.time_ns() - t)/(10**9)):.3f} s")

print(f"Elapsed seconds for grouping events of file {m}: {((time.time_ns() - t_0)/(10**9)):.2f} seconds")   


if ovwr_arrs == True or (f"idxs_grouped_01_file_{m}_arr.npy" not in os.listdir(os.getcwd())) and (f"idxs_grouped_012_file_{m}_arr.npy" not in os.listdir(os.getcwd())):
    
   np.save(f"idxs_grouped_02_file_{m}_arr.npy" , idxs_grouped_02_arr)
   np.save(f"idxs_grouped_012_file_{m}_arr.npy", idxs_grouped_012_arr)
   
   print(f"file {m}/14 saved")

elif ovwr_arrs == False: pass
    
print("Done")

    
#%% Comparing Arrays

temp_frames_lst_A, temp_pix_fired_arr_A, temp_DS_df_A, temp_DS_arr_A = decode_detsync_from_txt(path_to_load = txt_sync_folder, filename = f"{m}_detsync") 
    
# idxs_grouped_012_arr = np.load(codes_folder + f"idxs_grouped_012_file_{m}_arr.npy")
    
# idxs_grouped_02_arr = np.load(codes_folder + f"idxs_grouped_02_file_{m}_arr.npy")

temp_DS_arr_B = np.empty(shape = [0,13])

for row in idxs_grouped_012_arr:
    
    row = np.uint(row)
    
    
    temp_arr_0 = evs_0_lst[row[0]]

    
    temp_arr_1 = evs_1_lst[row[1]]    
      
       
    temp_arr_2 = evs_2_lst[row[2]]    
   
    
    temp_group_012_arr = np.vstack((temp_arr_0, temp_arr_1, temp_arr_2))
    
    temp_DS_arr_B = np.concatenate((temp_DS_arr_B, temp_group_012_arr))


# --- Da array a DF --- #

temp_DS_df_B = pd.DataFrame(temp_DS_arr_B, columns = columns_lst)

#%% Check of Goodness

# OPPURE: 
# temp_equal_arr = np.equal(temp_DS_arr_A, temp_DS_arr_B)
# if temp_equal_arr.sum() == temp_equal_arr.size: print("Perfect Match")
    
if temp_DS_arr_A.shape[0] == temp_DS_arr_B.shape[0]:

   temp_AB_subtract_arr = np.abs(temp_DS_arr_A - temp_DS_arr_B)
   
   if temp_AB_subtract_arr.nonzero()[0].sum() == 0: print("Perfect Match")

else:
    
   idxs_A_no_match_lst = [] # <-- Lista degli indici dell'array detsync che non sono presenti in quello offline
    
   j = -1
    
   for i, row_B in enumerate(temp_DS_arr_B):
        
       j += 1
        
       row_A = temp_DS_arr_A[j, :]
       
       while np.sum(row_A == row_B) != 13: # <-- i.e. se tutti gli elementi non sono matchati
        
           idxs_A_no_match_lst.append(j)
            
           j += 1
          
           row_A = temp_DS_arr_A[j, :]
    
   idxs_A_no_match_arr = np.array(idxs_A_no_match_lst)
    
   if idxs_A_no_match_arr.size != 0:
    
      idxs_A_no_match_compact_lst = []
        
      for pair in group_idxs(idxs_A_no_match_arr, 1):
            
          idxs_A_no_match_compact_lst.append([idxs_A_no_match_arr[pair[0]] , idxs_A_no_match_arr[pair[1]]])
        
      idxs_A_no_match_compact_arr = np.array(idxs_A_no_match_compact_lst)
      
#%% ESTIMATE EFFICIENCY

idxs_grouped_02_file_12_arr  = np.load("idxs_grouped_02_file_12_arr.npy")
idxs_grouped_012_file_12_arr = np.load("idxs_grouped_012_file_12_arr.npy")

eff_file_12 = idxs_grouped_012_file_12_arr.shape[0] / idxs_grouped_02_file_12_arr.shape[0]


idxs_grouped_02_file_13_arr  = np.load("idxs_grouped_02_file_13_arr.npy")
idxs_grouped_012_file_13_arr = np.load("idxs_grouped_012_file_13_arr.npy")

eff_file_13 = idxs_grouped_012_file_13_arr.shape[0] / idxs_grouped_02_file_13_arr.shape[0]


idxs_grouped_02_file_14_arr  = np.load("idxs_grouped_02_file_14_arr.npy")
idxs_grouped_012_file_14_arr = np.load("idxs_grouped_012_file_14_arr.npy")

eff_file_14 = idxs_grouped_012_file_14_arr.shape[0] / idxs_grouped_02_file_14_arr.shape[0]


idxs_grouped_02_file_15_arr  = np.load("idxs_grouped_02_file_15_arr.npy")
idxs_grouped_012_file_15_arr = np.load("idxs_grouped_012_file_15_arr.npy")

eff_file_15 = idxs_grouped_012_file_15_arr.shape[0] / idxs_grouped_02_file_15_arr.shape[0]



print(eff_file_12, eff_file_13, eff_file_14, eff_file_15)








      
      

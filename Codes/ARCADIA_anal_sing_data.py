
""" 
    Code to unpack and analyze single sensors data

-) Decode from .csv --> 1st array and 2nd DF
-) Decode from .txt --> 1st DF    and 2nd array

Single sensors data contain too many spurious events to be decoded singularly;
therefore they are only decoded when comparing their match with detsync data

The decode here just creates a big array of all superposed frames, in order to:
    
    .) Identify noisy pixel
    .) Avoid memory overflow due to the immense amount of null frames

So the GOALS are:

1) ts check of consistency
2) noisy pixel identification  
    
"""

# -) txt data have been sampled starting from .csv data <-- (cfr. code ARCADIA_data_conversion.py)

import os, time

#%% INTRO

""" Personal Folders Handling """
PC_str = r'C:\Giorgio PC/'

main_folder = PC_str + r'UNI\INSUBRIA\Attività Extra\ARCADIA - Recap/' # <-- Codes and Data folder

codes_folder = main_folder + 'Codes/'

txt_sing_folder = main_folder + r'Data\Single Sensor Data\txt format/'

figs_ts_folder    = main_folder + r'Results\sing_analysis\Figs\ts consistency/'
figs_noisy_folder = main_folder + r'Results\sing_analysis\Figs\Noisy Pixels/'

arrs_ts_folder    = main_folder + r'Results\sing_analysis\Vals\ts consistency/'
arrs_noisy_folder = main_folder + r'Results\sing_analysis\Vals\Noisy Pixels/'

# --- Check if folders exist --- #
if (os.path.isdir(codes_folder) == False): raise(Exception(f"Path \n{codes_folder}\n Not found"))

if (os.path.isdir(txt_sing_folder)   == False): raise(Exception(f"Path \n{txt_sing_folder}\n Not found"))
if (os.path.isdir(figs_ts_folder)    == False): raise(Exception(f"Path \n{figs_ts_folder}\n Not found")) 
if (os.path.isdir(figs_noisy_folder) == False): raise(Exception(f"Path \n{figs_noisy_folder}\n Not found")) 
if (os.path.isdir(arrs_ts_folder)    == False): raise(Exception(f"Path \n{arrs_ts_folder}\n Not found")) 
if (os.path.isdir(arrs_noisy_folder) == False): raise(Exception(f"Path \n{arrs_noisy_folder}\n Not found")) 

os.chdir(main_folder)

""" Graphic Parameters """
from matplotlib import pyplot as plt

plt.close('all')


last_fig_idx = -1 # <-- keeps count of opened figures

my_rect_fig_size = (15, 8) # <-- figures box dimensions
my_sqrd_fig_size = (8, 8)

my_std_font = {'family': 'Verdana',  'color':'black','weight':'normal','size': 20} # <-- std font for titles and labels 
my_font_sup = {'family': 'garamond', 'color':'darkblue','weight':'normal'} # <-- suptitles font
my_leg_font = {'family': 'Georgia', "size": 15} # <-- legend font

my_sup_title_fs = 25 # <-- suptitle size
my_leg_title_fs = 15 # <-- legend title size
my_txt_fs       = 15 # <-- printed text size

my_xy_tick_size = 18 # <-- labels tick size


# --- Ajusting cmap for 2D plots --- #
import copy, matplotlib as mpl

my_cmap = copy.copy(mpl.cm.inferno) # <-- copy the default cmap

my_cmap.set_bad(color = 'k') 


print("\nspecified folders are OK")
   

#%% MODULES & LIBRARIES

import numpy    as np
import pandas   as pd

os.chdir(codes_folder)

from ARCADIA_funcs import merged_2D_frame, ZG_save_element

os.chdir(main_folder)

#%% SETUP VARIABLES

# - Fitting - #
run_fits  = True
# run_fits  = False

# - Figures - #
run_figs = True
# run_figs = False


# - Boolean parameters to save/ovwr_arrs files (figures/arrays/json files) - #
# save_figs = True
save_figs = False

save_arrs = True
# save_arrs = False


# save_dfs = True
save_dfs = False

# save_pars = True
save_pars = False


ovwr_figs = True
# ovwr_figs = False

ovwr_arrs = True
# ovwr_arrs = False

ovwr_dfs = True
# ovwr_dfs = False

ovwr_pars = True
# ovwr_pars = False


print_msgs = True
# print_msgs = False


perc_data = 0.1 # <-- % of first points to show on ts trend-plot

thr_std_noise = 5 # <-- n. of standard deviations beyond which a pixel is labelled as noisy


#%% DATA LOAD

# - Choose wether to open a single file or all of them - #
which_file = '12_det0'
# which_file = 'all'

print(f"\n Opening {which_file} data --- showing {100 * perc_data:.0f}% of ts values")

if which_file + ".txt" not in os.listdir(txt_sing_folder):
    
   raise(Exception(f"{which_file} seems not to exist in {txt_sing_folder}\nMaybe you have not done .csv --> .txt conversion yet")) 


#%% TS CONSISTENCY CHECK

warn_ts_ext_lst = [] # <-- Contains idx of datasets with corrupted ts_ext (i.e. not always increasing)


# --- Choose filename to Open --- #
    
for filename in os.listdir(txt_sing_folder):
    
    file_to_open = filename[:-4]
           
    if which_file != 'all':    

       if file_to_open != str(which_file): continue  # <-- skips loop until desired file is encountered
    
    print(f"{file_to_open}: TS check")
 
    t_0 = time.time_ns() # <-- tracks the time
    
    
    DS_df = pd.read_csv(txt_sing_folder + file_to_open + r".txt", sep = '\t')
    
    DS_arr = DS_df.iloc[:,:].values # <-- Merge all the events in a big 2D array
    
    print(f"\nElapsed seconds for {file_to_open}.txt unpack: {((time.time_ns() - t_0)/(10**9)):.2f} seconds")
    
    # - Select timestamps - #
    ts_FPGA_arr = np.array(DS_df["ts FPGA"])
    ts_ext_arr  = np.array(DS_df["ts extended"])
    
    
    ts_FPGA_arr_diff = np.diff(np.int64(ts_FPGA_arr), n = 1, prepend = ts_FPGA_arr[0]) # <-- I evaluate the differences of near timestamps
    
    thr_reset_ts = 1000 # <-- Valore arbitrario da confrontare per capire se ci sono stati dei reset
    
    
    if np.where(ts_FPGA_arr_diff < 0)[0].size > 0:

       if ts_FPGA_arr_diff.min() < thr_reset_ts:
           
          print("WARNING ts_FPGA: There have been ts_FPGA reset (as expectes)")
    

    ts_ext_arr_diff = np.diff(np.int64(ts_ext_arr), n = 1, prepend = ts_ext_arr[0]) # <-- I evaluate the differences of near timestamps
    
    if np.where(ts_ext_arr_diff < 0)[0].size > 0:
        
       print("WARNING ts_EXT: some ts_ext were not in ascending order") 
          
       warn_ts_ext_lst.append(file_to_open)
        
    else: print("ts_EXT are fine --- always growing")  
    
   
    # --- Plotting --- #
    if run_figs == True:
        
       plt.close(last_fig_idx)
        
       # - Parameter to plot the first 'perc_data' of data - #
       plot_interval = [i for i in range(0, int(np.round(perc_data * ts_ext_arr.size, decimals = 0)), 1)]
        
       # - prepare for plotting - #
       temp_x1_arr = np.array(plot_interval)
       temp_y1_arr = ts_FPGA_arr[plot_interval]
    
       temp_x2_arr = np.array(plot_interval)
       temp_y2_arr = ts_ext_arr[plot_interval]        
       
       
       temp_extent_factor = 0.15
        
       last_fig_idx += 1 
      
       fig, (axs_1, axs_2) = plt.subplots(1, 2, num = last_fig_idx, figsize = my_rect_fig_size)
          
       fig.suptitle(f"Single Sensor Analysis - {file_to_open} - first {perc_data*100:.0f}% of points", 
                    fontsize = my_sup_title_fs, fontdict = my_font_sup)
         
         
       axs_1.set_title ('FPGA timestamp', fontdict = my_std_font)
       axs_1.set_xlabel('# data', fontdict = my_std_font)
       axs_1.set_ylabel('TS values [cc = 250 ns]', fontdict = my_std_font)
         
       plt.rcParams['xtick.labelsize'] = my_xy_tick_size
       plt.rcParams['ytick.labelsize'] = my_xy_tick_size  
         
         
       temp_x_lim_sx = temp_x1_arr.min() - temp_extent_factor * np.abs(temp_x1_arr.max() - temp_x1_arr.min())
       temp_x_lim_dx = temp_x1_arr.max() + temp_extent_factor * np.abs(temp_x1_arr.max() - temp_x1_arr.min())
             
       temp_y_lim_sx = temp_y1_arr.min() - temp_extent_factor * np.abs(temp_y1_arr.max() - temp_y1_arr.min())
       temp_y_lim_dx = temp_y1_arr.max() + temp_extent_factor * np.abs(temp_y1_arr.max() - temp_y1_arr.min())
             
       temp_x_lim = (temp_x_lim_sx, temp_x_lim_dx)
       temp_y_lim = (temp_y_lim_sx, temp_y_lim_dx)   
     
       axs_1.set_xlim(temp_x_lim)
       axs_1.set_ylim(temp_y_lim)
         
       axs_1.plot(temp_x1_arr, temp_y1_arr, c = 'c', marker = '.', ms = 1, ls = '--', lw = 1,
                  label = "trend plot")
     
       axs_1.grid(ls = ':')
       axs_1.legend(prop = my_leg_font, title_fontsize = my_leg_title_fs, title = "Legend", loc = "best")
             
         
       axs_2.set_title ('Extended timestamp', fontdict = my_std_font)
       axs_2.set_xlabel('# data', fontdict = my_std_font)
       axs_2.set_ylabel('TS values [??]', fontdict = my_std_font)
        
       plt.rcParams['xtick.labelsize'] = my_xy_tick_size
       plt.rcParams['ytick.labelsize'] = my_xy_tick_size  
         
         
       temp_x_lim_sx = temp_x2_arr.min() - temp_extent_factor * np.abs(temp_x2_arr.max() - temp_x2_arr.min())
       temp_x_lim_dx = temp_x2_arr.max() + temp_extent_factor * np.abs(temp_x2_arr.max() - temp_x2_arr.min())
             
       temp_y_lim_sx = temp_y2_arr.min() - temp_extent_factor * np.abs(temp_y2_arr.max() - temp_y2_arr.min())
       temp_y_lim_dx = temp_y2_arr.max() + temp_extent_factor * np.abs(temp_y2_arr.max() - temp_y2_arr.min())
             
       temp_x_lim = (temp_x_lim_sx, temp_x_lim_dx)
       temp_y_lim = (temp_y_lim_sx, temp_y_lim_dx)   
     
       axs_2.set_xlim(temp_x_lim)
       axs_2.set_ylim(temp_y_lim)
         
       axs_2.plot(temp_x2_arr, temp_y2_arr, c = 'm', marker = '.', ms = 1, ls = '', lw = 1,
                  label = 'trend plot')
     
       axs_2.grid(ls = ':')
       axs_2.legend(prop = my_leg_font, title_fontsize = my_leg_title_fs, title = "Legend", loc = "best")
        
         
       fig.show()
         
       # --- Figures Saving --- #
       temp_fig_str = which_file + "_ts_check_trend"
         
       if save_figs == True:
             
          if temp_fig_str in os.listdir(figs_ts_folder):
            
             if ovwr_figs == True:
                   
                fig.savefig(figs_ts_folder + temp_fig_str) 
                  
             elif ovwr_figs == False: pass 
             
          elif temp_fig_str not in os.listdir(figs_ts_folder): 
            
               fig.savefig(figs_ts_folder + temp_fig_str)
        
       elif save_figs == False: pass
    

if len(warn_ts_ext_lst) == 0: print("ALL ts_ext ARE CONSISTENT")


#%% NOISY PIXELS ANALYSIS

# - Initialize arrays to store noisy pixels' coordinates - #
noisy_coords_arr = np.zeros(shape = [0,2]) # <-- will contain idx of datasets with corrupted ts_ext (i.e. not always increasing)

t_0 = time.time_ns() # <-- tracks the time

for filename in os.listdir(txt_sing_folder):
    
    file_to_open = filename[:-4]
        
    
    if which_file != 'all':    

       if file_to_open != str(which_file): continue  # <-- skips loop until desired file is encounteredì
     
    print(f"\nOpening {file_to_open}")


    DS_df = pd.read_csv(txt_sing_folder + file_to_open + r".txt", sep = '\t')
    
    DS_arr = DS_df.iloc[:,:].values # <-- Merge all the events in a big 2D array

    
    all_evs_sum = np.zeros(shape = [512, 512], dtype = "uint8") # <-- Array with the total pixel values projection along all events
    
    
    t = time.time_ns() # <-- tracks the time
        
    print("\nCreating matrix of all superposed frames...")
    
    all_evs_sum = merged_2D_frame(DS_arr, print_msgs = False) # <-- Loops along the whole merged DS
    
    print(f"Elapsed seconds for filling impacts area: {((time.time_ns() - t)/(10**9)):.2f}")   
        
    print(f"\nlooking for noisy pixels --- {thr_std_noise} std over median")
    n_pix_fired_ave = np.ceil(np.median(all_evs_sum[all_evs_sum > 0])) # <-- n. of times a pixel has been fired on average
    
    err_n_pix_fired_ave = np.ceil(np.std(all_evs_sum[all_evs_sum > 0])) 
    
    
    noise_thr = n_pix_fired_ave + thr_std_noise * err_n_pix_fired_ave # <-- Increasing the 10 will lower the number of pixels removed
    
    temp_noisy_coords_arr = np.stack(np.where(all_evs_sum >= noise_thr), axis = 1)
   
    noisy_coords_arr = np.concatenate((noisy_coords_arr, temp_noisy_coords_arr), axis = 0)

# - Saving arrays if requested - #
if save_arrs == True:
    
   ZG_save_element(element = noisy_coords_arr, path = arrs_noisy_folder,                       
                   name = f"noisy_coords_{which_file}", ovwr = ovwr_arrs)    

print(f"Total elapsed seconds for noisy pixels detection: {((time.time_ns() - t_0)/(10**9)):.2f} seconds")
print(f"\n{noisy_coords_arr.shape[0]} noisy pixels detected")


#%% --- PLOT: Impacts & Noisy pixels

# - Some graphic parameters to draw a circle around noisy pixels - #
theta = np.linspace(0 , 2*np.pi, num = 100)
 
radius = 5 # <-- udm of radius to draw around noisy pixels
 
a = radius * np.cos(theta)
b = radius * np.sin(theta)
# ---------------------------------------------------------------- #

# - Plotting - #
if run_figs == True:
    
   last_fig_idx += 1 
    
   fig, axs = plt.subplots(num = last_fig_idx, figsize = my_sqrd_fig_size)
      
   fig.suptitle(f'Single Sensors Analysis --- {which_file} file', fontsize = my_sup_title_fs, fontdict = my_font_sup)
        
   axs.set_title(f'{which_file} - All Impacts - {DS_df.shape[0]} pix fired', fontdict = my_std_font)
   
   temp_image = plt.imshow(all_evs_sum, norm = mpl.colors.LogNorm(), 
                           cmap = my_cmap, origin = 'upper')
   
   for i, temp_coords in enumerate(noisy_coords_arr):
       
       if i == 0: # <-- print labels just on 1st iteration
      
          axs.plot(temp_coords[1] + 0.5, temp_coords[0] + 0.5, c = 'w', marker = '*', ls = '', ms = 10, label = f'{noisy_coords_arr.shape[0]} noisy pixels: {thr_std_noise} std over median')
       
       if i > 0:
      
          axs.plot(temp_coords[1] + 0.5, temp_coords[0] + 0.5, c = 'w', marker = '*', ls = '', ms = 10)
  
       axs.plot(a + temp_coords[1] + 0.5, b + temp_coords[0] + 0.5, c = 'lime', marker = ',', ls = '--', lw = 1)
         
   fig.colorbar(temp_image, ax = axs, fraction = 0.05, shrink = 0.75, location = 'right', extend = 'both') 
   
   axs.legend(prop = my_leg_font, title_fontsize = my_leg_title_fs, title = "Legend", loc = "best")
    
   fig.show() 
    
   # --- Figures Saving --- #
   temp_fig_str = which_file + "_impacts"
       
   if save_figs == True:
           
      if temp_fig_str in os.listdir(figs_ts_folder):
          
         if ovwr_figs == True:
                 
            fig.savefig(figs_noisy_folder + temp_fig_str) 
               
         elif ovwr_figs == False: pass 
           
      elif temp_fig_str not in os.listdir(figs_noisy_folder): 
         
            fig.savefig(figs_noisy_folder + temp_fig_str)
      
   elif save_figs == False: pass


#%%

a = all_evs_sum.ravel()

plt.plot(a[a > 0], c = 'k', marker = '.', ms = 2, ls = '')

